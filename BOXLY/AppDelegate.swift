//
//  AppDelegate.swift
//  BOXLY
//
//  Created by iMac on 03/01/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import IQKeyboardManagerSwift
import RESideMenu
import GoogleSignIn
import FBSDKCoreKit
import LoginWithAmazon
import GoogleMapsDirections
import Firebase
import FirebaseAuth

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    var KeyBoardHeight: CGFloat = 0
    var drawer : RESideMenu?
    var isSetDropPick: Bool = false
    var isFromDeliver: Bool = false
    var isFromPickup: Bool = false
    var arrSavedUsers = NSMutableArray()
    var currentLat: Double = 0.0
    var currentLong: Double = 0.0
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        FirebaseApp.configure()
        setupGoogleServices()
        setupStripe()
        
        //FACEBOOK LOGIN
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)

        SetGIFLoaderImage()
        IQKeyboardManager.shared.enable = true
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        setupInitials()

        return true
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        let sourceApplication: String? = options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String
        let facebookDidHandle = ApplicationDelegate.shared.application(app,open: url as URL, sourceApplication: sourceApplication, annotation: nil)
        let awsDidHandle = AIMobileLib.handleOpen(url, sourceApplication: UIApplication.OpenURLOptionsKey.sourceApplication.rawValue)
        guard let googleDidHandle = GIDSignIn.sharedInstance()?.handle(url) else { return facebookDidHandle }
        return facebookDidHandle || googleDidHandle || awsDidHandle
    }
    
    func setupInitials(){
        if UserDefaults.standard.bool(forKey: IS_LOGGED_IN) == true {
            GoToTabBar()
        }else {
            let vc = loadVC(strStoryboardId: SB_LOGIN, strVCId: "LauncherVC")
            self.window?.rootViewController = vc
        }
    }
    
    func GoToTabBar() {
        let vc = loadVC(strStoryboardId: SB_MAIN, strVCId: "TabBarVC")
        let nav = UINavigationController(rootViewController: vc)
        nav.setNavigationBarHidden(true, animated: false)

        let sideVC = loadVC(strStoryboardId: SB_MAIN, strVCId: "SideMenuVC")

        drawer = RESideMenu(contentViewController: nav, leftMenuViewController: sideVC, rightMenuViewController: nil)
        drawer?.bouncesHorizontally = false
        drawer?.contentViewInPortraitOffsetCenterX = 50
        drawer?.contentViewScaleValue = 0.85
        drawer?.panGestureEnabled = false
        drawer?.delegate = self
        self.window?.rootViewController = drawer
        self.window?.makeKeyAndVisible()
    }
    
    private func setupStripe() {
        StripeManager.shared.setupStripe()
    }
    
    private func setupGoogleServices() {
        GMSServices.provideAPIKey(kGOOGLE)
        GoogleMapsDirections.provide(apiKey: kGOOGLE)
        GIDSignIn.sharedInstance().clientID = "591617137269-i8puf0eq7o2e69652s5ie1qpk82gl8mr.apps.googleusercontent.com"
    }
}

extension AppDelegate : RESideMenuDelegate {
    func sideMenu(_ sideMenu: RESideMenu!, didHideMenuViewController menuViewController: UIViewController!) {
        if let nav = drawer?.contentViewController as? UINavigationController {
            if let tab = nav.viewControllers[0] as? TabBarVC {
                tab.view.layer.cornerRadius = 0
            }
        }
    }
}

extension AppDelegate {
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
      return userActivity.webpageURL.flatMap(handlePasswordlessSignIn)!
    }
    
    func handlePasswordlessSignIn(withURL url: URL) -> Bool {
      let link = url.absoluteString

      if Auth.auth().isSignIn(withEmailLink: link) {
        UserDefaults.standard.set(link, forKey: "Link")
        
        let vc = loadVC(strStoryboardId: SB_LOGIN, strVCId: "VerifyEmailVC")
        if let nav = self.window?.rootViewController as? UINavigationController {
            nav.pushWithFadeTo(vc)
        }
        
        return true
      }
      return false
    }
}
