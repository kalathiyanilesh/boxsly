//
//  KeyNamesConstants.swift
//  ELFramework
//
//  Created by Admin on 14/12/17.
//  Copyright © 2017 EL. All rights reserved.
//

import Foundation
import UIKit

let APP_DELEGATE = UIApplication.shared.delegate as! AppDelegate

let kGOOGLE = "AIzaSyCezZ8I9Coi-YcUTN-ccHha2dh3H1QECLk"
let kGOOGLE_PLACE = "AIzaSyBCHPjyUf-FL7C0vC9ayVjVu1N_qoH05SI"

//MARK:- APPNAME
public let APPNAME :String = "Boxly"

//MARK:- LOADER COLOR
let loadercolor = [Color_Hex(hex: "6E5CFF"), Color_Hex(hex: "FFFFFF")]

//MARK:- PLACEHOLDER IMAGES
let USER_PLACE_HOLDER = UIImage(named: "")

//MARK:- HEADER KEY
public let kHEADER = "token"
public let kTIMEZONE = "timezone"

//MARK:- PAGELIMIT (PAGINATION)
public let PAGE_LIMIT = 15

//MARK:- COLORDS
let RED_COLOR = Color_Hex(hex: "F53C5E")
let GREEN_COLOR = Color_Hex(hex: "20D479")

//MARK:- STORY BOARD NAMES
public let SB_ONBOARDING = "Onboarding"
public let SB_LOGIN:String = "Login"
public let SB_MAIN:String = "Main"
public let SB_Hosting = "Hosting"
public let SB_ADDRESS = "Address"
public let SB_SELECT_TIMES = "SelectTimes"
public let SB_DOCUMENT = "Document"
public let SB_SCHEDULE = "Schedule"
public let SB_SCHEDULE_MAP = "ScheduleMap"
public let SB_BOOKING = "Booking"
public let SB_PICKUP = "Pickup"
public let SB_CHAT = "Chat"

//MARK:- USER DEFAULTS KEY
let UD_TOKEN_OBJECT = "UD_TOKEN_OBJECT"
let IS_LOGGED_IN = "IS_LOGGED_IN"
let UD_USERNAME = "UD_USERNAME"
let UD_USERIMAGE = "UD_USERIMAGE"
let UD_USEREMAIL = "UD_USEREMAIL"
let UD_SAVEDUSER = "UD_SAVEDUSER"

let SEEN_PICKUP_INTRO = "SEEN_PICKUP_INTRO"

//MARK:- CONTROLLERS ID
public let idNavOnboarding = "navonbaording"
public let idNavLogin = "navlogin"

//MARK - MESSAGES
let ServerResponseError = "Server Response Error"
let ErrorMSG = "Something going wrong. Please try again!"
let DontHaveCamera = "You don't have camera."
let InternetNotAvail = "Internet Not Available."

//MARK: Observers
let ShowScheduleDeliveryVC = "ShowScheduleDeliveryVC"
let SetScheduleDirectionText = "SetScheduleDirectionText"
let ShowPickUpOptionsVC = "ShowPickUpOptionsVC"
let ShowPickupMenuVC = "ShowPickupMenuVC"
