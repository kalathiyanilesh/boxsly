//
//  Constants.swift
//
//  Created by C237 on 30/10/17.
//  Copyright © 2017. All rights reserved.
//

import Foundation
import UIKit

let DEFAULT_ERROR_MESSAGE = "Please try again later."

let kDATA = "data"
let kSUCCESS = "success"
let kMESSAGE = "message"
let kRESPONSE_STATUS = "ResponseStatus"

let SERVER_URL = ""
let TERMS_CONDITION = ""

//API Services
let APILogin = "user/login"

