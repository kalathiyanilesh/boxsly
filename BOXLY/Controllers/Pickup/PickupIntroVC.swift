//
//  PickupIntroVC.swift
//  BOXLY
//
//  Created by Dhaval on 25/02/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit
protocol delegatePickupIntro {
    func backFromPickupIntro()
}
class PickupIntroVC: UIViewController {

    var delegatePickupIntro : delegatePickupIntro?
    override func viewDidLoad() {
        super.viewDidLoad()
        UserDefaults.standard.set(true, forKey: SEEN_PICKUP_INTRO)
    }

    @IBAction func btnBackClicked(_ sender: Any) {
        UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
            self.navigationController?.popViewController(animated: false)
        }) { (finished) in
        }
    }
    
    @IBAction func btnDoneClicked(_ sender: Any) {
        UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
            self.navigationController?.popViewController(animated: false)
        }) { (finished) in
            self.delegatePickupIntro?.backFromPickupIntro()
        }
    }
}
