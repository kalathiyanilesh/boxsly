//
//  PickupMenuVC.swift
//  BOXLY
//
//  Created by iMac on 22/03/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class PickupMenuVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnClose(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnoptions(_ sender: UIButton) {
        switch sender.tag {
        case 2, 3:
            NotificationCenter.default.post(name: Notification.Name(ShowPickUpOptionsVC), object: nil)
        default:
            break
        }
    }
}
