//
//  PickupOptionVC.swift
//  BOXLY
//
//  Created by iMac on 22/03/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class PickupOptionVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnClose(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnPickUp(_ sender: Any) {
        APP_DELEGATE.isFromPickup = true
        APP_DELEGATE.isFromDeliver = false
        //NotificationCenter.default.post(name: Notification.Name(SetScheduleDirectionText), object: nil)
        NotificationCenter.default.post(name: Notification.Name(ShowScheduleDeliveryVC), object: nil)
        self.btnClose(UIButton())
        //self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func btnDelivery(_ sender: Any) {
        APP_DELEGATE.isFromPickup = false
        APP_DELEGATE.isFromDeliver = true
        NotificationCenter.default.post(name: Notification.Name(ShowScheduleDeliveryVC), object: nil)
        self.btnClose(UIButton())
        /*
        let vc = loadVC(strStoryboardId: SB_SCHEDULE, strVCId: "ScheduleLocationVC") as! ScheduleLocationVC
        self.navigationController?.pushViewController(vc, animated: true)*/
    }

}
