//
//  AddressListVC.swift
//  BOXLY
//
//  Created by Dhaval on 13/01/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class AddressListVC: UIViewController {

    @IBOutlet weak var tblAddressList: UITableView!
    @IBOutlet weak var btnEditOriginalAdd: UIButton!
    @IBOutlet var viewContinue: UIView!
    @IBOutlet var heightContinue: NSLayoutConstraint!
    
    var isFromSchedule: Bool = false
    var sAddress: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblAddressList.delegate = self
        tblAddressList.dataSource = self
        if isFromSchedule {
            btnEditOriginalAdd.alpha = 0.74
            viewContinue.alpha = 0
            heightContinue.constant = 0
        }
    }

    @IBAction func btnEditOriginalAdd(_ sender: Any) {
        if isFromSchedule {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func btnContinueClicked(_ sender: Any) {
        let vc = loadVC(strStoryboardId: SB_MAIN, strVCId: "DropOffCalenderVC")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnBackClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension AddressListVC : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddressCell", for: indexPath) as! AddressCell
        if isFromSchedule {
            cell.btnConfirm.backgroundColor = Color_Hex(hex: "#8AA9C8")
            cell.btnConfirm.alpha = 0.28
        }
        cell.btnConfirm.addTarget(self, action: #selector(btnConfirm(_:)), for: .touchUpInside)
        cell.lblAddress.text = sAddress
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 139
    }
    
    @objc func btnConfirm(_ sender: UIButton) {
        if var viewControllers: [UIViewController] = self.navigationController?.viewControllers {
            viewControllers = viewControllers.reversed()
            for currentViewController in viewControllers {
                if currentViewController .isKind(of: ScheduleLocationVC.self) {
                    self.navigationController?.popToViewController(currentViewController, animated: true)
                    break
                }
            }
        }
    }
}

class AddressCell : UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var btnConfirm: UIButton!
}
