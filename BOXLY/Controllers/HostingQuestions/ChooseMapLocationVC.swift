//
//  ChooseMapLocationVC.swift
//  BOXLY
//
//  Created by Dhaval on 11/01/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import Pulsator
import CoreLocation

class ChooseMapLocationVC: UIViewController, GMSMapViewDelegate {

    @IBOutlet var mapView: GMSMapView!
    @IBOutlet var lblCurrentLocation: UILabel!
    
    var markerCurrent = GMSMarker()
    var circle = GMSCircle()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblCurrentLocation.text = "Not found"
        if LocationHandler.shared.asklocationPermission() {
            LocationHandler.shared.startUpdatingLocation()
        }
        LocationHandler.shared.delegate = self
        SetCurrentLocation()
    }
    
    @IBAction func backClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnNextClicjked(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        AddCircle(pdblLatitude: position.target.latitude, withLongitude: position.target.longitude)
        getAddressFromLatLon(pdblLatitude: position.target.latitude, withLongitude: position.target.longitude)
    }
    
    func SetCurrentLocation() {
        mapView.delegate = self
        let camera = GMSCameraPosition.camera(withLatitude: APP_DELEGATE.currentLat, longitude: APP_DELEGATE.currentLong, zoom: 15)
        mapView.camera = camera
        getAddressFromLatLon(pdblLatitude: APP_DELEGATE.currentLat, withLongitude: APP_DELEGATE.currentLong)
        runAfterTime(time: 1) {
            CATransaction.begin()
            CATransaction.setValue(NSNumber(value: 1.5 as Float), forKey: kCATransactionAnimationDuration)
            //self.mapView.animate(toZoom: 13)
            CATransaction.commit()
            runAfterTime(time: 0.5) {
                self.mapView.clear()
                self.AddCircle(pdblLatitude: APP_DELEGATE.currentLat, withLongitude: APP_DELEGATE.currentLong)
                let marker = GMSMarker()
                let location = CLLocation(latitude: APP_DELEGATE.currentLat, longitude: APP_DELEGATE.currentLong)
                marker.position = CLLocationCoordinate2DMake(location.coordinate.latitude,location.coordinate.longitude)
                marker.rotation = location.course
                let pulsator = Pulsator()
                pulsator.radius = 100
                pulsator.numPulse = 3
                pulsator.repeatCount = 3
                pulsator.backgroundColor = UIColor(red: 38/255.0, green: 147/255.0, blue: 238/255.0, alpha: 0.8).cgColor
                let view: UIView = UIView.init(frame: CGRect(x: 0, y: 0, width: 240, height: 240))
                view.backgroundColor = UIColor.clear
                marker.iconView = view
                marker.iconView?.layer.insertSublayer(pulsator, above: marker.iconView?.layer)
                pulsator.position = (marker.iconView?.layer.position)!
                pulsator.start()
                marker.map = self.mapView
                marker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
                self.markerCurrent = GMSMarker()
                self.markerCurrent.position = CLLocationCoordinate2DMake(location.coordinate.latitude,location.coordinate.longitude)
                self.markerCurrent.rotation = location.course
                self.markerCurrent.icon = UIImage(named: "ic_map_pin")
                self.markerCurrent.map = self.mapView
                self.markerCurrent.groundAnchor = CGPoint(x: 0.5, y: 0.5)
                runAfterTime(time: 5.8) {
                    marker.map = nil
                }
            }
        }
    }
    
    func AddCircle(pdblLatitude: Double, withLongitude pdblLongitude: Double) {
        circle.map = nil
        let circleCenter : CLLocationCoordinate2D  = CLLocationCoordinate2D(latitude: pdblLatitude, longitude: pdblLongitude)
        circle = GMSCircle(position: circleCenter, radius: 300)
        circle.fillColor = Color_Hex(hex: "#71B0F8").withAlphaComponent(0.55)
        circle.strokeColor = Color_Hex(hex: "#71B0F8")
        circle.strokeWidth = 2
        circle.map = mapView
    }
    
    func getAddressFromLatLon(pdblLatitude: Double, withLongitude pdblLongitude: Double) {

        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = pdblLatitude
        let lon: Double = pdblLongitude
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                let pm = placemarks! as [CLPlacemark]
                self.lblCurrentLocation.text = "Not found"
                if pm.count > 0 {
                    let pm = placemarks![0]
                    var addressString : String = ""
                    if pm.subLocality != nil {
                        addressString = addressString + pm.subLocality! + ", "
                    }
                    if pm.thoroughfare != nil {
                        addressString = addressString + pm.thoroughfare! + ", "
                    }
                    if pm.locality != nil {
                        addressString = addressString + pm.locality! + ", "
                    }
                    if pm.country != nil {
                        addressString = addressString + pm.country! + ", "
                    }
                    if pm.postalCode != nil {
                        addressString = addressString + pm.postalCode! + " "
                    }
                    if addressString.count > 0 {
                        self.lblCurrentLocation.text = addressString
                    }
              }
        })
    }
}

extension ChooseMapLocationVC : LocationUpdateProtocol {
    func locationDidUpdateToLocation(location: CLLocation) {
        APP_DELEGATE.currentLat = location.coordinate.latitude
        APP_DELEGATE.currentLong = location.coordinate.longitude
        let camera = GMSCameraPosition.camera(withLatitude: APP_DELEGATE.currentLat, longitude: APP_DELEGATE.currentLong, zoom: mapView.camera.zoom)
        markerCurrent.position = camera.target
    }
}
