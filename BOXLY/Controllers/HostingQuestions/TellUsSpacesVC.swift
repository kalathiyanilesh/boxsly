//
//  TellUsSpacesVC.swift
//  BOXLY
//
//  Created by Dhaval on 11/01/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class TellUsSpacesVC: UIViewController {
    let arrLocationType = ["Apartment", "House", "Secondary unit", "Hotel", "Your Small Business location"]
    let arrBoxType = ["Packages Only", "Small Boxes", "Large Boxes", "Large Furniture/ Appliances", "Unique Reques"]
    @IBOutlet var tableView : UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
    }
    
    @IBAction func btnBackClicked(sender : UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnNextClicked(sender : UIButton) {
        let vc = loadVC(strStoryboardId: SB_Hosting, strVCId: "TellUsSpacesVC2")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension TellUsSpacesVC : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TellUsSpacesOptionsCell", for: indexPath) as! TellUsSpacesOptionsCell
        if indexPath.row == 0 {
            cell.lblTitle.text = "First, lets narrow things down"
        }else {
            cell.lblTitle.text = "Now choose what are you willing to hold"
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0{
            let alertcontroller = UIAlertController(title: "Select One", message: nil, preferredStyle: .actionSheet)
            for item in arrLocationType {
                alertcontroller.addAction(UIAlertAction(title: item, style: .default, handler: { (action) in
                    
                }))
            }
            alertcontroller.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
                
            }))
            DispatchQueue.main.async {
                self.present(alertcontroller, animated: true, completion: nil)
            }
        }else {
            let alertcontroller = UIAlertController(title: "Select One", message: nil, preferredStyle: .actionSheet)
            for item in arrBoxType {
                alertcontroller.addAction(UIAlertAction(title: item, style: .default, handler: { (action) in
                    
                }))
            }
            alertcontroller.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
                
            }))
            DispatchQueue.main.async {
                self.present(alertcontroller, animated: true, completion: nil)
            }
        }
    }
}

class TellUsSpacesOptionsCell : UITableViewCell {
    @IBOutlet var lblTitle : UILabel!
    @IBOutlet var lblSelection : UILabel!
}
