//
//  AddPhotosVC.swift
//  BOXLY
//
//  Created by Dhaval on 11/01/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class AddPhotosVC: UIViewController {

    @IBOutlet weak var btnSkip: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        btnSkip.layer.borderWidth = 0.5
        btnSkip.layer.borderColor = Color_Hex(hex: "6EADF5").cgColor
        // Do any additional setup after loading the view.
    }
    @IBAction func btnBackClicked(sender : UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnNextClicked(sender : UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}
