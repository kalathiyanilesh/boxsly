//
//  TellUsSpacesVC2.swift
//  BOXLY
//
//  Created by Dhaval on 11/01/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class TellUsSpacesVC2: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    @IBAction func btnBackClicked(sender : UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnNextClicked(sender : UIButton) {
        let vc = loadVC(strStoryboardId: SB_Hosting, strVCId: "TellUsSpacesVC3")
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
