//
//  TakeLocationVC.swift
//  BOXLY
//
//  Created by Dhaval on 11/01/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

@objc protocol TableViewDelegate: NSObjectProtocol{

    func afterClickingReturnInTextField(cell: AddressFieldCell)
}

class AddressFieldCell : UITableViewCell, UITextFieldDelegate {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var textAdd: UITextField!
    weak var tableViewDelegate: TableViewDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        textAdd.delegate = self
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        tableViewDelegate?.responds(to: #selector(TableViewDelegate.afterClickingReturnInTextField(cell:)))
        tableViewDelegate?.afterClickingReturnInTextField(cell: self)
    }
}

class TakeLocationVC: UIViewController, LocationUpdateProtocol {

    var isForHosting = false
    var isForSchedule = false
    var isEdit: Bool = false
    let arrAddressFields = ["Country/ Region", "Street", "Apt, suite, etc. (optional)", "City", "State", "Zip / Postal Code"]
    var arrAddressValue: NSMutableArray = ["", "", "", "", "", ""]
    var objAddress: BAddress?
    
    @IBOutlet weak var tblAddress: UITableView!
    @IBOutlet weak var btnUserCurrentLocation: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblAddress.delegate = self
        tblAddress.dataSource = self
        tblAddress.tableFooterView = UIView()
        
        btnUserCurrentLocation.layer.borderWidth = 0.5
        btnUserCurrentLocation.layer.borderColor = UIColor.black.cgColor
    }
    
    @IBAction func btnUseCurrentLocationClicked(_ sender: UIButton) {
        /*
        let vc = loadVC(strStoryboardId: SB_Hosting, strVCId: "ChooseMapLocationVC")
        self.navigationController?.pushViewController(vc, animated: true)*/
        ShowLoadingAnimation(sender)
        if LocationHandler.shared.asklocationPermission() {
            LocationHandler.shared.startUpdatingLocation()
        }
        LocationHandler.shared.delegate = self
    }
    
    @IBAction func btnBackClicked(sender : UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnNextClicked(sender : UIButton) {
        if isForHosting {
            //self.navigationController?.popToRootViewController(animated: true)
            ShowLoadingAnimation(sender)
            runAfterTime(time: 1) {
                let vc = loadVC(strStoryboardId: SB_Hosting, strVCId: "ChooseMapLocationVC")
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } else {
            self.view.endEditing(true)
            let arrIndex = [3,1,4,0,5,2]
            var sAddress = ""
            for i in 0..<arrAddressValue.count {
                let address = arrAddressValue[arrIndex[i]] as! String
                if TRIM(string: address).count > 0 {
                    sAddress = TRIM(string: sAddress)
                    if sAddress.count > 0 {
                        sAddress = "\(sAddress), \(address)"
                    } else {
                        sAddress = address
                    }
                }
            }
            
            if sAddress.count > 0 {
                ShowLoadingAnimation(sender)
                let geoCoder = CLGeocoder()
                geoCoder.geocodeAddressString(sAddress) { (placemarks, error) in
                    guard
                        let placemarks = placemarks,
                        let location = placemarks.first?.location
                    else {
                        showMessage("Enter Valid Address")
                        return
                    }

                    runAfterTime(time: 1) {
                        let vc = loadVC(strStoryboardId: SB_ADDRESS, strVCId: "AddressListVC") as! AddressListVC
                        vc.isFromSchedule = self.isForSchedule
                        vc.sAddress = sAddress
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
            }
        }
    }
}

//MARK:- UITABLEVIEW DELEGATE
extension TakeLocationVC : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrAddressFields.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddressFieldCell", for: indexPath) as! AddressFieldCell
        cell.lblTitle.text = arrAddressFields[indexPath.row]
        cell.textAdd.placeholder = arrAddressFields[indexPath.row]
        cell.textAdd.text = arrAddressValue[indexPath.row] as? String
        cell.textAdd.tag = indexPath.row+555
        cell.tableViewDelegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85
    }
}

extension TakeLocationVC: TableViewDelegate {
    func afterClickingReturnInTextField(cell: AddressFieldCell) {
        arrAddressValue.replaceObject(at: cell.textAdd.tag-555, with: cell.textAdd.text ?? "")
    }
}

extension TakeLocationVC {
    func locationDidUpdateToLocation(location: CLLocation) {
        LocationHandler.shared.stopUpdatingLocation()
        getAddressFromLatLon(pdblLatitude: "\(location.coordinate.latitude)", withLongitude: "\(location.coordinate.longitude)")
    }
    
    func getAddressFromLatLon(pdblLatitude: String, withLongitude pdblLongitude: String) {
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = Double("\(pdblLatitude)")!
        let lon: Double = Double("\(pdblLongitude)")!
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                self.arrAddressValue = ["", "", "", "", "", ""]
                self.tblAddress.reloadData()
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                let pm = placemarks! as [CLPlacemark]

                if pm.count > 0 {
                    let pm = placemarks![0]
                    if pm.country != nil {
                        self.arrAddressValue.replaceObject(at: 0, with: pm.country ?? "")
                    }
                    if pm.subLocality != nil {
                        self.arrAddressValue.replaceObject(at: 1, with: pm.subLocality ?? "")
                    }
                    if pm.subThoroughfare != nil {
                        self.arrAddressValue.replaceObject(at: 2, with: pm.subThoroughfare! + ", ")
                    }
                    if pm.locality != nil {
                        self.arrAddressValue.replaceObject(at: 3, with: pm.locality ?? "")
                    }
                    if pm.administrativeArea != nil {
                        self.arrAddressValue.replaceObject(at: 4, with: pm.administrativeArea ?? "")
                    }
                    if pm.postalCode != nil {
                        self.arrAddressValue.replaceObject(at: 5, with: pm.postalCode ?? "")
                    }
              }
        })

    }
    
    
}



