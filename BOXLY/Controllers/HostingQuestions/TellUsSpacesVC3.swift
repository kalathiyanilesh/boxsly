//
//  TellUsSpacesVC3.swift
//  BOXLY
//
//  Created by Dhaval on 11/01/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class TellUsSpacesVC3: UIViewController {
    @IBOutlet var tableView : UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
    }
    @IBAction func btnBackClicked(sender : UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnNextClicked(sender : UIButton) {
        self.navigationController?.popToRootViewController(animated: true)
    }

}

extension TellUsSpacesVC3 : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SelectBoxCountCell", for: indexPath) as! SelectBoxCountCell
        if indexPath.row == 0 {
            cell.lblTitle.text = "Number of small boxes"
        }else {
            cell.lblTitle.text = "Number of large boxes"
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}

class SelectBoxCountCell : UITableViewCell {
    @IBOutlet var lblTitle : UILabel!
}
