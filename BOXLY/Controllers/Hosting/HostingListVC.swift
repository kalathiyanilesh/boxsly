//
//  HostingListVC.swift
//  BOXLY
//
//  Created by Dhaval on 10/01/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class HostingListVC: UIViewController {

    @IBOutlet weak var btnFilter: UIButton!
    @IBOutlet weak var tblHostingList: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tblHostingList.delegate = self
        tblHostingList.dataSource = self
        btnFilter.layer.borderColor = UIColor.gray.cgColor
    }
    @IBAction func btnbackclicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension HostingListVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "HostingListCell", for: indexPath) as! HostingListCell
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CreateNewHostingCell", for: indexPath) as! CreateNewHostingCell
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 211
        }else {
            return 45
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = loadVC(strStoryboardId: SB_Hosting, strVCId: "HostingMenuVC")
        let nav = UINavigationController(rootViewController: vc)
        nav.setNavigationBarHidden(true, animated: true)
        nav.modalPresentationStyle = .fullScreen
        self.present(nav, animated: true, completion: nil)
    }
}

class HostingListCell : UITableViewCell {
    @IBOutlet weak var imgCamera: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var pregressView: UIProgressView!
    @IBOutlet weak var completeLabel: UILabel!
}

class  CreateNewHostingCell : UITableViewCell {
    
}
