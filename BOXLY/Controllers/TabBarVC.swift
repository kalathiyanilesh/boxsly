//
//  TabBarVC.swift
//  BOXLY
//
//  Created by iMac on 16/01/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class TabBarVC: UITabBarController, UITabBarControllerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBar.sizeThatFits(CGSize(width: UIScreen.main.bounds.width, height: isIPhoneX() ? 100 : 70))
    
        self.delegate = self
        self.tabBar.isHidden = true
        
        if #available(iOS 13, *) {
            let appearance = UITabBarAppearance()
            appearance.stackedLayoutAppearance.normal.titleTextAttributes = [NSAttributedString.Key.foregroundColor: Color_Hex(hex: "#838383")]
            appearance.stackedLayoutAppearance.normal.iconColor = Color_Hex(hex: "#838383")
            tabBar.standardAppearance = appearance
        }
        if #available(iOS 13.0, *) {
            let appearance = tabBar.standardAppearance
            appearance.shadowImage = nil
            appearance.shadowColor = nil
            tabBar.standardAppearance = appearance;
        } else {
            tabBar.shadowImage = UIImage()
            tabBar.backgroundImage = UIImage()
        }

        self.tabBar.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.tabBar.layer.shadowRadius = 3
        self.tabBar.layer.shadowColor = UIColor.lightGray.cgColor
        self.tabBar.layer.shadowOpacity = 0.5
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
    }

    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        if self.viewControllers?.index(of: viewController) == 3 {
            self.tabBarController?.tabBar.isHidden = true
        }
        return true
    }
}

extension UITabBar {

    override public func sizeThatFits(_ size: CGSize) -> CGSize {
        super.sizeThatFits(size)
        var sizeThatFits = super.sizeThatFits(size)
        if isIPhoneX() {
            sizeThatFits.height = 100
        }else {
            sizeThatFits.height = 70
        }
        return sizeThatFits
    }

}


