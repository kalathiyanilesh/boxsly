//
//  SelectDeliveryTimeVC.swift
//  BOXLY
//
//  Created by iMac on 06/02/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class CellCalender: UICollectionViewCell {
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var lblBottomLine: UILabel!
}

class CellSelectDeliveryTime: UITableViewCell {
    @IBOutlet var imgSelection: UIImageView!
    @IBOutlet var lblTime: UILabel!
    @IBOutlet var lblPrice: UILabel!
}

protocol delegateSelectDeliveryTime {
    func SelectDeliveryDate()
}

class SelectDeliveryTimeVC: UIViewController {

    @IBOutlet var lblAddTitle: UILabel!
    @IBOutlet var segment: UISegmentedControl!
    @IBOutlet var viewChoose: UIView!
    @IBOutlet var heightViewChoose: NSLayoutConstraint!
    @IBOutlet var viewTop: UIView!
    @IBOutlet var tblTimes: UITableView!
    @IBOutlet var collTime: UICollectionView!
    
    var delegateSelectDeliveryTime: delegateSelectDeliveryTime?
    var selectedDate: Date?
    var arrDates = [Date]()
    var selectedIndex = -1
    var selectedDateIndex = 0
    var isFromChooseDelivey: Bool = false
    var arrTimes = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewChoose.alpha = 0
        heightViewChoose.constant = 0
        if APP_DELEGATE.isFromPickup {
            lblAddTitle.text = "Pickup at"
            segment.selectedSegmentIndex = 0
        } else {
            lblAddTitle.text = "Delivery to"
            segment.selectedSegmentIndex = 1
        }
        
        if selectedDate == nil {
            selectedDate = Date()
        }
        guard let MonthAgoDate = Calendar.current.date(byAdding: .month, value: 1, to: selectedDate!) else { return }
        arrDates = Date.dates(from: selectedDate!, to: MonthAgoDate)
        
        arrTimes = ["08:00 AM - 09:00 AM","09:00 AM - 10:00 AM","10:00 AM - 11:00 AM","11:00 AM - 12:00 PM","12:00 PM - 01:00 PM","01:00 PM - 02:00 PM","02:00 PM - 03:00 PM","03:00 PM - 04:00 PM","04:00 PM - 05:00 PM","05:00 PM - 06:00 PM","06:00 PM - 07:00 PM","07:00 PM - 08:00 PM"]
    }
    
    @IBAction func segment(_ sender: Any) {
        if segment.selectedSegmentIndex == 0 {
            lblAddTitle.text = "Pickup at"
        } else {
            lblAddTitle.text = "Delivery to"
        }
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnChange(_ sender: Any) {
        if APP_DELEGATE.isFromPickup {
            if var viewControllers: [UIViewController] = self.navigationController?.viewControllers {
                viewControllers = viewControllers.reversed()
                for currentViewController in viewControllers {
                    if currentViewController .isKind(of: ScheduleMapVC.self) {
                        self.navigationController?.popToViewController(currentViewController, animated: true)
                        break
                    }
                }
            }
        } else {
            let vc = loadVC(strStoryboardId: SB_Hosting, strVCId: "TakeLocationVC") as! TakeLocationVC
            vc.isForSchedule = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    @IBAction func btnChoose(_ sender: UIButton) {
        ShowLoadingAnimation(sender)
        runAfterTime(time: 1) {
            if self.isFromChooseDelivey {
                self.delegateSelectDeliveryTime?.SelectDeliveryDate()
                self.navigationController?.popViewController(animated: true)
            } else {
                let vc = loadVC(strStoryboardId: SB_SCHEDULE, strVCId: "ScheduleSummaryVC") as! ScheduleSummaryVC
                vc.screenIsFor = .pickup
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        /*
        let vc = loadVC(strStoryboardId: SB_BOOKING, strVCId: "BookingDetailsVC")
        self.navigationController?.pushViewController(vc, animated: true)*/
    }
}

//MARK:- UICOLLECTION VIEW DELEGATE
extension SelectDeliveryTimeVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrDates.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: CellCalender = collectionView.dequeueReusableCell(withReuseIdentifier: "CellCalender", for: indexPath) as! CellCalender
        
        let date: Date = arrDates[indexPath.row]
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        cell.lblTitle.text = dateFormatter.string(from: date)
        dateFormatter.dateFormat = "dd"
        cell.lblDate.text = dateFormatter.string(from: date)
        
        let calendar = NSCalendar.autoupdatingCurrent
        if calendar.isDateInToday(date) {
            cell.lblTitle.text = "Today"
        }
        if calendar.isDateInTomorrow(date) {
            cell.lblTitle.text = "Tomorrow"
        }
        if calendar.isDateInYesterday(date) {
            cell.lblTitle.text = "Yesterday"
        }
        if indexPath.row == selectedDateIndex {
            cell.lblTitle.textColor = Color_Hex(hex: "1D6DBE")
            cell.lblDate.textColor = Color_Hex(hex: "1D6DBE")
            cell.lblBottomLine.isHidden = false
        } else {
            cell.lblTitle.textColor = UIColor.black
            cell.lblDate.textColor = UIColor.black
            cell.lblBottomLine.isHidden = true
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedDateIndex = indexPath.row
        collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: SCREENWIDTH()/4, height: 51)
    }
}

//MARK:- UITABLEVIEW DELEGATE
extension SelectDeliveryTimeVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrTimes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CellSelectDeliveryTime = tableView.dequeueReusableCell(withIdentifier: "CellSelectDeliveryTime", for: indexPath) as! CellSelectDeliveryTime
        cell.imgSelection.image = UIImage(named: "ic_unselected")
        cell.lblTime.textColor = .black
        cell.lblPrice.textColor = .black
        if selectedIndex == indexPath.row {
            cell.imgSelection.image = UIImage(named: "ic_selected_blue")
            cell.lblTime.textColor = Color_Hex(hex: "#1D6DBE")
            cell.lblPrice.textColor = Color_Hex(hex: "#1D6DBE")
        }
        cell.lblTime.text = arrTimes.object(at: indexPath.row) as? String
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        /*
        viewChoose.alpha = 1
        heightViewChoose.constant = 81
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }*/
        UIView.performWithoutAnimation {
            tableView.reloadData()
        }
        
        runAfterTime(time: 0.3) {
            if self.isFromChooseDelivey {
                self.delegateSelectDeliveryTime?.SelectDeliveryDate()
                self.navigationController?.popViewController(animated: true)
            } else {
                let vc = loadVC(strStoryboardId: SB_SCHEDULE, strVCId: "ScheduleSummaryVC") as! ScheduleSummaryVC
                vc.screenIsFor = .pickup
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        
        //self.btnChoose(UIButton())
    }
}

extension Date {
    static func dates(from fromDate: Date, to toDate: Date) -> [Date] {
        var dates: [Date] = []
        var date = fromDate

        while date <= toDate {
            dates.append(date)
            guard let newDate = Calendar.current.date(byAdding: .day, value: 1, to: date) else { break }
            date = newDate
        }
        return dates
    }
}
