//
//  SuggestedRideVC.swift
//  BOXLY
//
//  Created by iMac on 07/02/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class CellSuggestedRide: UITableViewCell {
    
    @IBOutlet var viewContainer: UIView!
    @IBOutlet var imgWhite: UIImageView!
}

class SuggestedRideVC: UIViewController {

    @IBOutlet var tblSuggestedRides: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

//MARK: - UITABLEVIEW DELEGATE
extension SuggestedRideVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 15
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CellSuggestedRide = tableView.dequeueReusableCell(withIdentifier: "CellSuggestedRide", for: indexPath) as! CellSuggestedRide
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 101
    }
}
