//
//  BookingDetailsVC.swift
//  BOXLY
//
//  Created by iMac on 06/02/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import FloatingPanel
import GoogleMapsDirections

class BookingDetailsVC: UIViewController, LocationUpdateProtocol {
    func locationDidUpdateToLocation(location: CLLocation) {
        
    }

    @IBOutlet var mapView: GMSMapView!
    @IBOutlet var bottomBackButton: NSLayoutConstraint!
    @IBOutlet var bottomCurrentLoc: NSLayoutConstraint!
    var fpc: FloatingPanelController!
    var arrLatLong = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if LocationHandler.shared.asklocationPermission() {
            LocationHandler.shared.startUpdatingLocation()
        }
        LocationHandler.shared.delegate = self
        SetupFloatingView()
        SetupMapView()
        setupNavigation()
        AddTemporary()
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCurrentLoc(_ sender: Any) {
        mapView.moveCamera(GMSCameraUpdate.setTarget(LocationHandler.shared.currentLocation!.coordinate, zoom: mapView.camera.zoom))
    }
}

//MARK:- MAPVIEW SETUP
extension BookingDetailsVC: GMSMapViewDelegate {
    func SetupMapView() {
        mapView.delegate = self
        mapView.settings.allowScrollGesturesDuringRotateOrZoom = false
        mapView.mapStyle(withFilename: "boxly", andType: "json")
        mapView.paddingAdjustmentBehavior = .automatic
    }
    
    private func setupNavigation() {
        let origin = GoogleMapsDirections.Place.stringDescription(address: "Davis Center, Waterloo, Canada")
        let destination = GoogleMapsDirections.Place.stringDescription(address: "Conestoga Mall, Waterloo, Canada")

        GoogleMapsDirections.direction(fromOrigin: origin, toDestination: destination) { (response, error) -> Void in
          // Check Status Code
          guard response?.status == GoogleMapsDirections.StatusCode.ok else {
            // Status Code is Not OK
            debugPrint(response?.errorMessage ?? "")
            return
          }
          
          // Use .result or .geocodedWaypoints to access response details
          // response will have same structure as what Google Maps Directions API returns
          debugPrint("it has \(response?.routes.count ?? 0) routes")
        }
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        print("changed position: \(mapView.camera.target)")
    }
}

//MARK:- SET FLOATING VIEW
extension BookingDetailsVC: FloatingPanelControllerDelegate {
    func SetupFloatingView() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            self.fpc = FloatingPanelController()
            self.fpc.delegate = self
            self.fpc.surfaceView.backgroundColor = .clear
            self.fpc.surfaceView.shadowHidden = false
            self.fpc.surfaceView.containerView.layer.cornerRadius = 12.0
            self.fpc.surfaceView.grabberHandleWidth = 47
            self.fpc.surfaceView.grabberHandleHeight = 5
            self.fpc.surfaceView.grabberHandle.backgroundColor = Color_Hex(hex: "DBDBDB")
            self.fpc.addPanel(toParent: self)
            let vc = loadVC(strStoryboardId: SB_BOOKING, strVCId: "SuggestedRideVC") as! SuggestedRideVC
            self.fpc.set(contentViewController: vc)
            self.fpc.track(scrollView: vc.tblSuggestedRides)
            DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                self.fpc.move(to: .half, animated: true)
            }
        }
    }

    func floatingPanel(_ vc: FloatingPanelController, layoutFor newCollection: UITraitCollection) -> FloatingPanelLayout? {
        return SuggestedRiderLayout()
    }
    
    func floatingPanelDidMove(_ vc: FloatingPanelController) {
        let bottom = (vc.view.frame.size.height - vc.surfaceView.frame.origin.y)
        //mapView.padding = UIEdgeInsets(top: 0, left: 0, bottom: bottom, right: 0)
        bottomBackButton.constant = bottom
        bottomCurrentLoc.constant = bottom
    }
    
    func floatingPanelDidChangePosition(_ vc: FloatingPanelController) {
        if vc.position == .full {
            self.tabBarController?.tabBar.isHidden = false
            return
        }
        self.tabBarController?.tabBar.isHidden = true
        
        let position = (vc.layout.insetFor(position: vc.position) ?? 0)
        //self.mapView.padding = UIEdgeInsets(top: 0, left: 0, bottom: position, right: 0)
        bottomBackButton.constant = position
        bottomCurrentLoc.constant = position
        self.view.endEditing(true)
    }
    
    func AddTemporary() {
        arrLatLong.add(["lat":21.176542, "long":72.837639])
        arrLatLong.add(["lat":21.148732, "long":72.871854])
        arrLatLong.add(["lat":21.180544, "long":72.867898])
        arrLatLong.add(["lat":21.148817, "long":72.842296])
        arrLatLong.add(["lat":21.168845, "long":72.822168])
        arrLatLong.add(["lat":21.195296, "long":72.847681])

        for i in 0..<arrLatLong.count {
            let dict = arrLatLong[i] as! NSDictionary
            let marker = GMSMarker()
            marker.position = CLLocationCoordinate2DMake(dict["lat"] as! Double, dict["long"] as! Double)
            marker.icon = UIImage(named: "ic_map_car")
            marker.title = "\((dict["lat"] as! Double, dict["long"] as! Double))"
            marker.map = mapView
        }

        let camera = GMSCameraPosition.camera(withLatitude: 21.171549, longitude: 72.847617, zoom: 12)
        mapView.camera = camera
        DrawPath()
    }
    
    func DrawPath() {
        let origin = "\(21.168845),\(72.822168)"
       // let destination = "\(21.148732),\(72.871854)"
        let destination = "\(19.075983),\(72.877655)"

        let urlString = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving&key=\(kGOOGLE_PLACE)"

        let url = URL(string: urlString)
        URLSession.shared.dataTask(with: url!, completionHandler: {
            (data, response, error) in
            if(error != nil){
                print("error")
            }else{
                do{
                    let json = try JSONSerialization.jsonObject(with: data!, options:.allowFragments) as! [String : AnyObject]
                    let routes = json["routes"] as! NSArray
                   // self.mapView.clear()
                    print(json)
                    OperationQueue.main.addOperation({
                        for route in routes
                        {
                            let routeOverviewPolyline:NSDictionary = (route as! NSDictionary).value(forKey: "overview_polyline") as! NSDictionary
                            let points = routeOverviewPolyline.object(forKey: "points")
                            let path = GMSPath.init(fromEncodedPath: points! as! String)
                            let polyline = GMSPolyline.init(path: path)
                            polyline.strokeWidth = 3

                            let bounds = GMSCoordinateBounds(path: path!)
                            self.mapView!.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 30.0))

                            polyline.map = self.mapView

                        }
                    })
                }catch let error as NSError{
                    print("error:\(error)")
                }
            }
        }).resume()
    }
}
