//
//  ChooseDeliveryCarListVC.swift
//  BOXLY
//
//  Created by iMac on 05/03/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit
import Lottie

class ChooseDeliveryCarListVC: UIViewController {

    @IBOutlet var tblCarList: UITableView!
    @IBOutlet var viewDeliveryOn: UIView!
    @IBOutlet var viewSelectDelivery: UIView!
    @IBOutlet var btnCar: UIButton!
    @IBOutlet var imgCar: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewDeliveryOn.alpha = 0
    }
    
    @IBAction func btnDeliveryOn(_ sender: UIButton) {
        ShowLoadingAnimation(sender)
        runAfterTime(time: 1) {
            self.GoToUpcomingDelivery(self.viewDeliveryOn)
        }
    }
    
    @IBAction func btnSelectDelivery(_ sender: UIButton) {
        ShowLoadingAnimation(sender)
        runAfterTime(time: 1) {
            self.GoToUpcomingDelivery(self.viewSelectDelivery)
        }
    }
    
    func GoToUpcomingDelivery(_ view: UIView) {
        let animationView = AnimationView(name: "loading")
        animationView.frame = view.bounds
        animationView.contentMode = .scaleAspectFit
        animationView.loopMode = .repeat(1)
        view.addSubview(animationView)
        animationView.play()
        runAfterTime(time:1) {
            animationView.removeFromSuperview()
            let vc = loadVC(strStoryboardId: SB_SCHEDULE_MAP, strVCId: "UpcomingDeliveryVC")
            vc.modalPresentationStyle = .overCurrentContext
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    @IBAction func btnCar(_ sender: Any) {
        let vc = loadVC(strStoryboardId: SB_BOOKING, strVCId: "SelectDeliveryTimeVC") as! SelectDeliveryTimeVC
        vc.selectedDate = Date()
        vc.isFromChooseDelivey = true
        vc.delegateSelectDeliveryTime = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension ChooseDeliveryCarListVC: delegateSelectDeliveryTime {
    func SelectDeliveryDate() {
        viewDeliveryOn.alpha = 1
        viewSelectDelivery.alpha = 0
        btnCar.alpha = 0
        imgCar.alpha = 0
    }
}

//MARK: - UITABLEVIEW DELEGATE
extension ChooseDeliveryCarListVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CellSuggestedRide = tableView.dequeueReusableCell(withIdentifier: "CellSuggestedRide", for: indexPath) as! CellSuggestedRide
        if indexPath.row == 1 {
            //#000000
            cell.viewContainer.backgroundColor = UIColor.clear
            cell.imgWhite.isHidden = false
            cell.viewContainer.layer.borderWidth = 0
        } else {
            cell.imgWhite.isHidden = true
            cell.viewContainer.backgroundColor = Color_Hex(hex: "FFF7F7")
            cell.viewContainer.layer.borderWidth = 1
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 101
    }
}
