//
//  ScheduleMapTurnVC.swift
//  BOXLY
//
//  Created by iMac on 05/03/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class ScheduleMapTurnVC: UIViewController {

    @IBOutlet var mapView: GMSMapView!
    @IBOutlet var btnResume: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnResume.alpha = 0
        let camera = GMSCameraPosition.camera(withLatitude: APP_DELEGATE.currentLat, longitude: APP_DELEGATE.currentLong, zoom: 18)
        mapView.camera = camera
    }
    
    @IBAction func btnResume(_ sender: UIButton) {
        ShowLoadingAnimation(sender)
        runAfterTime(time: 1) {
            //self.navigationController?.popViewController(animated: true)
            APP_DELEGATE.GoToTabBar()
        }
    }
    
    @IBAction func btnEnd(_ sender: UIButton) {
        ShowLoadingAnimation(sender)
        runAfterTime(time: 1) {
            UIView.animate(withDuration: 0.35) {
                self.btnResume.alpha = 1
            }
        }
    }
}
