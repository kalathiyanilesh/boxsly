//
//  UpcomingDeliveryVC.swift
//  BOXLY
//
//  Created by iMac on 06/03/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class UpcomingDeliveryVC: UIViewController {

    @IBOutlet var bottomView: NSLayoutConstraint!
    @IBOutlet var viewBlack: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.bottomView.constant = -500
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        runAfterTime(time: 0.5) {
            self.viewBlack.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            runAfterTime(time:0.5) {
                UIView.animate(withDuration: 0.35) {
                    self.bottomView.constant = 0
                    UIView.animate(withDuration: 0.3) {
                        self.view.layoutIfNeeded()
                    }
                }
            }
        }
    }
    
    @IBAction func btnDone(_ sender: UIButton) {
        ShowLoadingAnimation(sender)
        runAfterTime(time: 1) {
            UIView.animate(withDuration: 0.35, animations: {
                self.bottomView.constant = -500
                UIView.animate(withDuration: 0.3) {
                    self.view.layoutIfNeeded()
                }
            }) { (finished) in
                runAfterTime(time: 0.5) {
                    self.viewBlack.backgroundColor = UIColor.clear
                    APP_DELEGATE.GoToTabBar()
                }
            }
        }
    }
}
