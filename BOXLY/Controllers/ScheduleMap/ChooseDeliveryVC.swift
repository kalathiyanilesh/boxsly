//
//  ChooseDeliveryVC.swift
//  BOXLY
//
//  Created by iMac on 05/03/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import FloatingPanel

class ChooseDeliveryVC: UIViewController, GMSMapViewDelegate {

    @IBOutlet var mapView: GMSMapView!
    var fpc: FloatingPanelController!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        mapView.delegate = self
        mapView.settings.allowScrollGesturesDuringRotateOrZoom = false
        mapView.mapStyle(withFilename: "boxly", andType: "json")
        mapView.paddingAdjustmentBehavior = .automatic
        let camera = GMSCameraPosition.camera(withLatitude: APP_DELEGATE.currentLat, longitude: APP_DELEGATE.currentLong, zoom: 18)
        mapView.camera = camera
        SetupFloatingView()
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

//MARK:- SET FLOATING VIEW
extension ChooseDeliveryVC: FloatingPanelControllerDelegate {
    func SetupFloatingView() {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            self.fpc = FloatingPanelController()
            self.fpc.delegate = self
            self.fpc.surfaceView.backgroundColor = .clear
            self.fpc.surfaceView.shadowHidden = false
            self.fpc.surfaceView.containerView.layer.cornerRadius = 12.0
            self.fpc.surfaceView.grabberHandleWidth = 47
            self.fpc.surfaceView.grabberHandleHeight = 5
            self.fpc.surfaceView.grabberHandle.backgroundColor = Color_Hex(hex: "DBDBDB")
            self.fpc.addPanel(toParent: self)
            let vc = loadVC(strStoryboardId: SB_SCHEDULE_MAP, strVCId: "ChooseDeliveryCarListVC") as! ChooseDeliveryCarListVC
            self.fpc.set(contentViewController: vc)
            self.fpc.track(scrollView: vc.tblCarList)
            DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                self.fpc.move(to: .half, animated: true)
            }
        }
    }

    func floatingPanel(_ vc: FloatingPanelController, layoutFor newCollection: UITraitCollection) -> FloatingPanelLayout? {
        return SuggestedCarLayout()
    }
    
    func floatingPanelDidMove(_ vc: FloatingPanelController) {
       
    }
    
    func floatingPanelDidChangePosition(_ vc: FloatingPanelController) {
        
    }
}
