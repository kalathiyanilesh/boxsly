//
//  LauncherVC.swift
//  BOXLY
//
//  Created by Dhaval on 15/01/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class LauncherVC: UIViewController {

    @IBOutlet weak var labelAppName: UILabel!
    @IBOutlet weak var zommingView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DispatchQueue.main.asyncAfter(deadline: .now()+1) {
            
            UIView.animate(withDuration: 0.4, animations: {
                let originalTransform = self.labelAppName.transform
                let scaledTransform = originalTransform.scaledBy(x: 15, y: 15)
                let scaledAndTranslatedTransform = scaledTransform.translatedBy(x: 0.0 , y: 0.0)
                self.labelAppName.transform = scaledAndTranslatedTransform
                self.labelAppName.contentScaleFactor = 10
                self.labelAppName.center = CGPoint(x: self.view.center.x+60, y: self.view.center.y-100)
            }) { (done) in
                self.labelAppName.isHidden = true
                self.zommingView.isHidden = false

                UIView.animate(withDuration: 0.4, animations: {
                    let originalTransform = self.zommingView.transform
                    let scaledTransform = originalTransform.scaledBy(x: 50, y: 50)
                    let scaledAndTranslatedTransform = scaledTransform.translatedBy(x: 0.0 , y: 0.0)
                    self.zommingView.transform = scaledAndTranslatedTransform
                    self.zommingView.contentScaleFactor = 10
                    self.zommingView.center = CGPoint(x: self.view.center.x, y: self.view.center.y)
                }) { (done) in
                    let vc = loadVC(strStoryboardId: SB_LOGIN, strVCId: idNavLogin)
                    APP_DELEGATE.window?.rootViewController = vc
                    APP_DELEGATE.window?.makeKeyAndVisible()
                    UIView.transition(with: APP_DELEGATE.window!,
                                      duration: 0.3,
                                      options: .transitionCrossDissolve,
                                      animations: nil,
                                      completion: nil)
                }
            }
        }
    }
}

