//
//  ScheduleMapVC.swift
//  BOXLY
//
//  Created by iMac on 28/01/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import FloatingPanel

class ScheduleMapVC: UIViewController {

    @IBOutlet var mapView: GMSMapView!
    @IBOutlet var bottomBtnBack: NSLayoutConstraint!
    var fpc: FloatingPanelController!
    var arrMarkers = NSMutableArray()
    var arrLatLong = NSMutableArray()
    var markerWindow : GMSMarker?

    override func viewDidLoad() {
        super.viewDidLoad()
        SetupFloatingView()
        SetupMapView()
        AddTemporary()
        SetUpMarker()
        if LocationHandler.shared.asklocationPermission() {
            LocationHandler.shared.startUpdatingLocation()
        }
        LocationHandler.shared.delegate = self
        SetCurrentLocation()
    }
    
    @IBAction func btnClose(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func SetUpMarker() {
        arrMarkers = NSMutableArray()
        for i in 0..<arrLatLong.count {
            let dict = arrLatLong[i] as! NSDictionary
            let marker = GMSMarker()
            marker.position = CLLocationCoordinate2DMake(dict["lat"] as! Double, dict["long"] as! Double)
            marker.icon = UIImage(named: "ic_pin_pink")
            marker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
            marker.map = mapView
            arrMarkers.add(marker)
        }

        let camera = GMSCameraPosition.camera(withLatitude: 33.013641, longitude: -96.687759, zoom: 15)
        mapView.camera = camera
    }
    
    /*
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        marker.tracksInfoWindowChanges = true
        let location = CLLocationCoordinate2D(latitude: marker.position.latitude, longitude: marker.position.longitude)
        let img = UIImageView()
        img.frame = CGRect(x: 0, y: 0, width: 62, height: 74)
        img.image = UIImage(named: "ic_windowinfo")
        img.center = mapView.projection.point(for: location)
        img.center.y = -150
        return img
    }*/
}

//MARK:- MAPVIEW SETUP
extension ScheduleMapVC: GMSMapViewDelegate {
    func SetupMapView() {
        mapView.delegate = self
        mapView.settings.allowScrollGesturesDuringRotateOrZoom = false
        mapView.mapStyle(withFilename: "boxly", andType: "json")
        mapView.paddingAdjustmentBehavior = .automatic
    }
}

//MARK:- SET FLOATING VIEW
extension ScheduleMapVC: FloatingPanelControllerDelegate {
    func SetupFloatingView() {
        fpc = FloatingPanelController()
        fpc.delegate = self
        fpc.surfaceView.backgroundColor = .clear
        fpc.surfaceView.shadowHidden = false
        fpc.surfaceView.containerView.layer.cornerRadius = 12.0
        fpc.surfaceView.grabberHandleWidth = 47
        fpc.surfaceView.grabberHandleHeight = 5
        fpc.surfaceView.grabberHandle.backgroundColor = Color_Hex(hex: "DBDBDB")
        fpc.contentMode = .fitToBounds
        fpc.addPanel(toParent: self)
        let vc = loadVC(strStoryboardId: SB_SCHEDULE, strVCId: "ScheduleLocationPickVC") as! ScheduleLocationPickVC
        vc.delegateScheduleDelegate = self
        fpc.set(contentViewController: vc)
        fpc.addPanel(toParent: self)
        fpc.track(scrollView: vc.tblList)
        DispatchQueue.main.asyncAfter(deadline: .now()+1) {
            self.fpc.move(to: .half, animated: true)
        }
    }

    func floatingPanel(_ vc: FloatingPanelController, layoutFor newCollection: UITraitCollection) -> FloatingPanelLayout? {
        return ScheduleMapLayout()
    }
    
    func floatingPanelDidMove(_ vc: FloatingPanelController) {
        let bottom = (vc.view.frame.size.height - vc.surfaceView.frame.origin.y)
        //self.mapView.padding = UIEdgeInsets(top: 0, left: 0, bottom: bottom, right: 0)
        bottomBtnBack.constant = bottom
    }
    
    func floatingPanelDidChangePosition(_ vc: FloatingPanelController) {
        var strPosition = ""
        if vc.position == .tip {
            strPosition = "tip"
        }
        let position = (vc.layout.insetFor(position: vc.position) ?? 0)
        bottomBtnBack.constant = position
        //self.mapView.padding = UIEdgeInsets(top: 0, left: 0, bottom: position, right: 0)
        NotificationCenter.default.post(name: Notification.Name("updatechoosehereposition"), object: strPosition)
    }
}

extension ScheduleMapVC: delegateScheduleDelegate {
    func GetSelectedAddress(_ index: Int) {
        let marker = arrMarkers[index] as? GMSMarker

        let camera = GMSCameraPosition.camera(withLatitude: marker?.position.latitude ?? 0, longitude: marker?.position.longitude ?? 0, zoom: 15)
        mapView.camera = camera
        
        AddBigMarker(marker!)
    }
    
    func AddBigMarker(_ marker: GMSMarker) {
        if markerWindow == nil {
            markerWindow = GMSMarker()
            markerWindow?.position = CLLocationCoordinate2DMake(marker.position.latitude,marker.position.longitude)
            markerWindow?.icon = UIImage(named: "ic_windowinfo")
            markerWindow?.map = self.mapView
        } else {
            markerWindow?.position = CLLocationCoordinate2DMake(marker.position.latitude,marker.position.longitude)
        }
    }
    
    func AddTemporary() {
        arrLatLong.add(["lat":33.013641, "long":-96.687759])
        arrLatLong.add(["lat":33.013401, "long":-96.683006])
        arrLatLong.add(["lat":33.013880, "long":-96.681460])
        arrLatLong.add(["lat":33.010910, "long":-96.683680])
        arrLatLong.add(["lat":33.010380, "long":-96.682020])
        arrLatLong.add(["lat":33.187150, "long":-96.966860])
        arrLatLong.add(["lat":33.204250, "long":-96.979510])
        arrLatLong.add(["lat":33.225640, "long":-96.989780])
        arrLatLong.add(["lat":33.227650, "long":-96.984190])
    }
}

extension ScheduleMapVC : LocationUpdateProtocol {
    func SetCurrentLocation() {
        mapView.delegate = self
        
        let location = CLLocation(latitude: APP_DELEGATE.currentLat, longitude: APP_DELEGATE.currentLong)
        let markerCurrent = GMSMarker()
        markerCurrent.position = CLLocationCoordinate2DMake(location.coordinate.latitude,location.coordinate.longitude)
        markerCurrent.rotation = location.course
        markerCurrent.icon = UIImage(named: "ic_map_pin")
        markerCurrent.map = self.mapView
        markerCurrent.groundAnchor = CGPoint(x: 0.5, y: 0.5)
    }
    
    func locationDidUpdateToLocation(location: CLLocation) {
        APP_DELEGATE.currentLat = location.coordinate.latitude
        APP_DELEGATE.currentLong = location.coordinate.longitude
    }
}
