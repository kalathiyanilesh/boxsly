//
//  EditWorkVC.swift
//  BOXLY
//
//  Created by iMac on 29/01/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class EditWorkVC: UIViewController {

    @IBOutlet var tblAddress: UITableView!
    @IBOutlet var viewSearch: UIView!
    @IBOutlet var viewTransparentDelete: UIView!
    @IBOutlet var viewDelete: UIView!
    @IBOutlet var bottomViewDelete: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewTransparentDelete.alpha = 0
        bottomViewDelete.constant = -248
        viewDelete.layer.cornerRadius = 5
        //viewDelete.roundCorners([.topLeft, .topRight], radius: 10)
    }
    
    @IBAction func btnEditWork(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCloseDelete(_ sender: Any) {
        UIView.animate(withDuration: 0.35, animations: {
            self.bottomViewDelete.constant = -248
            self.view.layoutIfNeeded()
        }) { (finished) in
            UIView.animate(withDuration: 0.3) {
                self.viewTransparentDelete.alpha = 0
            }
        }
    }
    
    @IBAction func btnDelete(_ sender: Any) {
        UIView.animate(withDuration: 0.25, animations: {
            self.viewTransparentDelete.alpha = 1
        }) { (finished) in
            UIView.animate(withDuration: 0.3) {
                self.bottomViewDelete.constant = 0
                self.view.layoutIfNeeded()
            }
        }
    }
    
    @IBAction func btnCancel(_ sender: Any) {
        
    }
    
    @IBAction func btnConfirm(_ sender: Any) {
        
    }
    
}

//MARK:- UITABLEVIEW DELEGATE
extension EditWorkVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CellScheduleLocation = tableView.dequeueReusableCell(withIdentifier: "CellScheduleLocation", for: indexPath) as! CellScheduleLocation
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 74
    }
}
