//
//  ScheduleSummaryVC.swift
//  BOXLY
//
//  Created by iMac on 31/01/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

enum screenIsFor : Int {
    case scheduling
    case pickup
}

class ScheduleSummaryVC: UIViewController {

    @IBOutlet var lblCheckInTime: UILabel!
    @IBOutlet var lblCheckOutTime: UILabel!
    @IBOutlet var lblItemSize: UILabel!
    var screenIsFor : screenIsFor = .scheduling
    override func viewDidLoad() {
        super.viewDidLoad()

//        if sCheckedInTime.count > 0 {
//            lblCheckInTime.text = sCheckedInTime
//        }
//        if sPickedUpTime.count > 0 {
//            lblCheckOutTime.text = sPickedUpTime
//        }
        if UserDefaults.standard.object(forKey: "selectedsize") != nil {
            lblItemSize.text = UserDefaults.standard.object(forKey: "selectedsize") as? String
        }
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
//        UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
//            self.dismiss(animated: false, completion: nil)
//        }, completion: nil)
    }
    
    @IBAction func btnContinue(_ sender: Any) {
        if screenIsFor == .scheduling {
            self.navigationController?.popToRootViewController(animated: false)
        } else {
            let vc = loadVC(strStoryboardId: SB_BOOKING, strVCId: "BookingDetailsVC")
            self.navigationController?.pushViewController(vc, animated: true)
        }
//        if screenIsFor == .scheduling {
//            self.navigationController?.popToRootViewController(animated: false)
//            NotificationCenter.default.post(name: Notification.Name(ShowScheduleDeliveryVC), object: nil)
//        }else{
//            APP_DELEGATE.GoToTabBar()
//        }
    }
    
}
