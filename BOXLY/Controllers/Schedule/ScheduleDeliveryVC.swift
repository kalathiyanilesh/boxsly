//
//  ScheduleDeliveryVC.swift
//  BOXLY
//
//  Created by Dhaval on 25/02/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class ScheduleDeliveryVC: UIViewController {
    @IBOutlet weak var tblOptions: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblOptions.delegate = self
        tblOptions.dataSource = self
        tblOptions.tableFooterView = UIView()
    }

    @IBAction func btnCloseClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension ScheduleDeliveryVC : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ScheduleDeliveryOptionCell", for: indexPath) as! ScheduleDeliveryOptionCell
        cell.contentView.isUserInteractionEnabled = false
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = loadVC(strStoryboardId: SB_MAIN, strVCId: "PickUpDeliveryVC")
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

class ScheduleDeliveryOptionCell : UITableViewCell {
    
}
