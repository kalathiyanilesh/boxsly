//
//  SchedulePopupVC.swift
//  BOXLY
//
//  Created by iMac on 04/02/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

protocol delegateSchedulePopup {
    func AddedDetails()
}

class SchedulePopupVC: UIViewController {

    @IBOutlet var viewDetails: UIView!
    @IBOutlet var bottomView: NSLayoutConstraint!
    @IBOutlet var viewDatePicker: UIView!
    @IBOutlet var datePicker: UIDatePicker!
    @IBOutlet var lblDayDate: UILabel!
    @IBOutlet var btnTime: UIButton!
    @IBOutlet var bottomDateView: NSLayoutConstraint!
    @IBOutlet var viewBlack: UIView!
    @IBOutlet var lblDateScheduleTile: UILabel!
    @IBOutlet weak var viewAddTimeFrame: UIView!
    @IBOutlet var lblMinuts: UILabel!
    
    var minutesToAdd = 0
    var isSelectDate: Bool = false
    var delegateSchedulePopup: delegateSchedulePopup?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewBlack.alpha = 0
        if isSelectDate {
            viewDetails.isHidden = true
            bottomDateView.constant = -600
            datePicker.minuteInterval = 30
            datePicker.addTarget(self, action: #selector(datePickerChanged(picker:)), for: .valueChanged)
            SetDateText()
            if APP_DELEGATE.isFromPickup {
                lblDateScheduleTile.text = "Schedule Pickup"
            }
        } else {
            viewDatePicker.isHidden = true
            bottomView.constant = -600
            SetUpView(viewDetails)
            viewDetails.backgroundColor = UIColor.black.withAlphaComponent(0.20)
        }
        self.viewBlack.alpha = 0
    }

    override func viewDidAppear(_ animated: Bool) {
        UIView.animate(withDuration: 0.3, animations: {
            self.viewBlack.alpha = 1
        }) { (finished) in
            if self.isSelectDate {
                self.bottomDateView.constant = 0
            } else {
                self.bottomView.constant = 0
            }
            UIView.animate(withDuration: 0.25) {
                self.view.layoutIfNeeded()
            }
        }
    
        if !isSelectDate {
            runAfterTime(time: 1.5) {
                UIView.animate(withDuration: 0.35, animations: {
                    self.bottomView.constant = -600
                    self.viewBlack.alpha = 0
                    self.view.layoutIfNeeded()
                }) { (finished) in
                    self.dismiss(animated: true, completion: nil)
                    self.delegateSchedulePopup?.AddedDetails()
                }
            }
        }
    }

    //MARK:- UIBUTTON ACTIONS
    @IBAction func btnPlus(_ sender: Any) {
        let min = Int(lblMinuts.text ?? "0")!
        lblMinuts.text = String(min+15)
    }
    
    @IBAction func btnMinus(_ sender: Any) {
        let min = Int(lblMinuts.text ?? "0")!
        if min == 0 { return }
        lblMinuts.text = String(min-15)
    }
    
    @IBAction func btnSetHrs(_ sender: UIButton) {
        ShowLoadingAnimation(sender)
        runAfterTime(time: 1) {
            self.minutesToAdd = Int(self.lblMinuts.text ?? "0") ?? 15
            self.SetDateText()
            self.btnCloseTimeFramePickerClicked(sender)
        }
    }
    
    @IBAction func btnCancelHrs(_ sender: Any) {
        self.btnCloseTimeFramePickerClicked(UIButton())
    }
    
    @IBAction func btnClose(_ sender: Any) {
        btnCancel(UIButton())
    }
    
    @IBAction func btnCancel(_ sender: UIButton) {
        UIView.animate(withDuration: 0.35, animations: {
            self.bottomDateView.constant = -600
            self.viewBlack.alpha = 0
            self.view.layoutIfNeeded()
        }) { (finished) in
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func btnSet(_ sender: UIButton) {
        UserDefaults.standard.set(sender.titleLabel?.text, forKey: "scheduleTime")
        UserDefaults.standard.synchronize()
        ShowLoadingAnimation(sender)
        runAfterTime(time: 1) {
            self.SetDateText()
            runAfterTime(time: 0.5) {
                UIView.animate(withDuration: 0.35, animations: {
                    self.bottomDateView.constant = -600
                    self.viewBlack.alpha = 0
                    self.view.layoutIfNeeded()
                }) { (finished) in
                    self.dismiss(animated: true) {
                        self.delegateSchedulePopup?.AddedDetails()
                    }
                }
            }
        }
    }

    @objc func datePickerChanged(picker: UIDatePicker) {
        SetDateText()
    }
    
    @IBAction func btnTimeClicked(_ sender: Any) {
        UIView.transition(with: view, duration: 0.4, options: .transitionCrossDissolve, animations: {
            self.viewAddTimeFrame.isHidden = false
        })
    }
    
    @IBAction func btnCloseTimeFramePickerClicked(_ sender: Any) {
        UIView.transition(with: view, duration: 0.4, options: .transitionCrossDissolve, animations: {
            self.viewAddTimeFrame.isHidden = true
        })
    }
}

extension SchedulePopupVC {
    func SetUpView(_ vwView: UIView) {
        vwView.backgroundColor = .clear
        let blurEffect = UIBlurEffect(style: .regular)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = vwView.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        vwView.insertSubview(blurEffectView, at: 0)
    }
    
    func SetDateText() {
        let formatter = DateFormatter()
        formatter.dateFormat = "EEE, MMM dd"
        lblDayDate.text = formatter.string(from: datePicker.date)
        formatter.dateFormat = "hh:mm a"
        btnTime.setTitle("\(formatter.string(from: datePicker.date)) - " + formatter.string(from: datePicker.date.addingTimeInterval(TimeInterval(60 * minutesToAdd))), for: .normal)
    }
}
