//
//  ScheduleInroVC.swift
//  BOXLY
//
//  Created by iMac on 03/02/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

protocol delegateScheduleIntro {
    func backFromScheduleIntro()
}

class ScheduleInroVC: UIViewController {

    var delegateScheduleIntro: delegateScheduleIntro?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UserDefaults.standard.set("1", forKey: "alreadyshowscheduleintro")
        UserDefaults.standard.synchronize()
    }
    
    @IBAction func btnImIn(_ sender: Any) {
        UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
            self.navigationController?.popViewController(animated: false)
        }) { (finished) in
            self.delegateScheduleIntro?.backFromScheduleIntro()
        }
    }
    
    @IBAction func btnBack(_ sender: Any) {
        UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
            self.navigationController?.popViewController(animated: false)
        }) { (finished) in
            self.delegateScheduleIntro?.backFromScheduleIntro()
        }
    }
}
