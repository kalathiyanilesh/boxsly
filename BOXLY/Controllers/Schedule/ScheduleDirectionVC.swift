//
//  ScheduleDirectionVC.swift
//  BOXLY
//
//  Created by iMac on 05/03/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit
import MapboxDirections
import MapboxCoreNavigation
import MapboxNavigation

class ScheduleDirectionCell: UITableViewCell {
    @IBOutlet var btnGo: UIButton!
    @IBOutlet var lblTime: UILabel!
    @IBOutlet var lblAddress: UILabel!
}

class ScheduleDirectionVC: UIViewController {

    @IBOutlet var lblTitleText: UILabel!
    @IBOutlet var tblOptions: UITableView!
    @IBOutlet var viewTotalCost: UIView!
    
    @IBOutlet var lblDeliveryCost: UILabel!
    @IBOutlet var lblDeliveryValue: UILabel!
    @IBOutlet var lblTotalCosetValue: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewTotalCost.alpha = 0
        SetScheduleDirectionTitleText()
        NotificationCenter.default.addObserver(self, selector: #selector(SetScheduleDirectionTitleText), name: Notification.Name(SetScheduleDirectionText), object: nil)
    }
    
    @objc func SetScheduleDirectionTitleText() {
        if APP_DELEGATE.isFromPickup {
            lblTitleText.text = "Schedule Pickup"
            lblDeliveryCost.text = "Service Fee"
        } else if APP_DELEGATE.isFromDeliver {
            lblTitleText.text = "Home Delivery"
            lblTotalCosetValue.text = "TBD"
            lblDeliveryValue.text = "TBD"
        }
        tblOptions.reloadData()
    }
    
    @IBAction func btnClose(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnDropDown(_ sender: Any) {
        let vc = loadVC(strStoryboardId: SB_MAIN, strVCId: "PickUpDeliveryVC")
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

//MARK:- UITableView Delegate
extension ScheduleDirectionVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: ScheduleDirectionCell = tableView.dequeueReusableCell(withIdentifier: "ScheduleDirectionCell", for: indexPath) as! ScheduleDirectionCell
        cell.contentView.isUserInteractionEnabled = false
        if APP_DELEGATE.isFromPickup || APP_DELEGATE.isFromDeliver {
            cell.btnGo.backgroundColor = Color_Hex(hex: "#64EB83")
            viewTotalCost.alpha = 1
        } else {
            cell.btnGo.backgroundColor = Color_Hex(hex: "#EDEDED")
            viewTotalCost.alpha = 0
        }
        if (UserDefaults.standard.object(forKey: "scheduledAddress") != nil) {
            cell.lblAddress.text = UserDefaults.standard.object(forKey: "scheduledAddress") as? String
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if APP_DELEGATE.isFromPickup {
            let vc = loadVC(strStoryboardId: SB_SCHEDULE, strVCId: "SchedulePopupVC") as! SchedulePopupVC
            vc.delegateSchedulePopup = self
            vc.isSelectDate = true
            vc.modalPresentationStyle = .fullScreen
            vc.modalPresentationStyle = .overCurrentContext
            self.present(vc, animated: true, completion: nil)
        } else if APP_DELEGATE.isFromDeliver {
            let vc = loadVC(strStoryboardId: SB_SCHEDULE_MAP, strVCId: "ChooseDeliveryVC")
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            let vc = loadVC(strStoryboardId: SB_MAIN, strVCId: "PickUpDeliveryVC")
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 131
    }
}

extension ScheduleDirectionVC: delegateSchedulePopup {
    func AddedDetails() {
        //temporary
        
        let origin = Waypoint(coordinate: CLLocationCoordinate2D(latitude: 21.2408, longitude: 72.8806), name: "Surat")
        let destination = Waypoint(coordinate: CLLocationCoordinate2D(latitude: 23.0225, longitude: 72.5714), name: "Ahemdabad")
        let options = NavigationRouteOptions(waypoints: [origin, destination])

        Directions.shared.calculate(options) { (waypoints, routes, error) in
            guard let route = routes?.first else { return }
         
            let viewController = NavigationViewController(for: route)
            viewController.modalPresentationStyle = .fullScreen
            self.present(viewController, animated: true, completion: nil)
        }
        
        /*
        let vc = loadVC(strStoryboardId: SB_SCHEDULE_MAP, strVCId: "ScheduleMapTurnVC")
        self.navigationController?.pushViewController(vc, animated: true)*/
    }
}
