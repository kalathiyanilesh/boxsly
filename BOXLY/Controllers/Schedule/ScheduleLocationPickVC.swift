//
//  ScheduleLocationPickVC.swift
//  BOXLY
//
//  Created by iMac on 28/01/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit
import Lottie

protocol delegateScheduleDelegate {
    func GetSelectedAddress(_ index: Int)
}

class ScheduleLocationPickVC: UIViewController {

    var selectedIndex = -1
    var arrAddress = NSMutableArray()
    var delegateScheduleDelegate: delegateScheduleDelegate?
    
    @IBOutlet var tblList: UITableView!
    @IBOutlet var bottomChooseHere: NSLayoutConstraint!
    @IBOutlet var btnSchedule: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bottomChooseHere.constant = -500
        btnSchedule.alpha = 0
        NotificationCenter.default.addObserver(self, selector: #selector(UpdateChooseHere(_ :)), name: Notification.Name("updatechoosehereposition"), object: nil)
        
        arrAddress.add("1808 10th St Suite 100, Plano, TX 75074")
        arrAddress.add("1101 Jupiter Rd, Plano, TX 75074")
        arrAddress.add("1100 Jupiter Rd #170, Plano, TX 75074")
        arrAddress.add("901 Jupiter Rd, Plano, TX 75074")
        arrAddress.add("900 Jupiter Rd, Plano, TX 75074")
        arrAddress.add("3500 Wildridge Blvd, Oak Point, TX 75068")
        arrAddress.add("8924 King Ranch Dr, Cross Roads, TX 76227")
        arrAddress.add("10001 US-380, Cross Roads, TX 76227")
        arrAddress.add("11700 US-380, Cross Roads, TX 76227")
    }
    
    @IBAction func btnSchedule(_ sender: UIButton) {
        let sAddress = arrAddress[selectedIndex] as? String
        UserDefaults.standard.set(sAddress, forKey: "scheduledAddress")
        UserDefaults.standard.synchronize()
        ShowLoadingAnimation(sender)
        runAfterTime(time: 1) {
            let vc = loadVC(strStoryboardId: SB_SELECT_TIMES, strVCId: "ReviewPurchaseVC") as! ReviewPurchaseVC
            vc.screenIsFor = .scheduling
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func btnChooseHere(_ sender: UIButton) {
        /*
        let vc = loadVC(strStoryboardId: SB_SCHEDULE, strVCId: "ScheduleSummaryVC") as! ScheduleSummaryVC
        vc.screenIsFor = .scheduling
        self.navigationController?.pushViewController(vc, animated: true)*/
        
        ShowLoadingAnimation(sender)
        runAfterTime(time: 1) {
            let vc = loadVC(strStoryboardId: SB_SELECT_TIMES, strVCId: "ReviewPurchaseVC") as! ReviewPurchaseVC
            vc.screenIsFor = .scheduling
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @objc func UpdateChooseHere(_ sender: NSNotification) {
        if sender.object as! String == "tip" {
            bottomChooseHere.constant = -500
            UIView.animate(withDuration: 0.35) {
                self.view.layoutIfNeeded()
            }
        } else if selectedIndex != -1 && bottomChooseHere.constant != 0 {
            bottomChooseHere.constant = 0
            UIView.animate(withDuration: 0.35) {
                self.view.layoutIfNeeded()
            }
        }
    }
}

extension ScheduleLocationPickVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrAddress.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CellDropPickSize = tableView.dequeueReusableCell(withIdentifier: "CellDropPickSize", for: indexPath) as! CellDropPickSize
        cell.imgSelection.image = UIImage(named: "ic_unselected")
        cell.lblAddress.text = arrAddress[indexPath.row] as? String
        
        let animationView = AnimationView(name: "favourite")
        animationView.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        animationView.contentMode = .scaleAspectFit
        animationView.loopMode = .playOnce
        cell.viewFavAnim.addSubview(animationView)
        
        if selectedIndex == indexPath.row {
            cell.imgSelection.image = UIImage(named: "ic_selected_blue")
            animationView.play()
        }

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension //68
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        tblList.reloadData()
        self.delegateScheduleDelegate?.GetSelectedAddress(indexPath.row)
        if self.btnSchedule.alpha == 0 {
            runAfterTime(time: 0.2) {
                self.btnSchedule.transform = CGAffineTransform(scaleX: 0.2, y: 0.2)
                UIView.animate(withDuration: 0.3) {
                    self.btnSchedule.alpha = 1
                    UIView.animate(withDuration: 0.35,
                    animations: {
                        self.btnSchedule.transform = CGAffineTransform(scaleX: 1, y: 1)
                    },
                    completion: { _ in
                    })
                }
                /*
                if self.bottomChooseHere.constant != 0 {
                    self.bottomChooseHere.constant = 0
                    UIView.animate(withDuration: 0.35) {
                        self.view.layoutIfNeeded()
                    }
                }*/
            }
        }
    }
}
