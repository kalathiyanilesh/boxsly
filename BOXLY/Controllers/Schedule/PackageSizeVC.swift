//
//  PackageSizeVC.swift
//  BOXLY
//
//  Created by iMac on 04/02/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit
import VerticalSlider
import MTCircularSlider

class PackageSizeVC: UIViewController {

    @IBOutlet weak var viewBoxQuantity: UIView!
    @IBOutlet var imgSize: UIImageView!
    @IBOutlet var sliderRound: MTCircularSlider!
    @IBOutlet var trailingSlider: NSLayoutConstraint!
    @IBOutlet var trailingSizesView: NSLayoutConstraint!
    @IBOutlet weak var lblNumberOfBoxes: UILabel!
    
    var isFromScheduleIntro: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imgSize.isHidden = true
        sliderRound.value = 0.12
        UserDefaults.standard.set("Small", forKey: "selectedsize")
        UserDefaults.standard.synchronize()

        if SCREENHEIGHT() == 667 {
            trailingSlider.constant = -95
            trailingSizesView.constant = 35
        } else if SCREENHEIGHT() == 736 {
           trailingSlider.constant = -55
            trailingSizesView.constant = 40
        } else if SCREENHEIGHT() == 812 {
            trailingSlider.constant = -50
            trailingSizesView.constant = 35
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        imgSize.isHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        imgSize.isHidden = true
    }

    @IBAction func btnBack(_ sender: Any) {
        if isFromScheduleIntro {
            if var viewControllers: [UIViewController] = self.navigationController?.viewControllers {
                viewControllers = viewControllers.reversed()
                for currentViewController in viewControllers {
                    if currentViewController .isKind(of: TabBarVC.self) {
                        self.navigationController?.popToViewController(currentViewController, animated: true)
                        break
                    }
                }
            }
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func sliderValue(_ sender: Any) {
        for i in 1..<5 {
            let lbl: UILabel = self.view.viewWithTag(i) as! UILabel
            lbl.textColor = Color_Hex(hex: "3D3D3D")
            lbl.font = FontWithSize("AirbnbCerealApp-Bold", 17)
        }
        var currentSize = 1
        if sliderRound.value >= 0 && sliderRound.value < 0.25 {
            currentSize = 1
        } else if sliderRound.value > 0.25 && sliderRound.value < 0.50 {
            currentSize = 2
        } else if sliderRound.value > 0.50 && sliderRound.value < 0.75 {
            currentSize = 3
        } else {
            currentSize = 4
        }
        
        var sSize = "Small"
        switch currentSize {
        case 2:
            sSize = "Medium"
            UIView.transition(with: imgSize, duration: 0.3, options: .transitionCrossDissolve, animations: { [weak self] in
                self?.imgSize.image = #imageLiteral(resourceName: "ic_medium")
            }, completion: nil)
        case 3:
            sSize = "Large"
            UIView.transition(with: imgSize, duration: 0.3, options: .transitionCrossDissolve, animations: { [weak self] in
                self?.imgSize.image = #imageLiteral(resourceName: "ic_large")
            }, completion: nil)
        case 4:
            sSize = "X - Large"
            UIView.transition(with: imgSize, duration: 0.3, options: .transitionCrossDissolve, animations: { [weak self] in
                self?.imgSize.image = #imageLiteral(resourceName: "ic_other")
            }, completion: nil)
        default:
            UIView.transition(with: imgSize, duration: 0.3, options: .transitionCrossDissolve, animations: { [weak self] in
                self?.imgSize.image = #imageLiteral(resourceName: "ic_small")
            },completion: nil)
            break
        }
        UserDefaults.standard.set(sSize, forKey: "selectedsize")
        UserDefaults.standard.synchronize()
        let lbl = self.view.viewWithTag(currentSize) as! UILabel
        lbl.font = FontWithSize("AirbnbCerealApp-ExtraBold", 22)
        UIView.transition(with: lbl, duration: 0.3, options: .transitionCrossDissolve, animations: {
            lbl.textColor = Color_Hex(hex: "1D6DBE")
        },
        completion: nil)
    }
    
    @IBAction func btnAddBox(_ sender: UIButton) {
        self.lblNumberOfBoxes.text = "\(Int(lblNumberOfBoxes.text!)! + 1)"
    }
    
    @IBAction func btnLessBox(_ sender: UIButton) {
        if Int(lblNumberOfBoxes.text!)! > 0 {
            self.lblNumberOfBoxes.text = "\(Int(lblNumberOfBoxes.text!)! - 1)"
        }
    }
    
    @IBAction func btnNext(_ sender: UIButton) {
        ShowLoadingAnimation(sender)
        runAfterTime(time: 1) {
            UIView.transition(with: self.view, duration: 0.4, options: .transitionCrossDissolve, animations: {
                self.viewBoxQuantity.isHidden = false
            })
        }
    }
    
    @IBAction func btnCancel(_ sender: UIButton) {
        self.viewBoxQuantity.isHidden = true
    }
    
    @IBAction func btnSelectQuantityNextClicked(_ sender: UIButton) {
        ShowLoadingAnimation(sender)
        runAfterTime(time: 1) {
            UIView.transition(with: self.view, duration: 0.4, options: .transitionCrossDissolve, animations: {
                self.viewBoxQuantity.isHidden = true
            })
            /*
            let vc = loadVC(strStoryboardId: SB_SCHEDULE, strVCId: "SchedulePopupVC") as! SchedulePopupVC
            vc.delegateSchedulePopup = self
            vc.modalPresentationStyle = .fullScreen
            vc.modalPresentationStyle = .overCurrentContext
            self.present(vc, animated: true, completion: nil)*/
            self.AddedDetails()
        }
    }
    
    @IBAction func btnClosePackageQuantityView(_ sender: Any) {
        UIView.transition(with: view, duration: 0.4, options: .transitionCrossDissolve, animations: {
            self.viewBoxQuantity.isHidden = true
        })
    }
}

extension PackageSizeVC: delegateSchedulePopup {
    func AddedDetails() {
        APP_DELEGATE.isFromPickup = true
        let vc = loadVC(strStoryboardId: SB_SCHEDULE, strVCId: "ScheduleMapVC")
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
