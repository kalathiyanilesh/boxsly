//
//  ScheduleLocationVC.swift
//  BOXLY
//
//  Created by iMac on 28/01/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class CellScheduleLocation: UITableViewCell {
    @IBOutlet var lblMainAdd: UILabel!
    @IBOutlet var lblStreetAdd: UILabel!
    @IBOutlet var btnEdit: UIButton!
}

class CellScheduleCurrentLoc: UITableViewCell {
    @IBOutlet var imgLoc: UIImageView!
    @IBOutlet var lblName: UILabel!
}

class ScheduleLocationVC: UIViewController, UITextFieldDelegate {

    @IBOutlet var txtWhereTo: UITextField!
    @IBOutlet var lblWhereDeliveryLoc: UILabel!
    @IBOutlet var viewStep: UIView!
    @IBOutlet var viewTop: UIView!
    @IBOutlet var imgBlankLoc: UIImageView!
    @IBOutlet var imgLocSelect: UIImageView!
    @IBOutlet var viewWhereTo: UIView!
    @IBOutlet var tblAddress: UITableView!
    @IBOutlet var heightStepView: NSLayoutConstraint!
    
    var arrAddress = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        imgBlankLoc.image = UIImage(named: "ic_location_pink")?.withRenderingMode(.alwaysTemplate)
        imgBlankLoc.tintColor = Color_Hex(hex: "#748BB7")
        viewTop.layer.shadowColor = Color_Hex(hex: "#B4B4B4").withAlphaComponent(0.11).cgColor
        viewTop.layer.shadowOpacity = 1
        viewTop.layer.shadowOffset = .zero
        viewTop.layer.shadowRadius = 6
        viewStep.alpha = 0
        heightStepView.constant = 0
        imgLocSelect.alpha = 0
        txtWhereTo.addTarget(self, action: #selector(searchAdd), for: .editingChanged)
        txtWhereTo.delegate = self
    }
    
    @objc func searchAdd(textField:UITextField){
        if TRIM(string: txtWhereTo.text!).count > 0 {
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(apiGetAddress), object: nil)
            self.perform(#selector(apiGetAddress), with: nil, afterDelay: 1)
        } else {
            arrAddress.removeAllObjects()
            tblAddress.reloadData()
        }
    }
    
    @IBAction func btnClose(_ sender: Any) {
        APP_DELEGATE.GoToTabBar()
    }
    
    @IBAction func btnClear(_ sender: Any) {
        if viewStep.alpha == 1 {
            UIView.animate(withDuration: 0.5) {
                self.lblWhereDeliveryLoc.alpha = 1
                self.viewStep.alpha = 0
                self.imgBlankLoc.alpha = 1
                self.imgLocSelect.alpha = 0
                self.heightStepView.constant = 0
                self.view.layoutIfNeeded()
            }
        }
        txtWhereTo.text = ""
        viewWhereTo.layer.shadowOpacity = 0
        self.view.endEditing(true)
        apiGetAddress()
    }
    
    @IBAction func btnStep3(_ sender: UIButton) {
        /*
        let vc = loadVC(strStoryboardId: SB_SCHEDULE, strVCId: "ScheduleMapVC")
        self.navigationController?.pushViewController(vc, animated: true)*/

        ShowLoadingAnimation(sender)
        runAfterTime(time: 1) {
            NotificationCenter.default.post(name: Notification.Name(SetScheduleDirectionText), object: nil)
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
}

//MARK:- UITABLEVIEW DELEGATE
extension ScheduleLocationVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrAddress.count > 0 ? arrAddress.count : 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //CellScheduleLocation, CellScheduleCurrentLoc
        if arrAddress.count > 0 {
            let cell: CellScheduleLocation = tableView.dequeueReusableCell(withIdentifier: "CellScheduleLocation", for: indexPath) as! CellScheduleLocation
            let objAddress: BAddress = arrAddress[indexPath.row] as! BAddress
            let address: StructuredFormatting = objAddress.structuredFormatting!
            cell.lblMainAdd.text = address.mainText
            cell.lblStreetAdd.text = address.secondaryText
            cell.btnEdit.tag = indexPath.row+5
            cell.btnEdit.addTarget(self, action: #selector(btnEditAdd(_:)), for: .touchUpInside)
            return cell
        } else {
            let cell: CellScheduleCurrentLoc = tableView.dequeueReusableCell(withIdentifier: "CellScheduleCurrentLoc", for: indexPath) as! CellScheduleCurrentLoc
            switch indexPath.row {
            case 0:
                cell.lblName.text = "Work"
                cell.imgLoc.image = UIImage(named: "ic_set_on_map")
            case 1:
                cell.lblName.text = "Current Location"
                cell.imgLoc.image = UIImage(named: "ic_current_location_big")
            case 2:
                cell.lblName.text = "Set on map"
                cell.imgLoc.image = UIImage(named: "ic_set_on_map")
            default:
                break
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.view.endEditing(true)
        if arrAddress.count <= 0 {
            if viewStep.alpha == 0 {
                UIView.animate(withDuration: 0.5) {
                    self.viewStep.alpha = 1
                    self.heightStepView.constant = 98
                    self.view.layoutIfNeeded()
                }
            }
        } else {
            if viewStep.alpha == 0 {
                UIView.animate(withDuration: 0.5) {
                    self.lblWhereDeliveryLoc.alpha = 0
                    self.viewStep.alpha = 1
                    self.imgBlankLoc.alpha = 0
                    self.imgLocSelect.alpha = 1
                    self.heightStepView.constant = 98
                    self.view.layoutIfNeeded()
                }
            }
            
            let objAddress: BAddress = arrAddress[indexPath.row] as! BAddress
            let address: StructuredFormatting = objAddress.structuredFormatting!
            txtWhereTo.text = address.mainText
        }

        viewWhereTo.layer.shadowColor = Color_Hex(hex: "#748BB7").cgColor
        viewWhereTo.layer.shadowOpacity = 1
        viewWhereTo.layer.shadowOffset = .zero
        viewWhereTo.layer.shadowRadius = 4
        viewWhereTo.maskToBounds = false
        viewWhereTo.layer.shadowOpacity = 0.5
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return arrAddress.count > 0 ? UITableView.automaticDimension : 74
    }
    
    @objc func btnEditAdd(_ sender: UIButton) {
        let vc = loadVC(strStoryboardId: SB_Hosting, strVCId: "TakeLocationVC") as! TakeLocationVC
        vc.isEdit = true
        vc.isForSchedule = true
        vc.objAddress = arrAddress[sender.tag-5] as? BAddress
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

//MARK: API CALLING
extension ScheduleLocationVC {
    @objc func apiGetAddress() {
        let service = "https://maps.googleapis.com/maps/api/place/autocomplete/json?key=\(kGOOGLE_PLACE)&types=address&input=\(TRIM(string: txtWhereTo.text!))"
        HttpRequestManager.sharedInstance.getRequest(
            endpointurl: service,
            parameters: NSDictionary()) { (responseObj, error) in
                print(responseObj ?? "")
                self.arrAddress.removeAllObjects()
                if responseObj?.object(forKey: "predictions") != nil {
                    let arrPredication: NSArray = responseObj?.object(forKey: "predictions") as! NSArray
                    for i in 0..<arrPredication.count {
                        let objAdd: BAddress = BAddress.init(object: arrPredication[i] as! NSDictionary)
                        self.arrAddress.add(objAdd)
                    }
                }
                self.tblAddress.reloadData()
        }
    }
}
