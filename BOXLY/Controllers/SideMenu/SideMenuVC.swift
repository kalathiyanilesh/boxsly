//
//  SideMenuVC.swift
//  BOXLY
//
//  Created by Dhaval on 15/01/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit
import RESideMenu
import Stripe

class SideMenuVC: UIViewController {
    @IBOutlet weak var tblMenu: UITableView!
    @IBOutlet var lblUserName: UILabel!
    @IBOutlet var imgUser: UIImageView!
    
    var arrMenu = ["Drop off", "Payments", "Host", "Switch to Driver", "No Message", "Help"]
    var arrImg = [#imageLiteral(resourceName: "Icon feather-droplet"),#imageLiteral(resourceName: "Icon feather-credit-card"),#imageLiteral(resourceName: "Icon feather-home"),#imageLiteral(resourceName: "Icon material-swap-calls"),#imageLiteral(resourceName: "Icon feather-message-square"),#imageLiteral(resourceName: "Icon ionic-ios-help-circle-outline")]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblMenu.delegate = self
        tblMenu.dataSource = self
        if (UserDefaults.standard.object(forKey: UD_USERNAME) != nil) {
            lblUserName.text = UserDefaults.standard.object(forKey: UD_USERNAME) as? String
        }
        
        if (UserDefaults.standard.object(forKey: UD_USERIMAGE) != nil) {
            let sProfilePic = UserDefaults.standard.object(forKey: UD_USERIMAGE) as? String ?? ""
            imgUser.sd_setImage(with: URL(string: sProfilePic), placeholderImage: nil)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tblMenu.reloadData()
    }
    @IBAction func btnLogoutClicked(_ sender: Any) {
        UserDefaults.standard.set(false, forKey: IS_LOGGED_IN)
        APP_DELEGATE.setupInitials()
    }
    @IBAction func btnProfile(_ sender: Any) {
        let vc = loadVC(strStoryboardId: SB_MAIN, strVCId: "CameraVC")
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    @IBAction func btnSettingClicked(_ sender: Any) {
        let vc = loadVC(strStoryboardId: SB_DOCUMENT, strVCId: "DeliveryWelcomeVC") as! DeliveryWelcomeVC
        vc.isDriver = true
        APP_DELEGATE.drawer?.hideViewController()
        (APP_DELEGATE.drawer?.contentViewController as! UINavigationController).pushViewController(vc, animated: true)
    }
}

//MARK:- UITABLEVIEW DELEGATE
extension SideMenuVC : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMenu.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellSideMenu", for: indexPath) as! CellSideMenu
        cell.lblMenu.text = arrMenu[indexPath.row]
        cell.imgIconMenu.image = arrImg[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if arrMenu[indexPath.row] == "Payments" {
            showPaymentOption()
        }
    }
    
    private func showPaymentOption() {
        let config = STPPaymentConfiguration()
        config.additionalPaymentOptions = [.default, .FPX]
        config.requiredBillingAddressFields = .none
        config.appleMerchantIdentifier = "dummy-merchant-id"
        let viewController = STPPaymentOptionsViewController(configuration: config, theme: STPTheme.default(), customerContext: MockCustomerContext(), delegate: self)
        viewController.apiClient = MockAPIClient()
        let navigationController = UINavigationController(rootViewController: viewController)
        navigationController.navigationBar.stp_theme = STPTheme.default()
        present(navigationController, animated: true, completion: nil)
    }
}

class CellSideMenu : UITableViewCell {
    @IBOutlet weak var lblMenu: UILabel!
    @IBOutlet weak var  imgIconMenu : UIImageView!
}

extension SideMenuVC :  RESideMenuDelegate {
    func sideMenu(_ sideMenu: RESideMenu!, willShowMenuViewController menuViewController: UIViewController!) {
        self.viewWillAppear(true)
    }
}

// MARK: STPPaymentOptionsViewControllerDelegate
extension SideMenuVC : STPPaymentOptionsViewControllerDelegate {
    func paymentOptionsViewControllerDidCancel(_ paymentOptionsViewController: STPPaymentOptionsViewController) {
        dismiss(animated: true, completion: nil)
    }

    func paymentOptionsViewControllerDidFinish(_ paymentOptionsViewController: STPPaymentOptionsViewController) {
        dismiss(animated: true, completion: nil)
    }

    func paymentOptionsViewController(_ paymentOptionsViewController: STPPaymentOptionsViewController, didFailToLoadWithError error: Error) {
        dismiss(animated: true, completion: nil)
    }
}
