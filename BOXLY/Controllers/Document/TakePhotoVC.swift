//
//  TakePhotoVC.swift
//  BOXLY
//
//  Created by Dhaval on 23/01/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit
import Photos
import AVFoundation

class TakePhotoVC: UIViewController {
    @IBOutlet var previewView: UIView!
    let session = AVCaptureSession()
    let photoOutput = AVCapturePhotoOutput()
    let sessionQueue = DispatchQueue(label: "session queue",
                                     attributes: [],
                                     target: nil)
    
    var previewLayer : AVCaptureVideoPreviewLayer!
    var videoDeviceInput: AVCaptureDeviceInput!
    var setupResult: SessionSetupResult = .success
    
    enum SessionSetupResult {
        case success
        case notAuthorized
        case configurationFailed
    }
    
    let cameraController = CameraController()

    override func viewDidLoad() {
        super.viewDidLoad()
        configureCameraController()
    }

    func configureCameraController() {
        cameraController.currentCameraPosition = .front
        cameraController.prepare {(error) in
            if let error = error {
                print(error)
            }

            try? self.cameraController.displayPreview(on: self.previewView)
        }
    }
    @IBAction func btnBackClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
