//
//  DeliveryWelcomeVC.swift
//  BOXLY
//
//  Created by iMac on 22/01/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class DeliveryWelcomeVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var lblWelcome: UILabel!
    var isDriver: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        if isDriver {
            tableView.tableFooterView = nil
            lblWelcome.text = "Welcome back"
            if (UserDefaults.standard.object(forKey: UD_USERNAME) != nil) {
                lblWelcome.text = "Welcome back, \(UserDefaults.standard.object(forKey: UD_USERNAME) as? String ?? "")"
            }
        }
    }
    
    @IBAction func btnBackClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension DeliveryWelcomeVC : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 5
        } else {
            return isDriver ? 1 : 2
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0
        }else {
            return 75
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 30, y: 0, width: tableView.bounds.width-35, height: 75))
        let label = UILabel(frame: view.frame)
        label.text = "Completed steps"
        label.font = AIRBNB_CEREAL_MEDIUM(fontSize: 19)
        view.addSubview(label)
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 89
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellUserDocNames", for: indexPath) as! CellUserDocNames
        cell.contentView.isUserInteractionEnabled = false
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                cell.viewContentView.backgroundColor = Color_Hex(hex: "F6F6F6")
            }else {
                cell.viewContentView.backgroundColor = Color_Hex(hex: "F0F0FF")
            }
            cell.btnNext.setImage(UIImage(named: "ic_right_arrow"), for: .normal)
            
            switch indexPath.row {
            case 0:
                cell.lblTitle.text = "Recommended next step"
                cell.lblSubTItle.text = "Property"
                if isDriver {
                    cell.lblSubTItle.text = "Background Check"
                }
            case 1:
                cell.lblTitle.text = "Get Started"
                cell.lblSubTItle.text = "Location"
                if isDriver {
                    cell.lblSubTItle.text = "Driver's License"
                }
            case 2:
                cell.lblTitle.text = "Get Started"
                cell.lblSubTItle.text = "Photo"
                if isDriver {
                    cell.lblSubTItle.text = "Vehical Insurance"
                }
            case 3:
                cell.lblTitle.text = "Recommended next step"
                cell.lblSubTItle.text = "Title"
                if isDriver {
                    cell.lblSubTItle.text = "Vehical Registration"
                }
            case 4:
                cell.lblTitle.text = "Recommended next step"
                cell.lblSubTItle.text = "Availablities"
                if isDriver {
                    cell.lblSubTItle.text = "Profile Photo"
                }
            default:
                break
            }
        } else {
            cell.btnNext.setImage(UIImage(named: "ic_round_checkmark"), for: .normal)
            cell.viewContentView.backgroundColor = Color_Hex(hex: "F6F6F6")
            cell.viewContentView.alpha = 0.4
            if isDriver {
                cell.lblTitle.text = "Recommended next step"
                cell.lblSubTItle.text = "Background Check"
            } else {
                switch indexPath.row {
                case 0:
                    cell.lblTitle.text = "Recommended next step"
                    cell.lblSubTItle.text = "Pricing"
                default:
                    cell.lblTitle.text = "Recommended next step"
                    cell.lblSubTItle.text = "Booking settings"
                }
            }
            
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0{
            if isDriver {
                if indexPath.row == 0 {
                    let vc = loadVC(strStoryboardId: SB_DOCUMENT, strVCId: "TakePhotoVC")
                    vc.modalPresentationStyle = .fullScreen
                    self.present(vc, animated: true, completion: nil)
                } else if indexPath.row == 1 {
                    let vc = loadVC(strStoryboardId: SB_DOCUMENT, strVCId: "DrivingLicenseVC")
                    vc.modalPresentationStyle = .fullScreen
                    self.present(vc, animated: true, completion: nil)
                }
            } else {
                 if indexPath.row == 0 {
                    let vc = loadVC(strStoryboardId: SB_Hosting, strVCId: "TellUsSpacesVC")
                    self.navigationController?.pushViewController(vc, animated: true)
                 } else if indexPath.row == 1 {
                    let vc = loadVC(strStoryboardId: SB_Hosting, strVCId: "TakeLocationVC") as! TakeLocationVC
                    vc.isForHosting = true
                    self.navigationController?.pushViewController(vc, animated: true)
                } else if indexPath.row == 2 {
                    let vc = loadVC(strStoryboardId: SB_Hosting, strVCId: "AddPhotosVC")
                    self.navigationController?.pushViewController(vc, animated: true)
                } else if indexPath.row == 3 {
                    let vc = loadVC(strStoryboardId: SB_Hosting, strVCId: "TitleQuestionsVC") as! TitleQuestionsVC
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
    }
}

class CellUserDocNames : UITableViewCell {
    @IBOutlet var viewContentView: UIView!
    @IBOutlet var btnNext: UIButton!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblSubTItle: UILabel!
}
