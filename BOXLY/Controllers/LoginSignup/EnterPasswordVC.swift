//
//  EnterPasswordVC.swift
//  BOXLY
//
//  Created by iMac on 03/01/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class EnterPasswordVC: UIViewController {

    @IBOutlet var txtPassword: UITextField!
    @IBOutlet var bottomView: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        txtPassword.becomeFirstResponder()
        bottomView.constant = APP_DELEGATE.KeyBoardHeight+50
//        UIView.animate(withDuration: 0.3) {
//            self.view.layoutIfNeeded()
//        }
    }

    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func btnHideShowPwd(_ sender: UIButton) {
        txtPassword.isSecureTextEntry = sender.isSelected
        sender.isSelected = !sender.isSelected
    }

    @IBAction func btnNext(_ sender: Any) {
        let vc = loadVC(strStoryboardId: SB_LOGIN, strVCId: "EnterEmailVC")
        self.navigationController?.pushViewController(vc, animated: false)
    }
}
