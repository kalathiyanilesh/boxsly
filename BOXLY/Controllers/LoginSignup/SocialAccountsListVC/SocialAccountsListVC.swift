//
//  SocialAccountsListVC.swift
//  BOXLY
//
//  Created by Dhaval on 17/01/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit
protocol SocialAccountListDelegate{
    func didSelectAccount()
}
class SocialAccountsListVC: UIViewController {
    @IBOutlet weak var tblAccountList: UITableView!

    var socialAccountListDelegate : SocialAccountListDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        tblAccountList.delegate = self
        tblAccountList.dataSource = self
    }
}

extension SocialAccountsListVC : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SocialAccountListCell", for: indexPath) as! SocialAccountListCell
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        UIView.animate(withDuration: 0.3) {
            self.view.alpha = 0
        }
        self.dismiss(animated: false, completion: nil)
        self.socialAccountListDelegate?.didSelectAccount()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
}

class SocialAccountListCell : UITableViewCell {
    
}
