//
//  EnterCodeVC.swift
//  BOXLY
//
//  Created by iMac on 03/01/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit
import SwiftMessages
protocol MyTextFieldDelegate {
    func textFieldDidDelete()
}

class MyTextField: UITextField {
    var myDelegate: MyTextFieldDelegate?
    
    override func deleteBackward() {
        super.deleteBackward()
        myDelegate?.textFieldDidDelete()
    }
}

class EnterCodeVC: UIViewController {
    
    @IBOutlet var lblSentMobileNo: UILabel!
    @IBOutlet var first: MyTextField!
    @IBOutlet var second: MyTextField!
    @IBOutlet var third: MyTextField!
    @IBOutlet var fourth: MyTextField!
    @IBOutlet var fifth: MyTextField!
    @IBOutlet var six: MyTextField!
    @IBOutlet var bottomView: NSLayoutConstraint!
    
    var currentTextFiled:MyTextField! = nil
    var sPhoneNumber = ""
    var isSavedNo: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        KeyboardCenter()
        setUpTextField()
        if sPhoneNumber.count > 0 {
            lblSentMobileNo.text = "Sent to \(sPhoneNumber)"
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        first.becomeFirstResponder()
        bottomView.constant = APP_DELEGATE.KeyBoardHeight
        
        var config = SwiftMessages.defaultConfig
        config.presentationContext = .window(windowLevel: UIWindow.Level.statusBar)
        config.dimMode = .none
        config.interactiveHide = true
        
        let view: MessageView
        view = MessageView.viewFromNib(layout: .cardView)
        view.configureContent(title: "Code sent", body: "", iconImage: nil, iconText: nil, buttonImage: nil, buttonTitle: "", buttonTapHandler: { _ in SwiftMessages.hide() })
        view.configureTheme(backgroundColor: #colorLiteral(red: 0.3122644424, green: 0.3996008039, blue: 0.4403602481, alpha: 1), foregroundColor: UIColor.clear, iconImage: nil, iconText: nil)
        view.titleLabel?.textColor = .white
        view.backgroundHeight = 55
    
        SwiftMessages.show(config: config, view: view)
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func btnNext(_ sender: Any) {
        /*
        let vc = loadVC(strStoryboardId: SB_LOGIN, strVCId: "EnterNameVC")
        self.navigationController?.pushViewController(vc, animated: false)*/
        
        if isSavedNo {
            if (UserDefaults.standard.object(forKey: UD_SAVEDUSER) != nil) {
                APP_DELEGATE.arrSavedUsers = (UserDefaults.standard.object(forKey: UD_SAVEDUSER) as! NSArray).mutableCopy() as! NSMutableArray
                print(APP_DELEGATE.arrSavedUsers)
            }
            
            if (UserDefaults.standard.object(forKey: UD_USEREMAIL) != nil) {
                let dictData: NSDictionary = ["email":UserDefaults.standard.object(forKey: UD_USEREMAIL) as! String, "phonenumber":sPhoneNumber]
                APP_DELEGATE.arrSavedUsers.add(dictData)
            } else {
                let dictData: NSDictionary = ["email":"", "phonenumber":sPhoneNumber]
                APP_DELEGATE.arrSavedUsers.add(dictData)
            }
            UserDefaults.standard.set(APP_DELEGATE.arrSavedUsers, forKey: UD_SAVEDUSER)
        }
        
        UserDefaults.standard.set(true, forKey: IS_LOGGED_IN)
        UserDefaults.standard.synchronize()
        APP_DELEGATE.setupInitials()
    }
    
    @IBAction func btnResendCode(_ sender: Any) {
    }
    
}

//MARK:- MyTextFieldDelegate Delegate
extension EnterCodeVC: UITextFieldDelegate, MyTextFieldDelegate {
    func setUpTextField() {
        
        first.myDelegate = self
        second.myDelegate = self
        third.myDelegate = self
        fourth.myDelegate = self
        fifth.myDelegate = self
        six.myDelegate = self
        
        first.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        second.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        third.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        fourth.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        fifth.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        six.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        
        first.addTarget(self, action: #selector(myTargetFunction(textField:)), for: .touchDown)
        second.addTarget(self, action: #selector(myTargetFunction(textField:)), for: .touchDown)
        third.addTarget(self, action: #selector(myTargetFunction(textField:)), for: .touchDown)
        fourth.addTarget(self, action: #selector(myTargetFunction(textField:)), for: .touchDown)
        fifth.addTarget(self, action: #selector(myTargetFunction(textField:)), for: .touchDown)
        six.addTarget(self, action: #selector(myTargetFunction(textField:)), for: .touchDown)
        
        currentTextFiled = first
    }
     
    func textFieldDidDelete() {
        var isGoDown: Bool = false
        if(currentTextFiled == first)
        {
            if(first.text!.count > 0)
            {
                first.text = ""
                currentTextFiled = first
            }
            else
            {
                self.view.endEditing(true)
                isGoDown = true
            }
        }
        else if(currentTextFiled == second)
        {
            if(second.text!.count > 0)
            {
                second.text = ""
            }
            else
            {
                first.text = ""
                first.becomeFirstResponder()
            }
        }
        else if(currentTextFiled == third)
        {
            if(third.text!.count > 0)
            {
                third.text = ""
            }
            else
            {
                currentTextFiled = second
                second.text = ""
                second.becomeFirstResponder()
            }
        }
        else if(currentTextFiled == fourth)
        {
            if(fourth.text!.count > 0)
            {
                fourth.text = ""
            }
            else
            {
                currentTextFiled = third
                third.text = ""
                third.becomeFirstResponder()
            }
        }
        else if(currentTextFiled == fifth)
        {
            if(fifth.text!.count > 0)
            {
                fifth.text = ""
            }
            else
            {
                currentTextFiled = fourth
                fourth.text = ""
                fourth.becomeFirstResponder()
            }
        }
        else if(currentTextFiled == six)
        {
            if(six.text!.count > 0)
            {
                six.text = ""
            }
            else
            {
                currentTextFiled = fifth
                fifth.text = ""
                fifth.becomeFirstResponder()
            }
        }
        if isGoDown {
            GoDownView()
        } else {
            GoUpView()
        }
    }
    
    
    @objc func myTargetFunction(textField: UITextField) {
        if(textField == first)
        {
            currentTextFiled = first
            first.text = ""
        }
        else if(textField == second)
        {
            currentTextFiled = second
            second.text = ""
        }
        else if(textField == third)
        {
            currentTextFiled = third
            third.text = ""
        }
        else if(textField == fourth)
        {
            currentTextFiled = fourth
            fourth.text = ""
        }
        else if(textField == fifth)
        {
            currentTextFiled = fifth
            fifth.text = ""
        }
        else if(textField == six)
        {
            currentTextFiled = six
            six.text = ""
        }
        GoUpView()
    }
    
    @objc func textFieldDidChange(textField: UITextField) {
        
        GoUpView()
        
        let text = textField.text
        if text?.count == 1 {
            switch textField{
            case first:
                currentTextFiled = second
                second.text = ""
                second.becomeFirstResponder()
            case second:
                currentTextFiled = third
                third.text = ""
                third.becomeFirstResponder()
            case third:
                currentTextFiled = fourth
                fourth.text = ""
                fourth.becomeFirstResponder()
            case fourth:
                currentTextFiled = fifth
                fifth.text = ""
                fifth.becomeFirstResponder()
            case fifth:
                currentTextFiled = six
                six.text = ""
                six.becomeFirstResponder()
            case six:
                self.view.endEditing(true)
                GoDownView()
            default:
                break
            }
        }
    }
    
    
    //MARK:- TextField Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        currentTextFiled = textField as? MyTextField
        textField.text = ""
        GoUpView()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
        GoDownView()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textFieldDidEndEditing(textField)
        return true
    }
    
    func GoUpView() {
        bottomView.constant = APP_DELEGATE.KeyBoardHeight
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    func GoDownView() {
        bottomView.constant = 50
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
}

//MARK:- KEYBOARD METHOD
extension EnterCodeVC {
    func KeyboardCenter() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let keyboardHeight = keyboardSize.height
            if APP_DELEGATE.KeyBoardHeight == 0 {
                APP_DELEGATE.KeyBoardHeight = keyboardHeight
                bottomView.constant = APP_DELEGATE.KeyBoardHeight
                UIView.animate(withDuration: 0.3) {
                    self.view.layoutIfNeeded()
                }
            }
        }
    }
}
