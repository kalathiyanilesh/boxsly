//
//  WelcomeVC.swift
//  BOXLY
//
//  Created by iMac on 03/01/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class WelcomeVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnSignUp(_ sender: Any) {
        let vc = loadVC(strStoryboardId: SB_LOGIN, strVCId: "MobileNoVC")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnLogin(_ sender: Any) {
        UserDefaults.standard.set(true, forKey: IS_LOGGED_IN)
        APP_DELEGATE.setupInitials()
    }
}
