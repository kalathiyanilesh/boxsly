//
//  NumberListVC.swift
//  BOXLY
//
//  Created by Dhaval on 17/01/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

protocol NumberListDelegate {
    func didSelectNumber(_ sNumber: String)
}

class NumberListVC: UIViewController {
    
    @IBOutlet weak var tblNumbers: UITableView!
    var numberListDelegate : NumberListDelegate?
    
    var arrNumbers = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblNumbers.delegate = self
        tblNumbers.dataSource = self
        let array = APP_DELEGATE.arrSavedUsers.value(forKeyPath: "phonenumber") as! NSArray
        arrNumbers = (array.value(forKeyPath: "@distinctUnionOfObjects.self") as! NSArray).mutableCopy() as! NSMutableArray
    }
    
    @IBAction func btnNoneOfAbove(_ sender: Any) {
        UIView.animate(withDuration: 0.3) {
            self.view.alpha = 0
        }
        self.dismiss(animated: false, completion: nil)
    }
}

extension NumberListVC : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrNumbers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NumberCell", for: indexPath) as! NumberCell
        cell.lblNumber.text = arrNumbers[indexPath.row] as? String
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        UIView.animate(withDuration: 0.3) {
            self.view.alpha = 0
        }
        self.dismiss(animated: false, completion: nil)
        self.numberListDelegate?.didSelectNumber(arrNumbers[indexPath.row] as! String)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
}

class NumberCell : UITableViewCell {
    @IBOutlet var lblNumber: UILabel!
}

extension Array where Element: Equatable {
    func removingDuplicates() -> Array {
        return reduce(into: []) { result, element in
            if !result.contains(element) {
                result.append(element)
            }
        }
    }
}
