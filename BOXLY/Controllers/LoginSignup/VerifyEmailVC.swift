//
//  VerifyEmailVC.swift
//  BOXLY
//
//  Created by Dhaval on 17/01/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit
import FirebaseAuth
class VerifyEmailVC: UIViewController {
    @IBOutlet weak var lblEmail: UILabel!
    var link: String!
    var email: String!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let email = UserDefaults.standard.value(forKey: "Email") as? String {
            self.email = email
            self.lblEmail.text = email
            
        }else {
            showMessage("Email can't be empty.")
        }
        
        if let link = UserDefaults.standard.value(forKey: "Link") as? String {
          self.link = link
        }
        
        if Auth.auth().isSignIn(withEmailLink: link) {
            Auth.auth().signIn(withEmail: email, link: self.link) { (user, error) in
                if let error = error {
                    showMessage(error.localizedDescription)
                    return
                }
                UserDefaults.standard.set(true, forKey: IS_LOGGED_IN)
                APP_DELEGATE.setupInitials()
            }
        }
    }
    
    @IBAction func btnBackClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
