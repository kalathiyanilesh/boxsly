//
//  EnterNameVC.swift
//  BOXLY
//
//  Created by iMac on 03/01/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class EnterNameVC: UIViewController {

    @IBOutlet var txtFirstName: UITextField!
    @IBOutlet var txtLastName: UITextField!
    @IBOutlet var bottomView: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        KeyboardCenter()
    }

    override func viewWillAppear(_ animated: Bool) {
        txtFirstName.becomeFirstResponder()
        bottomView.constant = APP_DELEGATE.KeyBoardHeight
//        UIView.animate(withDuration: 0.3) {
//            self.view.layoutIfNeeded()
//        }
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func btnNext(_ sender: Any) {
        let vc = loadVC(strStoryboardId: SB_LOGIN, strVCId: "EnterEmailVC")
        self.navigationController?.pushWithFadeTo(vc)
    }
}

//MARK:- KEYBOARD METHOD
extension EnterNameVC {
    func KeyboardCenter() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let keyboardHeight = keyboardSize.height
            if APP_DELEGATE.KeyBoardHeight == 0 {
                APP_DELEGATE.KeyBoardHeight = keyboardHeight
                bottomView.constant = APP_DELEGATE.KeyBoardHeight
                UIView.animate(withDuration: 0.3) {
                    self.view.layoutIfNeeded()
                }
            }
        }
    }
}
