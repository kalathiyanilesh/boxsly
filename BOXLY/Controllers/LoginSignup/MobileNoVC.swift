//
//  MobileNoVC.swift
//  BOXLY
//
//  Created by iMac on 03/01/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit
import Presentr
import PhoneNumberKit

class MobileNoVC: UIViewController {

    @IBOutlet var txtPhoneNumber: PhoneNumberTextField!
    @IBOutlet var bottomView: NSLayoutConstraint!
    @IBOutlet var btnCountryFlag: UIButton!
    @IBOutlet var lblCountryCode: UILabel!
    
    var countryPickerController : CountryCodePickerViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        KeyboardCenter()
        let phoneNumberKit = PhoneNumberKit()
        countryPickerController = CountryCodePickerViewController(phoneNumberKit: phoneNumberKit)
        updateFlag(currentRegion: txtPhoneNumber.currentRegion)
        lblCountryCode.text = "+" + getCountryPhonceCode(country: txtPhoneNumber.currentRegion)
    }

    override func viewDidAppear(_ animated: Bool) {
        let presenter: Presentr = {
            let width = ModalSize.custom(size: 310)
            let height = ModalSize.custom(size: 308)
            let customType = PresentationType.custom(width: width, height: height, center: .center)
            
            let customPresenter = Presentr(presentationType: customType)
            customPresenter.transitionType = nil
            customPresenter.dismissTransitionType = nil
            customPresenter.dismissAnimated = true
            customPresenter.cornerRadius = 5
            customPresenter.roundCorners = true
            return customPresenter
        }()

        if (UserDefaults.standard.object(forKey: UD_SAVEDUSER) != nil) {
            let vc = loadVC(strStoryboardId: SB_LOGIN, strVCId: "NumberListVC") as! NumberListVC
            vc.numberListDelegate = self
            customPresentViewController(presenter, viewController: vc, animated: true, completion: nil)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //txtNumber.becomeFirstResponder()
        bottomView.constant = APP_DELEGATE.KeyBoardHeight
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func btnNext(_ sender: Any) {
        let isValidNumber = txtPhoneNumber.isValidNumber
        if isValidNumber {
            let vc = loadVC(strStoryboardId: SB_LOGIN, strVCId: "EnterCodeVC") as! EnterCodeVC
            vc.sPhoneNumber = "\(lblCountryCode.text!) \(txtPhoneNumber.text!)"
            self.navigationController?.pushViewController(vc, animated: false)
        } else {
            showMessage("Enter valid phone number")
        }
    }
    
    @IBAction func btnChangeNo(_ sender: Any) {
        
    }
    
    @IBAction func btnCountryFlag(_ sender: Any) {
        self.view.endEditing(true)
        countryPickerController?.delegate = self
        let nav = UINavigationController(rootViewController: countryPickerController!)
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            topController.present(nav, animated: true, completion: nil)
        }
    }
}

//MARK: Country Picker
extension MobileNoVC : CountryCodePickerDelegate {
    func countryCodePickerViewControllerDidPickCountry(_ country: CountryCodePickerViewController.Country) {
        txtPhoneNumber.text = /*isEditing ? "+" + country.prefix : */""
        countryPickerController?.navigationController?.dismiss(animated: true, completion: nil)
        lblCountryCode.text = country.prefix
        btnCountryFlag.setTitle(country.flag, for: .normal)
        btnCountryFlag.titleLabel?.font = UIFont.systemFont(ofSize: 30)
    }
    
    func updateFlag(currentRegion : String){
        let flagBase = UnicodeScalar("🇦").value - UnicodeScalar("A").value
        
        let flag = currentRegion
            .uppercased()
            .unicodeScalars
            .compactMap { UnicodeScalar(flagBase + $0.value)?.description }
            .joined()
         btnCountryFlag.setTitle(flag, for: .normal)
         btnCountryFlag.titleLabel?.font = UIFont.systemFont(ofSize: 30)
    }
}

//MARK:- KEYBOARD METHOD
extension MobileNoVC {
    func KeyboardCenter() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let keyboardHeight = keyboardSize.height
            if APP_DELEGATE.KeyBoardHeight == 0 {
                APP_DELEGATE.KeyBoardHeight = keyboardHeight
                bottomView.constant = APP_DELEGATE.KeyBoardHeight
                UIView.animate(withDuration: 0.3) {
                    self.view.layoutIfNeeded()
                }
            }
        }
    }
}

extension MobileNoVC : NumberListDelegate {
    func didSelectNumber(_ sNumber: String) {
        let vc = loadVC(strStoryboardId: SB_LOGIN, strVCId: "EnterCodeVC") as! EnterCodeVC
        vc.sPhoneNumber = sNumber
        self.navigationController?.pushViewController(vc, animated: false)
    }
}
