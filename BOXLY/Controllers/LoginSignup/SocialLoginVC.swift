//
//  SocialLoginVC.swift
//  BOXLY
//
//  Created by Dhaval on 13/01/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit
import Presentr
import GoogleSignIn
import FBSDKLoginKit
import LoginWithAmazon
import AuthenticationServices
import PhoneNumberKit
import FirebaseAuth

class SocialLoginVC: UIViewController, UITextFieldDelegate {

    @IBOutlet var txtPhoneNumber: PhoneNumberTextField!
    @IBOutlet var btnCountryCode: UIButton!
    @IBOutlet var btnCountryFlag: UIButton!
    
    var countryPickerController : CountryCodePickerViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtPhoneNumber.delegate = self
        txtPhoneNumber.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        let phoneNumberKit = PhoneNumberKit()
        countryPickerController = CountryCodePickerViewController(phoneNumberKit: phoneNumberKit)
        updateFlag(currentRegion: txtPhoneNumber.currentRegion)
        btnCountryCode.setTitle("+" + getCountryPhonceCode(country: txtPhoneNumber.currentRegion), for: .normal)
    }
    
    @IBAction func btnCountryFlag(_ sender: Any) {
        self.view.endEditing(true)
        countryPickerController?.delegate = self
        let nav = UINavigationController(rootViewController: countryPickerController!)
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            topController.present(nav, animated: true, completion: nil)
        }
    }

    @objc func textFieldDidChange(_ textFeild: UITextField) {
        guard txtPhoneNumber.text != nil else { return }
        let isValidNumber = txtPhoneNumber.isValidNumber
        if isValidNumber {
            let vc = loadVC(strStoryboardId: SB_LOGIN, strVCId: "EnterNameVC")
            self.navigationController?.pushWithFadeTo(vc)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        UserDefaults.standard.removeObject(forKey: UD_USERNAME)
        UserDefaults.standard.removeObject(forKey: UD_USERIMAGE)
    }
    
    @IBAction func btnPhoneLoginClicked(_ sender: Any) {
        let vc = loadVC(strStoryboardId: SB_LOGIN, strVCId: "MobileNoVC")
        self.navigationController?.pushWithFadeTo(vc)
    }
    
    @IBAction func btnSocialLoginClicked(_ sender: UIButton) {
        ShowLoadingAnimation(sender)
        runAfterTime(time: 1) {
            if sender.tag == 1 {
                self.AmazonLogin()
            } else if sender.tag == 2 {
                self.GoogleLogin()
            } else if sender.tag == 3 {
                self.AppleLogin()
            }
        }
        
        
        /*
        let presenter: Presentr = {
              
            let width = ModalSize.custom(size: Float(view.bounds.width - 44))
            let height = ModalSize.custom(size: 530)
            let customType = PresentationType.custom(width: width, height: height, center: .center)

            let customPresenter = Presentr(presentationType: customType)
              customPresenter.transitionType = nil
              customPresenter.dismissTransitionType = nil
              customPresenter.dismissAnimated = true
              
            customPresenter.cornerRadius = 5
              customPresenter.roundCorners = true
              return customPresenter
          }()

        
        let vc = loadVC(strStoryboardId: SB_LOGIN, strVCId: "SocialAccountsListVC") as! SocialAccountsListVC
        vc.socialAccountListDelegate = self
        customPresentViewController(presenter, viewController: vc, animated: true, completion: nil)*/
    }
    
    @IBAction func btnSkipClicked(_ sender: Any) {
        if (UserDefaults.standard.object(forKey: "isSkip") == nil) {
            let vc = loadVC(strStoryboardId: SB_ONBOARDING, strVCId: "OnboardingVC")
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            UserDefaults.standard.set(true, forKey: IS_LOGGED_IN)
            APP_DELEGATE.setupInitials()
        }
    }
    
    func AfterSocialLogin() {
        /*
        let vc = loadVC(strStoryboardId: SB_LOGIN, strVCId: "EnterNameVC")
        self.navigationController?.pushWithFadeTo(vc)*/
        
        /*
        if let email = UserDefaults.standard.object(forKey: UD_USEREMAIL) as? String {
            
            Auth.auth().fetchProviders(forEmail: email, completion: {
                (providers, error) in

                if let error = error {
                    showMessage(error.localizedDescription)
                } else if let providers = providers {
                    if providers.count > 0 {
                        Auth.auth().signIn(withEmail: email, password: "password") { authResult, error in
                            if error != nil {
                                showMessage(error?.localizedDescription ?? "")
                                return
                            }
                            UserDefaults.standard.set(true, forKey: IS_LOGGED_IN)
                            APP_DELEGATE.setupInitials()
                        }
                    }
                } else {
                    Auth.auth().createUser(withEmail: email, password: "password") { authResult, error in
                        guard let user = authResult?.user, error == nil else {
                            showMessage(error?.localizedDescription ?? "")
                            return
                        }
                        UserDefaults.standard.set(true, forKey: IS_LOGGED_IN)
                        APP_DELEGATE.setupInitials()
                    }
                }
            })
        }*/
        
        if (UserDefaults.standard.object(forKey: UD_SAVEDUSER) != nil) {
            APP_DELEGATE.arrSavedUsers = (UserDefaults.standard.object(forKey: UD_SAVEDUSER) as! NSArray).mutableCopy() as! NSMutableArray
            if let email = UserDefaults.standard.object(forKey: UD_USEREMAIL) as? String {
                let pred : NSPredicate = NSPredicate(format: "email = %@", email)
                let arrTemp =  APP_DELEGATE.arrSavedUsers.filtered(using: pred) as NSArray
                if arrTemp.count > 0 {
                    let dictData: NSDictionary = arrTemp[0] as! NSDictionary
                    let sPhoneNumber = dictData.object(forKey: "phonenumber") as! String
                    let vc = loadVC(strStoryboardId: SB_LOGIN, strVCId: "EnterCodeVC") as! EnterCodeVC
                    vc.sPhoneNumber = sPhoneNumber
                    vc.isSavedNo = false
                    self.navigationController?.pushViewController(vc, animated: false)
                } else {
                    let vc = loadVC(strStoryboardId: SB_LOGIN, strVCId: "MobileNoVC")
                    self.navigationController?.pushWithFadeTo(vc)
                }
            } else {
                let vc = loadVC(strStoryboardId: SB_LOGIN, strVCId: "MobileNoVC")
                self.navigationController?.pushWithFadeTo(vc)
            }
        } else {
            let vc = loadVC(strStoryboardId: SB_LOGIN, strVCId: "MobileNoVC")
            self.navigationController?.pushWithFadeTo(vc)
        }
        
//        UserDefaults.standard.set(true, forKey: IS_LOGGED_IN)
//        APP_DELEGATE.setupInitials()
    }
}


//MARK: Country Picker
extension SocialLoginVC : CountryCodePickerDelegate {
    func countryCodePickerViewControllerDidPickCountry(_ country: CountryCodePickerViewController.Country) {
        txtPhoneNumber.text = /*isEditing ? "+" + country.prefix : */""
        countryPickerController?.navigationController?.dismiss(animated: true, completion: nil)
        btnCountryCode.setTitle(country.prefix, for: .normal)
        btnCountryFlag.setTitle(country.flag, for: .normal)
        btnCountryFlag.titleLabel?.font = UIFont.systemFont(ofSize: 30)
    }
    
    func updateFlag(currentRegion : String){
        let flagBase = UnicodeScalar("🇦").value - UnicodeScalar("A").value
        
        let flag = currentRegion
            .uppercased()
            .unicodeScalars
            .compactMap { UnicodeScalar(flagBase + $0.value)?.description }
            .joined()
         btnCountryFlag.setTitle(flag, for: .normal)
         btnCountryFlag.titleLabel?.font = UIFont.systemFont(ofSize: 30)
    }
}

//MARK:- FACEBOOK LOGIN
extension SocialLoginVC {
    func FacebookLogin() {
        if(AccessToken.current == nil){
            let fbLoginManager : LoginManager = LoginManager()
            fbLoginManager.logIn(permissions: ["email"], from: self) { (result, error) -> Void in
                if (error == nil){
                    let fbloginresult : LoginManagerLoginResult = result!
                    if (result?.isCancelled)!{ return }
                    if error != nil {
                        print(error?.localizedDescription ?? "")
                        return
                    }
                    if(fbloginresult.grantedPermissions.contains("email"))
                    {
                        self.getFBUserData()
                    }
                }
            }
        }else{
            self.getFBUserData()
        }
    }
    
    func getFBUserData(){
        showLoaderHUD(strMessage: "")
        GraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
            if (error == nil){
                print(result ?? "Not Found")
                let dictResult = result as! NSDictionary
                print(dictResult )
                
                let strProfilePic = String(format:"https://graph.facebook.com/%@/picture?type=large",dictResult.object(forKey: "id") as? String ?? "")
                
                let strFName = dictResult.object(forKey: "first_name") as? String ?? ""
                UserDefaults.standard.set(strFName, forKey: UD_USERNAME)
                UserDefaults.standard.set(strProfilePic, forKey: UD_USERIMAGE)
                if let strEmail = dictResult.object(forKey: "email") as? String {
                    UserDefaults.standard.set(strEmail, forKey: UD_USEREMAIL)
                }
                UserDefaults.standard.synchronize()
                
                
                /*
                let strEmail = dictResult.object(forKey: "email") as? String ?? ""
                let strName = dictResult.object(forKey: "name") as? String ?? ""
                let strFName = dictResult.object(forKey: "first_name") as? String ?? ""
                let strLName = dictResult.object(forKey: "last_name") as? String ?? ""
                let strEmail = dictResult.object(forKey: "id") as? String ?? ""
                let strProfilePic = String(format:"https://graph.facebook.com/%@/picture?type=large",dictResult.object(forKey: "id") as? String ?? "")*/
                
                hideLoaderHUD()
                
                runAfterTime(time: 0.5) {
                    self.AfterSocialLogin()
                }
                
            } else {
                hideLoaderHUD()
            }
        })
    }
}

//MARK:- GOOGLE LOGIN
extension SocialLoginVC: GIDSignInDelegate {
    func GoogleLogin() {
        self.view.endEditing(true)
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance().signIn()
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            print("\(error.localizedDescription)")
            hideLoaderHUD()
        } else {
            var strProfilePic = ""
            if user.profile.hasImage {
                strProfilePic = "\(user.profile.imageURL(withDimension: 200)!)"
            }
            print(strProfilePic)
            print(user.profile.givenName ?? "")
            print(user.profile.familyName ?? "")
            print(user.profile.email ?? "")
            print(user.userID ?? "")
            UserDefaults.standard.set(user.profile.givenName ?? "", forKey: UD_USERNAME)
            UserDefaults.standard.set(strProfilePic, forKey: UD_USERIMAGE)
            
            if let email = user.profile.email {
                UserDefaults.standard.set(email, forKey: UD_USEREMAIL)
            }
            
            UserDefaults.standard.synchronize()
            hideLoaderHUD()
            runAfterTime(time: 0.5) {
                self.AfterSocialLogin()
            }
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        print("Error:",error.localizedDescription)
        hideLoaderHUD()
    }
}

extension SocialLoginVC {
    func getAccessToken(delegate: AIAuthenticationDelegate) {
        AIMobileLib.getAccessToken(forScopes: ["profile"], withOverrideParams: nil, delegate: delegate)
    }
    
    func getUserProfile(delegate: AIAuthenticationDelegate) {
        AIMobileLib.getProfile(delegate)
    }
}

//MARK:- AI AUTHENTICATE DELEGATE
extension SocialLoginVC: AIAuthenticationDelegate {
    
    func AmazonLogin() {
        AIMobileLib.authorizeUser(forScopes: ["profile", "postal_code"], delegate: self)
    }
    
    func requestDidSucceed(_ apiResult: APIResult!) {
        print(apiResult)
        
        switch (apiResult.api) {
            
        case API.authorizeUser:
            AIMobileLib.getAccessToken(forScopes: ["profile"], withOverrideParams: nil, delegate: self)
            
        case API.getAccessToken:
            guard let LWAtoken = apiResult.result as? String else { return }
            print("LWA Access Token: \(LWAtoken)")
            
            // Get the user profile from LWA (OPTIONAL)
            AIMobileLib.getProfile(self)
            
        case API.getProfile:
            print("LWA User Profile: \(String(describing: apiResult.result))")
            
            if let dictResult = apiResult.result as? NSDictionary {
                let strName = dictResult.object(forKey: "name") as! String
                UserDefaults.standard.set(strName, forKey: UD_USERNAME)
                if let email = dictResult.object(forKey: "email") as? String {
                    UserDefaults.standard.set(email, forKey: UD_USEREMAIL)
                }
                UserDefaults.standard.synchronize()
            }
            runAfterTime(time: 0.5) {
                self.AfterSocialLogin()
            }
                /*
            {
                email = "kalathiyanilesh1@gmail.com";
                name = "Nilesh Kalathiya";
                "user_id" = "amzn1.account.AGP4C52FLZS5NKN7A3OXHMQIQXDQ";
            }*/
            
        case API.clearAuthorizationState:
            print("user logged out from LWA")
            
            // Sign out from AWSMobileClient
            //AWSMobileClient.sharedInstance().signOut()
            
        default:
            print("unsupported")
        }
    }
    
    func requestDidFail(_ errorResponse: APIError!) {
        print("Error: \(errorResponse.error.message ?? "nil")")
    }
}

//MARK: LOGIN WITH APPLE ACCOUNT
extension SocialLoginVC: ASAuthorizationControllerDelegate {
    func AppleLogin() {
        if #available(iOS 13.0, *) {
            let appleIDProvider = ASAuthorizationAppleIDProvider()
            let request = appleIDProvider.createRequest()
            request.requestedScopes = [.fullName, .email]
            let authorizationController = ASAuthorizationController(authorizationRequests: [request])
            authorizationController.delegate = self
            authorizationController.performRequests()
        } else {
            // Fallback on earlier versions
        }
    }
    
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        if let appleIDCredential = authorization.credential as?  ASAuthorizationAppleIDCredential {
            let userIdentifier = appleIDCredential.user
            let fullName = appleIDCredential.fullName?.givenName
            let email = appleIDCredential.email
            print("User id is \(userIdentifier) \n Full Name is \(String(describing: fullName)) \n Email id is \(String(describing: email))")
            if fullName != nil {
                UserDefaults.standard.set("\(fullName!)", forKey: UD_USERNAME)
                UserDefaults.standard.synchronize()
            }
            if email != nil {
                UserDefaults.standard.set(email, forKey: UD_USEREMAIL)
                UserDefaults.standard.synchronize()
            }
            runAfterTime(time: 0.5) {
                self.AfterSocialLogin()
            }
        }
    }
}

//MARK:- SOCIAL ACCOUNT LIST DELEGATE
extension SocialLoginVC : SocialAccountListDelegate {
    func didSelectAccount() {
        let vc = loadVC(strStoryboardId: SB_LOGIN, strVCId: "EnterNameVC")
        self.navigationController?.pushWithFadeTo(vc)
    }
}
