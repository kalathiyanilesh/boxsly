//
//  EnterEmailVC.swift
//  BOXLY
//
//  Created by iMac on 03/01/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit
import FirebaseAuth
class EnterEmailVC: UIViewController {

    @IBOutlet var txtEmail: UITextField!
    @IBOutlet var bottomView: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        txtEmail.becomeFirstResponder()
        bottomView.constant = APP_DELEGATE.KeyBoardHeight
//        UIView.animate(withDuration: 0.3) {
//            self.view.layoutIfNeeded()
//        }
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func btnNext(_ sender: Any) {
        
        if !validateEmailAddress(txtEmail, withMessage: "Please Enter Valid Email") {
            return
        }
        
        let actionCodeSettings = ActionCodeSettings()
        actionCodeSettings.url = URL(string: "https://boxly.page.link/")
        // The sign-in operation has to always be completed in the app.
        actionCodeSettings.handleCodeInApp = true
        actionCodeSettings.setIOSBundleID(Bundle.main.bundleIdentifier!)
        actionCodeSettings.setAndroidPackageName("com.example.android",
                                                 installIfNotAvailable: false, minimumVersion: "12")

        Auth.auth().sendSignInLink(toEmail:txtEmail.text!,
                                   actionCodeSettings: actionCodeSettings) { error in
          // ...
            if let error = error {
              showMessage(error.localizedDescription)
              return
            }
            showMessage("Email verification link has been sent to your email.")
            UserDefaults.standard.set(self.txtEmail.text!, forKey: "Email")
                                    
//            let vc = loadVC(strStoryboardId: SB_LOGIN, strVCId: "VerifyEmailVC")
//            self.navigationController?.pushWithFadeTo(vc)
        }

    }
}
