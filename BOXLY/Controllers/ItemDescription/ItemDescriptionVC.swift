//
//  ItemDescriptionVC.swift
//  BOXLY
//
//  Created by Dhaval on 10/01/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit
class ItemDescriptionCell : UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    
}
class ItemDescriptionVC: UIViewController {
    @IBOutlet weak var topImage: UIImageView!
    
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var btnCheckAvailability: UIButton!
    @IBOutlet weak var tblDescription: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tblDescription.delegate = self
        tblDescription.dataSource = self
        btnCheckAvailability.layer.borderColor = UIColor.white.cgColor
        topImage.addBlackGradientLayerInBackground(frame: viewHeader.frame, colors: [UIColor.clear, UIColor.black])
    }
}

extension ItemDescriptionVC : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ItemDescriptionCell", for: indexPath)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension UIView{
   // For insert layer in Foreground
    func addBlackGradientLayerInForeground(frame: CGRect, colors:[UIColor]){
        let gradient = CAGradientLayer()
        gradient.frame = frame
        gradient.colors = colors.map{$0.cgColor}
        self.layer.addSublayer(gradient)
    }
   // For insert layer in background
   func addBlackGradientLayerInBackground(frame: CGRect, colors:[UIColor]){
        let gradient = CAGradientLayer()
        gradient.frame = frame
        gradient.colors = colors.map{$0.cgColor}
        self.layer.insertSublayer(gradient, at: 0)
   }
}
