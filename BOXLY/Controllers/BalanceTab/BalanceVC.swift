//
//  BalanceVC.swift
//  BOXLY
//
//  Created by iMac on 03/02/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit
import FloatingPanel
import AVFoundation
import MTBBarcodeScanner
import Lottie

class BalanceVC: UIViewController {
    
    var fpc: FloatingPanelController!
    var scanner: MTBBarcodeScanner?
    var isTorchEnable: Bool = false
    
    @IBOutlet var viewBlack: UIView!
    @IBOutlet var viewSquare: AnimationView!
    @IBOutlet var viewScan: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        SetupFloatingView()
        scanner = MTBBarcodeScanner(previewView: viewScan)

        MTBBarcodeScanner.requestCameraPermission(success: { success in
            if success {
                do {
                    // Start scanning with the front camera
                    self.AddScannerAnimation()
                    try self.scanner?.startScanning(with: .back,
                                                    resultBlock: { codes in
                                                        self.scanner?.scanRect = self.viewSquare.frame
                                                        if let codes = codes {
                                                            for code in codes {
                                                                let stringValue = code.stringValue ?? ""
                                                                print("Found code: \(stringValue)")
                                                                showMessage(stringValue)
                                                            }
                                                        }
                    })
                } catch {
                    NSLog("Unable to start scanning")
                }
            } else {
                showMessage("This app does not have permission to access the camera")
            }
        })
        
        /*
        let animationView = AnimationView(name: "scanning")
        animationView.frame = CGRect(x: 0, y: 0, width: 278, height: 295)
        animationView.contentMode = .scaleAspectFit
        animationView.loopMode = .loop
        self.viewSquare.addSubview(animationView)
    
        animationView.play()*/
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if (UserDefaults.standard.object(forKey: "already") != nil) {
            self.tabBarController?.tabBar.isHidden = true
        }
        UserDefaults.standard.set("1", forKey: "already")
        UserDefaults.standard.synchronize()
        let rect = CGRect(x: (SCREENWIDTH()-viewSquare.frame.width)/2, y: ((viewBlack.frame.height-viewSquare.frame.height)/2)-46, width:  viewSquare.frame.width, height:  viewSquare.frame.height)
        viewBlack.mask(withRect: rect, inverse: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if isTorchEnable {
            toggleFlash()
        }
        self.tabBarController?.tabBar.isHidden = false
    }
    
    func AddScannerAnimation() {
        let movingView: UIView = UIView.init(frame: CGRect(x: 0, y: 0, width: viewSquare.frame.size.width, height: 1))
        movingView.backgroundColor = UIColor.red
        viewSquare.addSubview(movingView)

        let animation = CABasicAnimation(keyPath: "position")
        animation.toValue = NSValue(cgPoint: CGPoint(x: movingView.center.x, y: viewSquare.frame.size.height))
        animation.duration = 2.0
        animation.autoreverses = true
        animation.repeatCount = .infinity
        animation.isRemovedOnCompletion = false
        animation.fillMode = .both
        movingView.layer.add(animation, forKey: "position")
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.tabBarController?.selectedIndex = 0
    }
    
    @IBAction func btnTorch(_ sender: Any) {
        toggleFlash()
    }
    
    func toggleFlash() {
        guard let device = AVCaptureDevice.default(for: AVMediaType.video) else { return }
        guard device.hasTorch else { return }

        do {
            try device.lockForConfiguration()

            if (device.torchMode == AVCaptureDevice.TorchMode.on) {
                device.torchMode = AVCaptureDevice.TorchMode.off
            } else {
                do {
                    try device.setTorchModeOn(level: 1.0)
                } catch {
                    print(error)
                }
            }
            device.unlockForConfiguration()
        } catch {
            print(error)
        }
        
        isTorchEnable = !isTorchEnable
    }
}

//MARK:- SET FLOATING VIEW
extension BalanceVC: FloatingPanelControllerDelegate {
    func SetupFloatingView() {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            self.fpc = FloatingPanelController()
            self.fpc.delegate = self
            self.fpc.surfaceView.backgroundColor = .clear
            self.fpc.surfaceView.shadowHidden = false
            self.fpc.surfaceView.containerView.layer.cornerRadius = 12.0
            self.fpc.surfaceView.grabberHandleWidth = 47
            self.fpc.surfaceView.grabberHandleHeight = 5
            self.fpc.surfaceView.grabberHandle.backgroundColor = Color_Hex(hex: "DBDBDB")
            self.fpc.addPanel(toParent: self)
            let vc = loadVC(strStoryboardId: SB_MAIN, strVCId: "MyQRCodeVC") as! MyQRCodeVC
            self.fpc.set(contentViewController: vc)
            self.fpc.track(scrollView: vc.tblQR)
            runAfterTime(time: 1) {
                self.fpc.move(to: .half, animated: true)
                self.tabBarController?.tabBar.isHidden = true
            }
        }
    }

    func floatingPanel(_ vc: FloatingPanelController, layoutFor newCollection: UITraitCollection) -> FloatingPanelLayout? {
        return MyQRPanelLayout()
    }
    
    func floatingPanelDidMove(_ vc: FloatingPanelController) {
        
    }
    
    func floatingPanelDidChangePosition(_ vc: FloatingPanelController) {
        self.tabBarController?.tabBar.isHidden = true
    }
}

extension UIView {
    
    func mask(withRect rect: CGRect, inverse: Bool = false) {
        let path = UIBezierPath(rect: rect)
        let maskLayer = CAShapeLayer()
        
        if inverse {
            path.append(UIBezierPath(rect: self.bounds))
            maskLayer.fillRule = CAShapeLayerFillRule.evenOdd
        }
        
        maskLayer.path = path.cgPath
        
        self.layer.mask = maskLayer
    }
}
