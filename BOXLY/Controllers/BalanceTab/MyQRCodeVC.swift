//
//  MyQRCodeVC.swift
//  BOXLY
//
//  Created by iMac on 03/02/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class MyQRCodeVC: UIViewController {

    @IBOutlet var tblQR: UITableView!
    @IBOutlet var btnBack: UIButton!
    @IBOutlet var imgQrCode: UIImageView!
    @IBOutlet var imageViewSmallQrCode: UIImageView!
    @IBOutlet var viewProgress: UIView!

    var isInScreen: Bool = false
    var isFromHost: Bool = false
    
    lazy fileprivate var chartProgress: RPCircularProgress = {
        let progress = RPCircularProgress()
        progress.width = 30
        progress.height = 30
        progress.trackTintColor = UIColor(red: 215/255, green: 216/255, blue: 221/255, alpha: 1)
        progress.progressTintColor = Color_Hex(hex: "#2DD668")
        progress.roundedCorners = false
        progress.thicknessRatio = 1
        return progress
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        /*
        if (UserDefaults.standard.object(forKey: UD_USERNAME) != nil) {
            lblUserName.text = UserDefaults.standard.object(forKey: UD_USERNAME) as? String
        }*/
        btnBack.isHidden = !isFromHost
    }
    
    override func viewDidAppear(_ animated: Bool) {
        isInScreen = true
        self.imgQrCode.image = convertTextToQRCode(text: self.randomAlphaNumericString())
        self.imageViewSmallQrCode.image = convertTextToBarCode(text: self.randomAlphaNumericString())
        viewProgress.addSubview(chartProgress)
        reProgress()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        isInScreen = false
    }
    
    @objc func reProgress() {
        if !isInScreen { return }
        chartProgress.updateProgress(1, animated: true, initialDelay: 1, duration: 60) {
            self.imgQrCode.image = convertTextToQRCode(text: self.randomAlphaNumericString())
            self.imageViewSmallQrCode.image = convertTextToBarCode(text: self.randomAlphaNumericString())
            self.chartProgress.updateProgress(0, animated: false, initialDelay: 0) {
                self.perform(#selector(self.reProgress), with: self, afterDelay: 0.25)
            }
        }
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func randomAlphaNumericString() -> String {
        let allowedChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let allowedCharsCount = UInt32(allowedChars.count)
        var randomString = ""

        for _ in 0..<10 {
            let randomNum = Int(arc4random_uniform(allowedCharsCount))
            let randomIndex = allowedChars.index(allowedChars.startIndex, offsetBy: randomNum)
            let newCharacter = allowedChars[randomIndex]
            randomString += String(newCharacter)
        }

        return randomString
    }
}

