//
//  SavedVC.swift
//  BOXLY
//
//  Created by iMac on 11/03/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit
import Lottie

class SavedVC: UIViewController {

    @IBOutlet var viewAnimation: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        viewAnimation.subviews.forEach({ $0.removeFromSuperview() })
        let animationView = AnimationView(name: "saved")
        animationView.frame = viewAnimation.bounds
        animationView.contentMode = .scaleAspectFit
        animationView.loopMode = .loop
        viewAnimation.addSubview(animationView)
        animationView.play()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
