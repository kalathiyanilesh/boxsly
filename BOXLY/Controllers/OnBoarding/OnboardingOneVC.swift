//
//  OnboardingOneVC.swift
//  BOXLY
//
//  Created by iMac on 11/02/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit
import SCPageControl

class OnboardingOneVC: UIViewController {

    var isFromDelivery: Bool = false
    var isFromHost: Bool = false
    @IBOutlet var pageControl: SCPageControlView!
    @IBOutlet var yBtnSkip: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if SCREENHEIGHT() > 736 {
            yBtnSkip.constant = 44
        }
        pageControl.scp_style = .SCNormal
        pageControl.set_view(3, current: 0, current_color: Color_Hex(hex: "B0C6FF"), disable_color: Color_Hex(hex: "7054FF"))
    }
    
    @IBAction func btnSkip(_ sender: Any) {
        if isFromDelivery {
            UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
                self.navigationController?.popViewController(animated: false)
            }, completion: nil)
        } else if isFromHost {
            UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
                self.navigationController?.popViewController(animated: false)
            }, completion: nil)
        } else {
            let vc = loadVC(strStoryboardId: SB_ONBOARDING, strVCId: "OnboardingPopUpVC") as! OnboardingPopUpVC
            vc.delegateOnBoardingPopUp = self
            vc.modalPresentationStyle = .fullScreen
            vc.modalPresentationStyle = .overCurrentContext
            self.present(vc, animated: true, completion: nil)
        }
    }
}

extension OnboardingOneVC: delegateOnBoardingPopUp {
    func GoToTabScreen() {
        UserDefaults.standard.set(true, forKey: IS_LOGGED_IN)
        APP_DELEGATE.setupInitials()
    }
}
