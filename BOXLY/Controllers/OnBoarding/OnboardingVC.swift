//
//  OnboardingVC.swift
//  BOXLY
//
//  Created by iMac on 03/01/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit
import liquid_swipe

class CellOnbaording: UICollectionViewCell {
    @IBOutlet var imgOnboardingFirst: UIImageView!
    @IBOutlet var imgOnboarding: UIImageView!
    @IBOutlet var lblStep: UILabel!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblDescription: UILabel!
}

class OnboardingVC: LiquidSwipeContainerController, LiquidSwipeContainerDataSource {
    
    var isFromDelivery: Bool = false
    var isFromHost: Bool = false

    var viewControllers: [UIViewController] = {
        let firstPageVC = UIStoryboard(name: SB_ONBOARDING, bundle: nil).instantiateViewController(withIdentifier: "OnboardingOneVC")
        let secondPageVC = UIStoryboard(name: SB_ONBOARDING, bundle: nil).instantiateViewController(withIdentifier: "OnboardingTwoVC")
        let thirdPageVC = UIStoryboard(name: SB_ONBOARDING, bundle: nil).instantiateViewController(withIdentifier: "OnboardingThreeVC")
        var controllers: [UIViewController] = [firstPageVC, secondPageVC, thirdPageVC]
        return controllers
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        datasource = self
    }
    
    func numberOfControllersInLiquidSwipeContainer(_ liquidSwipeContainer: LiquidSwipeContainerController) -> Int {
        return viewControllers.count
    }
       
    func liquidSwipeContainer(_ liquidSwipeContainer: LiquidSwipeContainerController, viewControllerAtIndex index: Int) -> UIViewController {
        if index == 0 {
            let vc = viewControllers[index] as! OnboardingOneVC
            vc.isFromHost = isFromHost
            vc.isFromDelivery = isFromDelivery
            return vc
        } else if index == 1 {
            let vc = viewControllers[index] as! OnboardingTwoVC
            vc.isFromHost = isFromHost
            vc.isFromDelivery = isFromDelivery
            return vc
        } else {
            let vc = viewControllers[index] as! OnboardingThreeVC
            vc.isFromHost = isFromHost
            vc.isFromDelivery = isFromDelivery
            return vc
        }
    }
}
