//
//  OnboardingThreeVC.swift
//  BOXLY
//
//  Created by iMac on 11/02/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit
import SCPageControl

class OnboardingThreeVC: UIViewController {

    var isFromDelivery: Bool = false
    var isFromHost: Bool = false
    @IBOutlet var pageControl: SCPageControlView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        pageControl.scp_style = .SCNormal
        pageControl.set_view(3, current: 2, current_color: Color_Hex(hex: "B0C6FF"), disable_color: Color_Hex(hex: "7054FF"))
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture(_ :)))
        swipeRight.direction = UISwipeGestureRecognizer.Direction.left
        self.view.addGestureRecognizer(swipeRight)
    }
    
    @objc func respondToSwipeGesture(_ gesture: UIGestureRecognizer) {
        GoToTabScreen()
    }
    
    @IBAction func btnSkip(_ sender: Any) {
        if isFromDelivery {
            UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
                self.navigationController?.popViewController(animated: false)
            }, completion: nil)
        } else if isFromHost {
            UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
                self.navigationController?.popViewController(animated: false)
            }, completion: nil)
        } else {
            let vc = loadVC(strStoryboardId: SB_ONBOARDING, strVCId: "OnboardingPopUpVC") as! OnboardingPopUpVC
            vc.delegateOnBoardingPopUp = self
            vc.modalPresentationStyle = .fullScreen
            vc.modalPresentationStyle = .overCurrentContext
            self.present(vc, animated: true, completion: nil)
        }
    }
}

extension OnboardingThreeVC: delegateOnBoardingPopUp {
    func GoToTabScreen() {
        UserDefaults.standard.set(true, forKey: IS_LOGGED_IN)
        APP_DELEGATE.setupInitials()
    }
}
