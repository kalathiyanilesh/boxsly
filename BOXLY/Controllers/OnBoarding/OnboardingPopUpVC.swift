//
//  OnboardingPopUpVC.swift
//  BOXLY
//
//  Created by iMac on 03/02/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit


protocol delegateOnBoardingPopUp {
    func GoToTabScreen()
}

class OnboardingPopUpVC: UIViewController {
    
    @IBOutlet var viewDetails: UIView!
    @IBOutlet var bottomView: NSLayoutConstraint!
    
    var delegateOnBoardingPopUp: delegateOnBoardingPopUp?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.bottomView.constant = -600
        viewDetails.backgroundColor = .clear
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = viewDetails.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        viewDetails.insertSubview(blurEffectView, at: 0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        UIView.animate(withDuration: 0.35) {
            self.bottomView.constant = 0
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func btnYes(_ sender: Any) {
        UIView.animate(withDuration: 0.35, animations: {
            self.bottomView.constant = -600
            self.view.layoutIfNeeded()
        }) { (finished) in
            self.dismiss(animated: true, completion: nil)
            self.delegateOnBoardingPopUp?.GoToTabScreen()
        }
    }
    
    @IBAction func btnNo(_ sender: Any) {
        UserDefaults.standard.set("1", forKey: "isSkip")
        UserDefaults.standard.synchronize()
        self.btnYes(UIButton())
    }
}
