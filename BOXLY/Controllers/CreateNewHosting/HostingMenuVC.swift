//
//  HostingMenuVC.swift
//  BOXLY
//
//  Created by Dhaval on 10/01/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class HostingMenuVC: UIViewController {

    let arrMenu = ["Property and guests","Location","Amenities", "Photos","Title", "Booking Settings", "Availabilities", "Pricing" , "Review"]
    
    @IBOutlet weak var tblMenu: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblMenu.delegate = self
        tblMenu.dataSource = self
        tblMenu.tableFooterView = UIView()
    }
    
    @IBAction func btnCloseClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension HostingMenuVC : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMenu.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HostingMenuCell", for: indexPath) as! HostingMenuCell
        cell.lblMenu.text = arrMenu[indexPath.row]
        if indexPath.row == 0 {
            cell.btnContinue.isHidden = false
        }else {cell.btnContinue.isHidden = true}
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if arrMenu[indexPath.row] == "Property and guests" {
            let vc = loadVC(strStoryboardId: SB_Hosting, strVCId: "TellUsSpacesVC")
            self.navigationController?.pushViewController(vc, animated: true)
        } else if arrMenu[indexPath.row] == "Location" {
            let vc = loadVC(strStoryboardId: SB_Hosting, strVCId: "TakeLocationVC") as! TakeLocationVC
            vc.isForHosting = true
            self.navigationController?.pushViewController(vc, animated: true)
        }else if arrMenu[indexPath.row] == "Photos" {
            let vc = loadVC(strStoryboardId: SB_Hosting, strVCId: "AddPhotosVC")
            self.navigationController?.pushViewController(vc, animated: true)
        } else if arrMenu[indexPath.row] == "Title" {
            let vc = loadVC(strStoryboardId: SB_Hosting, strVCId: "TitleQuestionsVC") as! TitleQuestionsVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
}

class HostingMenuCell : UITableViewCell {
    
    @IBOutlet weak var lblMenu: UILabel!
    @IBOutlet weak var btnContinue: UIButton!
}
