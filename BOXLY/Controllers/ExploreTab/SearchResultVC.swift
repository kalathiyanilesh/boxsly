//
//  SearchResultVC.swift
//  BOXLY
//
//  Created by iMac on 09/01/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class CellOptions: UICollectionViewCell {
    @IBOutlet var lblTitle: UILabel!
}

class CellResults: UITableViewCell {
    @IBOutlet var imgResult: UIImageView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblDesc: UILabel!
    @IBOutlet var lblDollar: UILabel!
}

class SearchResultVC: UIViewController {

    @IBOutlet var collOptions: UICollectionView!
    @IBOutlet var tblResults: UITableView!
    var arrOptions = ["Size", "Time", "Filters"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

//MARK:- UITABLEVIEW DELEGATE METHOD
extension SearchResultVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CellResults = tableView.dequeueReusableCell(withIdentifier: "CellResults", for: indexPath) as! CellResults
        if indexPath.row % 2 == 0 {
            cell.imgResult.image = UIImage(named: "temp1")
        } else {
            cell.imgResult.image = UIImage(named: "temp2")
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = loadVC(strStoryboardId: SB_MAIN, strVCId: "ItemDescriptionVC")
        self.present(vc, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

//MARK:- UICOLLETIONVIEW DELEGATE METHOD
extension SearchResultVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrOptions.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: CellOptions = collectionView.dequeueReusableCell(withReuseIdentifier: "CellOptions", for: indexPath) as! CellOptions
        cell.lblTitle.text = arrOptions[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 72, height: collectionView.frame.size.height)
    }
    
}

