
//
//  MapLocationListVC.swift
//  BOXLY
//
//  Created by iMac on 06/01/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class CellLocation: UITableViewCell {
    @IBOutlet var imgLocationtype: UIImageView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblDesc: UILabel!
    @IBOutlet var imgDropDown: UIImageView!
    @IBOutlet var imgType: UIImageView!
    @IBOutlet var lblHours: UILabel!
    @IBOutlet var yTitle: NSLayoutConstraint!
}

protocol delegateLocationList {
    func gotoHostingFlow()
    func dropOffOptionSelected()
}

protocol delegateChangeFloating {
    func ChangeFloating()
}

enum SegmentsHome : Int {
    case DROP_OFF
    case PICKUP
    case HOST
}

class MapLocationListVC: UIViewController {

    @IBOutlet var btnOptions: [UIButton]!
    @IBOutlet var lblSizeTitleQuestion: UILabel!
    @IBOutlet var topView: UIView!
    @IBOutlet var buttonViewOptions: UIView!
    @IBOutlet var tblLocationList: UITableView!
    @IBOutlet var viewSize: UIView!
    @IBOutlet var collSize: UICollectionView!
    
    var delegateChangeFloating: delegateChangeFloating?
    var delegateLocationList: delegateLocationList?
    var currentSegment : SegmentsHome?
    
    var isPickUp: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblLocationList.isScrollEnabled = false
        collSize.isScrollEnabled = false
        viewSize.alpha = 0
        tblLocationList.tableFooterView = UIView()
                
        let gmView = UIView(frame: topView.frame)
        gmView.tag = 555
        let lblGmMsg = UILabel(frame: CGRect(x: 0, y: 10, width: UIScreen.main.bounds.width, height: topView.bounds.height - 10))
        
        lblGmMsg.text = greetingLogic()
        if (UserDefaults.standard.object(forKey: UD_USERNAME) != nil) {
            lblGmMsg.text = "\(greetingLogic()), \(UserDefaults.standard.object(forKey: UD_USERNAME) as! String)"
        }
        lblGmMsg.font = AIRBNB_CEREAL_MEDIUM(fontSize: 17)
        lblGmMsg.textColor = Color_Hex(hex: "3D3D3D")
        lblGmMsg.textAlignment = .center
        gmView.addSubview(lblGmMsg)
        self.view.addSubview(gmView)
        buttonViewOptions.alpha = 0
    }

    @IBAction func btnOptionClicked(_ sender: UIButton) {
        print("Options Tag: ", sender.tag)
        if sender.tag == 0 {
            self.delegateChangeFloating?.ChangeFloating()
        }
        let gmView = self.view.viewWithTag(555)
        if sender.tag != 1 && sender.tag != 3 {
            UIView.animate(withDuration: 0.35) {
                gmView?.alpha = 1
            }
        }
        
        APP_DELEGATE.isFromPickup = false
        APP_DELEGATE.isFromDeliver = false
        
        switch sender.tag {
        case 0:
            if !UserDefaults.standard.bool(forKey: SEEN_PICKUP_INTRO) {
                let vc = loadVC(strStoryboardId: SB_PICKUP, strVCId: "PickupIntroVC") as! PickupIntroVC
                vc.delegatePickupIntro = self
                UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
                    self.navigationController?.pushViewController(vc, animated: false)
                }, completion: nil)
            } else {
                //NotificationCenter.default.post(name: Notification.Name(ShowScheduleDeliveryVC), object: nil)
                NotificationCenter.default.post(name: Notification.Name(ShowPickupMenuVC), object: nil)
            }
            break
        case 1:
            let vc = loadVC(strStoryboardId: SB_DOCUMENT, strVCId: "DeliveryWelcomeVC") as! DeliveryWelcomeVC
            vc.isDriver = true
            self.navigationController?.pushViewController(vc, animated: true)
            break
        case 2:
            let vc = loadVC(strStoryboardId: SB_DOCUMENT, strVCId: "DeliveryWelcomeVC") as! DeliveryWelcomeVC
            self.navigationController?.pushViewController(vc, animated: true)
            /*
            let nav = UINavigationController(rootViewController: vc)
            nav.setNavigationBarHidden(true, animated: true)
            nav.modalPresentationStyle = .fullScreen
            self.present(nav, animated: true, completion: nil)*/
        case 3:
            if (UserDefaults.standard.object(forKey: "alreadyshowscheduleintro") == nil) {
                let vc = loadVC(strStoryboardId: SB_SCHEDULE, strVCId: "ScheduleInroVC") as! ScheduleInroVC
                vc.delegateScheduleIntro = self
                UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
                    self.navigationController?.pushViewController(vc, animated: false)
                }, completion: nil)
            } else {
                backFromScheduleIntro()
            }
            
        default:
            currentSegment = .HOST
            self.delegateLocationList?.gotoHostingFlow()
            break
        }
    }

    @IBAction func btnBackToTopViewClicked(_ sender: Any) {
        self.topView.isHidden = true
        self.viewSlideInFromRightToLeft(views: topView)

        UIView.animate(withDuration: 0.5) {
            self.viewSize.alpha = 0
        }
        DispatchQueue.main.asyncAfter(deadline: .now()+0.5) {
            self.buttonViewOptions.isHidden = false
            self.viewSlideInFromLeftToRight(views: self.buttonViewOptions)
        }
        
         let gmView = self.view.viewWithTag(555)
        UIView.animate(withDuration: 0.35) {
            gmView?.alpha = 1
        }
    }
    
    @IBAction func btnSeeAll(_ sender: Any) {
        
    }
    
    //MARK: UIBUTTON ACTIONS
    @IBAction func btnSize(_ sender: Any) {
        UIView.animate(withDuration: 0.5) {
            self.viewSize.alpha = 1
        }
    }
   
}

//MARK:- delegates
extension MapLocationListVC: delegateSchedulePopup, delegateScheduleIntro, delegatePickupIntro {
    
    func backFromScheduleIntro() {
        let vc = loadVC(strStoryboardId: SB_SCHEDULE, strVCId: "SchedulePopupVC") as! SchedulePopupVC
        vc.delegateSchedulePopup = self
        vc.isSelectDate = true
        vc.modalPresentationStyle = .fullScreen
        vc.modalPresentationStyle = .overCurrentContext
        self.present(vc, animated: true, completion: nil)
    }
    
    func AddedDetails() {
        let vc = loadVC(strStoryboardId: SB_MAIN, strVCId: "DropOffCalenderVC") as! DropOffCalenderVC
        vc.isFromSchedule = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func backFromPickupIntro() {
        //NotificationCenter.default.post(name: Notification.Name(ShowScheduleDeliveryVC), object: nil)
        NotificationCenter.default.post(name: Notification.Name(ShowPickupMenuVC), object: nil)
    }
}


//MARK:- UITABLEVIEW DELEGATE METHOD
extension MapLocationListVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isPickUp ? 2 : 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CellLocation = tableView.dequeueReusableCell(withIdentifier: "CellLocation", for: indexPath) as! CellLocation
        if isPickUp {
            switch indexPath.row {
            case 0:
                cell.lblTitle.text = "Your package was delivered"
                cell.lblDesc.text = "4255 Patriot Dr"
                cell.lblHours.text = "5hrs"
                cell.imgDropDown.isHidden = false
                cell.imgDropDown.isHidden = false
                cell.imgType.image = UIImage(named: "ic_watch")
                 cell.imgLocationtype.image = UIImage(named: "ic_blue_package")
                cell.yTitle.constant = -8.5
            case 1:
                cell.lblTitle.text = "No packages are waiting"
                cell.lblDesc.text = ""
                cell.lblHours.text = ""
                cell.imgDropDown.isHidden = true
                cell.imgType.isHidden = true
                cell.imgLocationtype.image = UIImage(named: "ic_blue_star")
                cell.yTitle.constant = 0
            default:
                break
            }
        } else {
            switch indexPath.row {
            case 0:
                cell.lblTitle.text = "Your package was delivered"
                cell.lblDesc.text = "4255 Patriot Dr"
                cell.lblHours.text = "5hrs"
                cell.imgDropDown.isHidden = false
                cell.imgDropDown.isHidden = false
                cell.imgType.image = UIImage(named: "ic_watch")
                cell.imgLocationtype.image = UIImage(named: "ic_star_1")
                cell.yTitle.constant = -8.5
            case 1:
                cell.lblTitle.text = "No packages are waiting"
                cell.lblDesc.text = ""
                cell.lblHours.text = ""
                cell.imgDropDown.isHidden = true
                cell.imgType.isHidden = true
                cell.imgLocationtype.image = UIImage(named: "ic_star_1")
                cell.yTitle.constant = 0
            case 2:
                cell.lblTitle.text = "Your pickup is confirmed"
                cell.lblDesc.text = "4255 Patriot Dr"
                cell.lblHours.text = "09:00 1/23/2020"
                cell.imgDropDown.isHidden = true
                cell.imgType.image = UIImage(named: "ic_checkmark")
                cell.imgLocationtype.image = UIImage(named: "ic_pickup_m")
                cell.yTitle.constant = -8.5
            default:
                break
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        /*
        let vc = loadVC(strStoryboardId: SB_MAIN, strVCId: "ExploreVC")
        self.present(vc, animated: true, completion: nil)*/
//        let vc = loadVC(strStoryboardId: SB_MAIN, strVCId: "PickUpDeliveryVC")
//        self.navigationController?.pushViewController(vc, animated: true)
//        vc.modalPresentationStyle = .fullScreen
//        self.present(vc, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 83
    }
}


//MARK:- UICOLLETIONVIEW DELEGATE METHOD
extension MapLocationListVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: CellDelivery = collectionView.dequeueReusableCell(withReuseIdentifier: "CellDelivery", for: indexPath) as! CellDelivery
        switch indexPath.row {
        case 0:
            if self.currentSegment == SegmentsHome.PICKUP {
                cell.imgDelivery.image = UIImage(named: "ic_home")
                cell.lblTitle.text = "Pick Up".uppercased()
            }else{
                cell.imgDelivery.image = UIImage(named: "ic_small_package")
                cell.lblTitle.text = "Small Packages".uppercased()
            }
        case 1:
            if self.currentSegment == SegmentsHome.PICKUP {
                cell.imgDelivery.image = UIImage(named: "ic_truck")
                cell.lblTitle.text = "Delivery".uppercased()
            }else {
                cell.imgDelivery.image = UIImage(named: "ic_large_package")
                cell.lblTitle.text = "Large packages".uppercased()
            }
        default:
            break
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if self.currentSegment == SegmentsHome.DROP_OFF {
            self.delegateLocationList?.dropOffOptionSelected()
        }
    }
    
   func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (SCREENWIDTH()-92)/2, height: 200)
    }
    
}

class CellDelivery: UICollectionViewCell {
    @IBOutlet var imgDelivery: UIImageView!
    @IBOutlet var lblTitle: UILabel!
}
