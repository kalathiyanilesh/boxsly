//
//  DropOffCalenderVC.swift
//  BOXLY
//
//  Created by iMac on 09/01/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class DropOffCalenderVC: UIViewController {

    @IBOutlet var calendar: FSCalendar!
    @IBOutlet var viewPriceMayVar: UIView!
    @IBOutlet var heightVIew: NSLayoutConstraint!
    @IBOutlet var btnChoose: UIButton!
    @IBOutlet var btnCheckedIn: UIButton!
    @IBOutlet var btnPickedup: UIButton!
    
    var toDate : Date?
    var isFromSchedule: Bool = false
    var isFromPickUpDelivery: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isFromSchedule || isFromPickUpDelivery {
            heightVIew.constant = 81
            viewPriceMayVar.alpha = 1
        } else {
            heightVIew.constant = 0
            viewPriceMayVar.alpha = 0
        }
        if isFromPickUpDelivery {
            btnChoose.setBackgroundImage(UIImage(named: "ic_blue"), for: .normal)
        }
        SetUpCalendar()
    }
    
    //MARK: UIBUTON
    @IBAction func btnBack(_ sender: Any) {
        if isFromPickUpDelivery {
            self.dismiss(animated: true, completion: nil)
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func btnChoose(_ sender: Any) {
        if isFromSchedule {
            let vc = loadVC(strStoryboardId: SB_SCHEDULE, strVCId: "PackageSizeVC")
            self.navigationController?.pushViewController(vc, animated: true)
            /*
            let vc = loadVC(strStoryboardId: SB_BOOKING, strVCId: "SelectDeliveryTimeVC") as! SelectDeliveryTimeVC
            vc.selectedDate = calendar.selectedDates.first
            self.navigationController?.pushViewController(vc, animated: true)*/
            /*
            let vc = loadVC(strStoryboardId: SB_SCHEDULE, strVCId: "ScheduleMapVC") as! ScheduleMapVC
            if btnCheckedIn.titleLabel?.text != "Checked In" {
                vc.sCheckedInTime = btnCheckedIn.titleLabel?.text ?? ""
            }
            if btnPickedup.titleLabel?.text != "Picked up" {
                vc.sPickedUpTime = btnPickedup.titleLabel?.text ?? ""
            }
            self.navigationController?.pushViewController(vc, animated: true)*/
        } else {
            let vc = loadVC(strStoryboardId: SB_SELECT_TIMES , strVCId: "SelectTimeVC") as! SelectTimeVC
            //vc.isFromSchedule = isFromSchedule
            vc.isFromPickUpDelivery = isFromPickUpDelivery
            vc.modalPresentationStyle = .fullScreen
            UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
                self.present(vc, animated: false, completion: nil)
            }, completion: nil)
        }
    }
    
    @IBAction func btnClear(_ sender: Any) {
        for date in calendar.selectedDates {
            calendar.deselect(date)
        }
    }
    
    @IBAction func btnCheckedIn(_ sender: Any) {
        let vc = loadVC(strStoryboardId: SB_MAIN, strVCId: "SearchResultVC")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnPickedUp(_ sender: Any) {
        let vc = loadVC(strStoryboardId: SB_MAIN, strVCId: "SearchResultVC")
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension DropOffCalenderVC {
    func SetUpCalendar() {
        calendar.delegate = self
        calendar.dataSource = self
        calendar.currentPage = Date()
        calendar.today = nil
        calendar.appearance.headerTitleColor = Color_Hex(hex: "014040")
        calendar.appearance.headerTitleFont = FontWithSize("AirbnbCerealApp-Book", 33)
        calendar.appearance.weekdayTextColor = Color_Hex(hex: "014040")
        calendar.appearance.weekdayFont = FontWithSize("AirbnbCerealApp-Light", 16)
        calendar.appearance.selectionColor = Color_Hex(hex: "175492")
        calendar.appearance.todayColor =  Color_Hex(hex: "175492")
        calendar.scrollDirection = .vertical
        calendar.allowsMultipleSelection = true
        calendar.swipeToChooseGesture.isEnabled = true
    }
}

extension DropOffCalenderVC: FSCalendarDelegate, FSCalendarDataSource {
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        if toDate == nil {
            toDate = date
        }
        let dateF = DateFormatter()
        dateF.dateFormat = "MMM dd"
        let strDate = dateF.string(from: date)
        if btnCheckedIn.titleLabel?.text == "Checked In" {
            btnCheckedIn.setTitle(strDate, for: .normal)
            btnCheckedIn.setTitleColor(Color_Hex(hex: "175492"), for: .normal)
        } else {
            btnPickedup.setTitle(strDate, for: .normal)
            btnPickedup.setTitleColor(Color_Hex(hex: "175492"), for: .normal)
        }
        
        if !isFromSchedule {
            let vc = loadVC(strStoryboardId: SB_SELECT_TIMES , strVCId: "SelectTimeVC") as! SelectTimeVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func calendar(_ calendar: FSCalendar, shouldSelect date: Date, at monthPosition: FSCalendarMonthPosition) -> Bool {
        if date < Date() {
            return false
        }
        return true
    }
    
}
