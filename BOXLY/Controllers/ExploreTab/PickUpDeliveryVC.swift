//
//  PickUpDeliveryVC.swift
//  BOXLY
//
//  Created by iMac on 01/02/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class CellPickUpDelivery: UICollectionViewCell {
    @IBOutlet var viewContainer: GradientView!
    @IBOutlet var imgDelivery: UIImageView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var imgNext: UIImageView!
}

class PickUpDeliveryVC: UIViewController {

    @IBOutlet var collPickUpDelivery: UICollectionView!
    @IBOutlet var viewTop: UIView!
    
    var selectedIndex = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        APP_DELEGATE.isFromPickup = false
        APP_DELEGATE.isFromDeliver = false
        viewTop.layer.shadowColor = Color_Hex(hex: "#B4B4B4").withAlphaComponent(0.11).cgColor
        viewTop.layer.shadowOpacity = 1
        viewTop.layer.shadowOffset = .zero
        viewTop.layer.shadowRadius = 6
    }
    
    @IBAction func btnClose(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnChoose(_ sender: UIButton) {
        
        if selectedIndex == 0 {
            APP_DELEGATE.isFromPickup = true
        } else if selectedIndex == 1 {
            APP_DELEGATE.isFromDeliver = true
        }
        
        ShowLoadingAnimation(sender)
        runAfterTime(time: 1) {
            if APP_DELEGATE.isFromPickup {
                NotificationCenter.default.post(name: Notification.Name(SetScheduleDirectionText), object: nil)
                self.navigationController?.popToRootViewController(animated: true)
            } else {
                let vc = loadVC(strStoryboardId: SB_SCHEDULE, strVCId: "ScheduleLocationVC") as! ScheduleLocationVC
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
}

extension PickUpDeliveryVC: delegateSchedulePopup {
    func AddedDetails() {
        let vc = loadVC(strStoryboardId: SB_SCHEDULE, strVCId: "PackageSizeVC")
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

//MARK:- UICOLLETIONVIEW DELEGATE METHOD
extension PickUpDeliveryVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: CellPickUpDelivery = collectionView.dequeueReusableCell(withReuseIdentifier: "CellPickUpDelivery", for: indexPath) as! CellPickUpDelivery
        
        
        if selectedIndex == indexPath.row {
            cell.viewContainer.startColor = Color_Hex(hex: "5A99D8")
            cell.viewContainer.endColor = Color_Hex(hex: "5A99D8")
            cell.viewContainer.layer.borderColor = Color_Hex(hex: "AAD2FA").cgColor
            cell.imgNext.image = UIImage(named: "ic_choose_selected")
        } else {
            cell.viewContainer.startColor = Color_Hex(hex: "9CBCDC")
            cell.viewContainer.endColor = Color_Hex(hex: "ACD6FF")
            cell.viewContainer.layer.borderColor = Color_Hex(hex: "5A99D8").cgColor
            cell.imgNext.image = UIImage(named: "ic_choose_unselected")
        }
        switch indexPath.row {
        case 0:
            cell.imgDelivery.image = UIImage(named: "ic_pickup_box")
            cell.lblTitle.text = "Pick Up"
        case 1:
            cell.imgDelivery.image = UIImage(named: "ic_delivery_truck")
            cell.lblTitle.text = "Delivery"
        default:
            break
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        collectionView.reloadData()
    }
    
   func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (SCREENWIDTH()-92)/2, height: 216)
    }
    
}
