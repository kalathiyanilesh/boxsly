//
//  ExploreVC.swift
//  BOXLY
//
//  Created by iMac on 09/01/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class CellMenu: UITableViewCell {
    @IBOutlet var collMenu: UICollectionView!
    
}
class CellExplore: UITableViewCell {
    @IBOutlet var collMenu: UICollectionView!
    
}
class CellCollMenu: UICollectionViewCell {
    @IBOutlet var imgMenu: UIImageView!
    @IBOutlet var lblMenu: UILabel!
}

class ExploreVC: UIViewController {

    fileprivate var tableViewCellCoordinator: [Int: IndexPath] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

extension ExploreVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell: CellMenu = tableView.dequeueReusableCell(withIdentifier: "CellMenu", for: indexPath) as! CellMenu
            cell.collMenu.delegate = self
            cell.collMenu.dataSource = self

            let tag = indexPath.section
            cell.collMenu.tag = tag
            //tableViewCellCoordinator[tag] = indexPath
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellExplore", for: indexPath) as! CellExplore
            
            cell.collMenu.delegate = self
            cell.collMenu.dataSource = self
            cell.collMenu.tag = indexPath.section
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 239
        }else{
            return 91 + (320 * 10 / 2)
        }
    }
}

extension ExploreVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView.tag == 0 {
            let cell: CellCollMenu = collectionView.dequeueReusableCell(withReuseIdentifier: "CellCollMenu", for: indexPath) as! CellCollMenu
            if indexPath.row%2 == 0{
                cell.imgMenu.image = UIImage(named: "temp4")
                cell.lblMenu.text = "Boxes"
            } else {
                cell.imgMenu.image = UIImage(named: "temp3")
                cell.lblMenu.text = "Furniture"
            }
            return cell
        }else{
            let cell: CellCollMenu = collectionView.dequeueReusableCell(withReuseIdentifier: "CellCollMenu", for: indexPath) as! CellCollMenu
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        print("selected collectionViewCell with indexPath: \(indexPath) in tableViewCell with indexPath: \(tableViewCellCoordinator[collectionView.tag]!)")
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView.tag == 0 {
            return CGSize(width: 169, height: 171)
        }else {
            return CGSize(width: (collectionView.bounds.width-20) / 2, height: 320)
        }
    }
}
