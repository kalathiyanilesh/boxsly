//
//  MapVC.swift
//  BOXLY
//
//  Created by iMac on 04/01/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import FloatingPanel
import GoogleMaps
import GooglePlaces
import IQKeyboardManagerSwift
import Pulsator
import SPPermissions

enum CurrentDrawerState {
    case defaultState
    case scheduleDeliveryDrawer
    case PickupMenuDrawer
    case PickupDrawer
}

class MapVC: UIViewController,MKMapViewDelegate {
    @IBOutlet weak var bottomLocationButton: NSLayoutConstraint!
    @IBOutlet var mapView: GMSMapView!
    @IBOutlet var viewBoxlyOpTrailing: NSLayoutConstraint!
    @IBOutlet var btnCurrentLoc: UIButton!
    var currentDrawer : CurrentDrawerState = .defaultState
    var fpc: FloatingPanelController!
    var isOnCurrentLoc: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showLaunchingView()
        IQKeyboardManager.shared.enable = true
        SetupFloatingView()
        SetupMapView()
        viewBoxlyOpTrailing.constant = -150
        if LocationHandler.shared.asklocationPermission() {
            LocationHandler.shared.startUpdatingLocation()
        }
        LocationHandler.shared.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(showShowScheduleDeliveryVC), name: Notification.Name(ShowScheduleDeliveryVC), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showShowPickUpOptionsVC), name: Notification.Name(ShowPickUpOptionsVC), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showShowPickupMenuVC), name: Notification.Name(ShowPickupMenuVC), object: nil)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        if APP_DELEGATE.isSetDropPick {
            APP_DELEGATE.isSetDropPick = false
            let vc = loadVC(strStoryboardId: SB_MAIN, strVCId: "DropPickSizeVC")
            fpc.set(contentViewController: vc)
            fpc.addPanel(toParent: self)
            fpc.move(to: .half, animated: true)
        }
        
        runAfterTime(time: 4) {
            var arrPermissions = [SPPermission]()
            if !SPPermission.camera.isAuthorized {
                arrPermissions.append(.camera)
            }
            if !SPPermission.photoLibrary.isAuthorized {
                arrPermissions.append(.photoLibrary)
            }
            if !SPPermission.locationAlwaysAndWhenInUse.isAuthorized {
                arrPermissions.append(.locationAlwaysAndWhenInUse)
            }
            if !SPPermission.locationWhenInUse.isAuthorized {
                arrPermissions.append(.locationWhenInUse)
            }
            if arrPermissions.count > 0 {
                let contr = SPPermissions.dialog(arrPermissions)
                contr.present(on: self)
            }
        }
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func showShowScheduleDeliveryVC() {
        self.fpc.move(to: .half, animated: true)
        currentDrawer = .scheduleDeliveryDrawer
        let vc = loadVC(strStoryboardId: SB_SCHEDULE, strVCId: "ScheduleDirectionVC") as! ScheduleDirectionVC
        let fpc = FloatingPanelController()
        fpc.delegate = self
        fpc.surfaceView.backgroundColor = .clear
        fpc.surfaceView.shadowHidden = false
        fpc.surfaceView.containerView.layer.cornerRadius = 12.0
        fpc.surfaceView.grabberHandleWidth = 47
        fpc.surfaceView.grabberHandleHeight = 5
        fpc.surfaceView.grabberHandle.backgroundColor = Color_Hex(hex: "DBDBDB")
        fpc.set(contentViewController: vc)
        fpc.addPanel(toParent: self)
        fpc.track(scrollView: vc.tblOptions)
        fpc.move(to: .half, animated: true)
        fpc.isRemovalInteractionEnabled = true
    }
    
    @objc func showShowPickupMenuVC() {
        self.fpc.move(to: .half, animated: true)
        currentDrawer = .PickupMenuDrawer
        let vc = loadVC(strStoryboardId: SB_PICKUP, strVCId: "PickupMenuVC") as! PickupMenuVC
        let fpc = FloatingPanelController()
        fpc.delegate = self
        fpc.surfaceView.backgroundColor = .clear
        fpc.surfaceView.shadowHidden = false
        fpc.surfaceView.containerView.layer.cornerRadius = 12.0
        fpc.surfaceView.grabberHandleWidth = 47
        fpc.surfaceView.grabberHandleHeight = 5
        fpc.surfaceView.grabberHandle.backgroundColor = Color_Hex(hex: "DBDBDB")
        fpc.set(contentViewController: vc)
        fpc.addPanel(toParent: self)
        fpc.move(to: .half, animated: true)
        fpc.isRemovalInteractionEnabled = true
    }
    
    @objc func showShowPickUpOptionsVC() {
        self.fpc.move(to: .half, animated: true)
        currentDrawer = .PickupDrawer
        let vc = loadVC(strStoryboardId: SB_PICKUP, strVCId: "PickupOptionVC") as! PickupOptionVC
        let fpc = FloatingPanelController()
        fpc.delegate = self
        fpc.surfaceView.backgroundColor = .clear
        fpc.surfaceView.shadowHidden = false
        fpc.surfaceView.containerView.layer.cornerRadius = 12.0
        fpc.surfaceView.grabberHandleWidth = 47
        fpc.surfaceView.grabberHandleHeight = 5
        fpc.surfaceView.grabberHandle.backgroundColor = Color_Hex(hex: "DBDBDB")
        fpc.set(contentViewController: vc)
        fpc.addPanel(toParent: self)
        fpc.move(to: .half, animated: true)
        fpc.isRemovalInteractionEnabled = true
    }
    
    @IBAction func btnSideMenuClicked(_ sender: Any) {
        APP_DELEGATE.drawer?.presentLeftMenuViewController()
        self.view.layer.cornerRadius = 15.0
        self.view.clipsToBounds = true
    }
    
    //MARK:- UIBUTTON
    @IBAction func btnDropOff(_ sender: Any) {
        let btn = self.view.viewWithTag(11) as! UIButton
        btnBoxlyOptions(btn)
    }
    
    @IBAction func btnDelivery(_ sender: Any) {
        let btn = self.view.viewWithTag(12) as! UIButton
        btnBoxlyOptions(btn)
    }
    
    @IBAction func btnHost(_ sender: Any) {
        let btn = self.view.viewWithTag(13) as! UIButton
        btnBoxlyOptions(btn)
    }
    
    @IBAction func btnChat(_ sender: Any) {
        /*
        let vc = loadVC(strStoryboardId: SB_CHAT, strVCId: "ChatVC")
        self.navigationController?.pushViewController(vc, animated: true)*/
        navigationController?.pushViewController(BasicExampleViewController(), animated: true)
    }
    
    @IBAction func btnBoxlyOptions(_ sender: UIButton) {
        for i in 0..<3 {
            let viewOp = self.view.viewWithTag(i+1)
            viewOp?.backgroundColor = UIColor.clear
        }
        let viewOp = self.view.viewWithTag(sender.tag-10)
        UIView.animate(withDuration: 0.25, animations: {
            viewOp?.backgroundColor = Color_Hex(hex: "F1F0FF")
        })
        switch sender.tag {
        case 11:
            SetDropOff()
        case 12:
            let vc = loadVC(strStoryboardId: SB_ONBOARDING, strVCId: "OnboardingVC") as! OnboardingVC
            vc.isFromDelivery = true
            UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
                self.navigationController?.pushViewController(vc, animated: false)
            }, completion: { (finished) in
                self.SetDelivery()
            })
        case 13:
            let vc = loadVC(strStoryboardId: SB_ONBOARDING, strVCId: "OnboardingVC") as! OnboardingVC
            vc.isFromHost = true
            UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
                self.navigationController?.pushViewController(vc, animated: false)
            }, completion: { (finished) in
            })
        default:
            break
        }
    }
    
    @IBAction func btnCurrentLoc(_ sender: Any) {
        isOnCurrentLoc = true
        btnCurrentLoc.isHidden = true
        mapView.moveCamera(GMSCameraUpdate.setTarget(LocationHandler.shared.currentLocation!.coordinate, zoom: mapView.camera.zoom))
    }
}

//MARK:- MAPVIEW SETUP
extension MapVC: GMSMapViewDelegate {
    func SetupMapView() {
        mapView.delegate = self
        mapView.settings.allowScrollGesturesDuringRotateOrZoom = false
        mapView.mapStyle(withFilename: "boxly", andType: "json")
        mapView.paddingAdjustmentBehavior = .automatic
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        print("changed position: \(mapView.camera.target)")
        if !isOnCurrentLoc {
            btnCurrentLoc.isHidden = false
        } else {
            isOnCurrentLoc = false
        }
    }
    
    func AddGMSCircle() {
        /*
         if (LocationHandler.shared.currentLocation != nil) {
         let circleCenter : CLLocationCoordinate2D  = LocationHandler.shared.currentLocation!.coordinate
         let circ = GMSCircle(position: circleCenter, radius: 500)
         circ.fillColor = Color_Hex(hex: "#71B0F8").withAlphaComponent(0.55)
         circ.strokeColor = Color_Hex(hex: "#71B0F8").withAlphaComponent(0.55)
         circ.strokeWidth = 0.5
         circ.map = mapView
         }*/
    }
}


//MARK: FLOATING SET CONTENTVIEW
extension MapVC {
    func SetDropOff() {
        let vc = loadVC(strStoryboardId: SB_MAIN, strVCId: "MapLocationListVC") as! MapLocationListVC
        vc.delegateLocationList = self
        vc.delegateChangeFloating = self
        fpc.set(contentViewController: vc)
        fpc.track(scrollView: vc.tblLocationList)
    }
    
    func SetDelivery() {
        let vc = loadVC(strStoryboardId: SB_MAIN, strVCId: "DeliveryVC")
        fpc.set(contentViewController: vc)
        fpc.addPanel(toParent: self)
    }
}

//MARK:- SET FLOATING VIEW
extension MapVC: FloatingPanelControllerDelegate {
    func SetupFloatingView() {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            self.fpc = FloatingPanelController()
            self.fpc.delegate = self
            self.fpc.surfaceView.backgroundColor = .clear
            self.fpc.surfaceView.shadowHidden = false
            self.fpc.surfaceView.containerView.layer.cornerRadius = 12.0
            self.fpc.surfaceView.grabberHandleWidth = 47
            self.fpc.surfaceView.grabberHandleHeight = 5
            self.fpc.surfaceView.grabberHandle.backgroundColor = Color_Hex(hex: "DBDBDB")
            self.fpc.addPanel(toParent: self)
            self.SetDropOff()
            DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                self.fpc.move(to: .half, animated: true)
                self.btnCurrentLoc.isHidden = true
                self.AddGMSCircle()
            }
        }
    }
    
    func floatingPanel(_ vc: FloatingPanelController, layoutFor newCollection: UITraitCollection) -> FloatingPanelLayout? {
        
        if currentDrawer == .defaultState {
            return MyFloatingPanelLayout()
        }else if currentDrawer == .scheduleDeliveryDrawer {
            return ScheduleFloatingPanelLayout()
        }else if currentDrawer == .PickupDrawer {
            return PickupOptionsFloatingPanelLayout()
        }else if currentDrawer == .PickupMenuDrawer {
            return PickupFloatingPanelLayout()
        }else {
            return MyFloatingPanelLayout()
        }
    }
    
    func floatingPanelDidMove(_ vc: FloatingPanelController) {
        let bottom = (vc.view.frame.size.height - vc.surfaceView.frame.origin.y)
        //self.mapView.padding = UIEdgeInsets(top: 0, left: 0, bottom: bottom, right: 0)
        bottomLocationButton.constant = bottom
    }
    
    func floatingPanelDidChangePosition(_ vc: FloatingPanelController) {
        if vc.position == .full {
            self.tabBarController?.tabBar.isHidden = false
            return
        }
        self.tabBarController?.tabBar.isHidden = true
        
        let position = (vc.layout.insetFor(position: vc.position) ?? 0)
        //self.mapView.padding = UIEdgeInsets(top: 0, left: 0, bottom: position, right: 0)
        bottomLocationButton.constant = position
        self.view.endEditing(true)
    }
}

//MARK: DELEGATE LOCATION LIST
extension MapVC: delegateLocationList, delegateChangeFloating {
    func ChangeFloating() {
        if fpc.position == .half {
            fpc.move(to: .full, animated: true)
        }
    }
    
    func gotoHostingFlow() {
        let btn = self.view.viewWithTag(13) as! UIButton
        btnBoxlyOptions(btn)
    }
    
    func closeFloatinController() {
        fpc.move(to: .tip, animated: true)
    }
    
    func dropOffOptionSelected() {
        let vc = loadVC(strStoryboardId: SB_Hosting, strVCId: "TakeLocationVC") as! TakeLocationVC
        vc.isForHosting = false
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension MapVC {
    func showLaunchingView() {
        
        let topView = UIView(frame: self.view.frame)
        APP_DELEGATE.window?.addSubview(topView)
        topView.backgroundColor = .white
        let zoomingView = UIView()
        topView.addSubview(zoomingView)
        zoomingView.translatesAutoresizingMaskIntoConstraints = false
        zoomingView.heightAnchor.constraint(equalToConstant: 120).isActive = true
        zoomingView.widthAnchor.constraint(equalToConstant: 120).isActive = true
        zoomingView.centerYAnchor.constraint(equalTo: topView.centerYAnchor).isActive = true
        zoomingView.centerXAnchor.constraint(equalTo: topView.centerXAnchor).isActive = true
        
        zoomingView.backgroundColor = Color_Hex(hex: "3F535D").withAlphaComponent(0.33)
        zoomingView.layer.cornerRadius = 60
        
        
        let insideView = UIView()
        zoomingView.addSubview(insideView)
        insideView.translatesAutoresizingMaskIntoConstraints = false
        
        insideView.heightAnchor.constraint(equalToConstant: 20).isActive = true
        insideView.widthAnchor.constraint(equalToConstant: 20).isActive = true
        insideView.centerYAnchor.constraint(equalTo: zoomingView.centerYAnchor).isActive = true
        insideView.centerXAnchor.constraint(equalTo: zoomingView.centerXAnchor).isActive = true
        
        insideView.backgroundColor = Color_Hex(hex: "3F535D")
        insideView.layer.cornerRadius = 10
        
        DispatchQueue.main.asyncAfter(deadline: .now()+0.2) {
            UIView.animate(withDuration: 0.7, animations: {
                let originalTransform = zoomingView.transform
                let scaledTransform = originalTransform.scaledBy(x: 50, y: 50)
                let scaledAndTranslatedTransform = scaledTransform.translatedBy(x: 0.0 , y: 0.0)
                zoomingView.transform = scaledAndTranslatedTransform
                zoomingView.contentScaleFactor = 10
                zoomingView.center = CGPoint(x: self.view.center.x, y: self.view.center.y)
            }) { (done) in
                topView.removeFromSuperview()
            }
        }
    }
}

extension MapVC : LocationUpdateProtocol {
    func locationDidUpdateToLocation(location: CLLocation) {
        LocationHandler.shared.stopUpdatingLocation()
        APP_DELEGATE.currentLat = location.coordinate.latitude
        APP_DELEGATE.currentLong = location.coordinate.longitude
        let camera = GMSCameraPosition.camera(withLatitude: APP_DELEGATE.currentLat, longitude: APP_DELEGATE.currentLong, zoom: 18)
        mapView.camera = camera
        runAfterTime(time: 1) {
            CATransaction.begin()
            CATransaction.setValue(NSNumber(value: 1.5 as Float), forKey: kCATransactionAnimationDuration)
            self.mapView.animate(toZoom: 13)
            CATransaction.commit()
            runAfterTime(time: 0.5) {
                self.mapView.clear()
                let marker = GMSMarker()
                marker.position = CLLocationCoordinate2DMake(location.coordinate.latitude,location.coordinate.longitude)
                marker.rotation = location.course
                let pulsator = Pulsator()
                pulsator.radius = 100
                pulsator.numPulse = 3
                pulsator.repeatCount = 3
                pulsator.backgroundColor = UIColor(red: 38/255.0, green: 147/255.0, blue: 238/255.0, alpha: 0.8).cgColor
                let view: UIView = UIView.init(frame: CGRect(x: 0, y: 0, width: 240, height: 240))
                view.backgroundColor = UIColor.clear
                marker.iconView = view
                marker.iconView?.layer.insertSublayer(pulsator, above: marker.iconView?.layer)
                pulsator.position = (marker.iconView?.layer.position)!
                pulsator.start()
                marker.map = self.mapView
                marker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
                let m = GMSMarker()
                m.position = CLLocationCoordinate2DMake(location.coordinate.latitude,location.coordinate.longitude)
                m.rotation = location.course
                m.icon = UIImage(named: "ic_map_pin")
                m.map = self.mapView
                m.groundAnchor = CGPoint(x: 0.5, y: 0.5)
                runAfterTime(time: 5.8) {
                    marker.map = nil
                }
            }
        }
    }
}
