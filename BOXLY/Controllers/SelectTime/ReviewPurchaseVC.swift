//
//  ReviewPurchaseVC.swift
//  BOXLY
//
//  Created by iMac on 22/01/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class ReviewPurchaseVC: UIViewController {

    @IBOutlet var btnAddPayment: UIButton!
    var screenIsFor : screenIsFor = .scheduling
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if screenIsFor == .scheduling {
            btnAddPayment.setTitle("Continue", for: .normal)
        }
    }

    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnAddPayment(_ sender: UIButton) {
        ShowLoadingAnimation(sender)
        runAfterTime(time: 1) {
            if self.screenIsFor == .scheduling {
                self.navigationController?.popToRootViewController(animated: false)
            } else {
                let vc = loadVC(strStoryboardId: SB_SELECT_TIMES, strVCId: "BookYourDropVC")
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
}
