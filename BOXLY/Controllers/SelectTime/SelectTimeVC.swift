//
//  SelectTimeVC.swift
//  BOXLY
//
//  Created by Dhaval on 13/01/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class SelectTimeVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var lblToday: UILabel!
    @IBOutlet var lblDate: UILabel!
    @IBOutlet var btnChoose: UIButton!
    @IBOutlet var lblPricesMayVar: UILabel!
    
    var isFromSchedule: Bool = false
    var isFromPickUpDelivery: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        if isFromPickUpDelivery {
            btnChoose.setBackgroundImage(UIImage(named: "ic_blue"), for: .normal)
            lblToday.textColor = Color_Hex(hex: "2C62BE")
            lblDate.textColor = Color_Hex(hex: "2C62BE")
            lblPricesMayVar.textColor = Color_Hex(hex: "2C62BE")
        }
    }
    
    @IBAction func btnBackClicked(_ sender: Any) {
        if isFromSchedule || isFromPickUpDelivery {
            UIView.transition(with: APP_DELEGATE.window!, duration: 0.2, options: .transitionCrossDissolve, animations: {
                self.dismiss(animated: true, completion: nil)
            }, completion: nil)
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func btnChoose(_ sender: Any) {
        if var viewControllers: [UIViewController] = self.navigationController?.viewControllers {
            viewControllers = viewControllers.reversed()
            for currentViewController in viewControllers {
                if currentViewController .isKind(of: TabBarVC.self) {
                    APP_DELEGATE.isSetDropPick = true
                    self.navigationController?.popToViewController(currentViewController, animated: true)
                    break
                }
            }
        }
    }
}

extension SelectTimeVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 20
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TimeListCell", for: indexPath) as! TimeListCell
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let viewHeader = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.width, height: 60))
        viewHeader.backgroundColor = .white
        let label = UILabel(frame: CGRect(x: 20, y: 0, width: 500, height: 60))
        label.font = AIRBNB_CEREAL_BOLD(fontSize: 21)
        label.text = "Friday 10"
        viewHeader.addSubview(label)
        return viewHeader
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
}

class TimeListCell : UITableViewCell {
    
}
