//
//  DropPickSizeVC.swift
//  BOXLY
//
//  Created by iMac on 21/01/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit
import Lottie

class DropPickSizeVC: UIViewController {

    var selectedIndex = -1
    @IBOutlet var tblList: UITableView!
    @IBOutlet var btnChooseHere: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnChooseHere.isHidden = true
    }
    
    @IBAction func btnChooseHere(_ sender: Any) {
        let vc = loadVC(strStoryboardId: SB_SELECT_TIMES, strVCId: "AboutDropOffVC")
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension DropPickSizeVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 15
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CellDropPickSize = tableView.dequeueReusableCell(withIdentifier: "CellDropPickSize", for: indexPath) as! CellDropPickSize
        cell.imgSelection.image = UIImage(named: "ic_unselected")
        if selectedIndex == indexPath.row {
            cell.imgSelection.image = UIImage(named: "ic_selected")
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 68
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        tblList.reloadData()
        btnChooseHere.isHidden = false
    }
}

class CellDropPickSize: UITableViewCell {
    @IBOutlet var imgSelection: UIImageView!
    @IBOutlet var viewFavAnim: AnimationView!
    @IBOutlet var lblAddress: UILabel!
}
