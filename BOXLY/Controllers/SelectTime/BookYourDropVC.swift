//
//  BookYourDropVC.swift
//  BOXLY
//
//  Created by iMac on 22/01/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class CellPickUp: UITableViewCell {
    
}

class CellKeepInMind: UITableViewCell {
    
}

class BookYourDropVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func btnAgree(_ sender: Any) {
    }
}

extension BookYourDropVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerview = UIView.init(frame: CGRect(x: 0, y: 0, width: SCREENWIDTH(), height: 70))
        headerview.backgroundColor = .white
        
        let line = UILabel()
        line.frame = CGRect(x: 36, y: 0, width: SCREENWIDTH()-71, height: 1)
        line.backgroundColor = Color_Hex(hex: "D1D1D1")
        headerview.addSubview(line)
        
        let label = UILabel()
        label.frame = CGRect(x: 37, y: 0, width: SCREENWIDTH()-47, height: 70)
        switch section {
        case 0:
            label.text = "5 hours at two men and a truck"
        default:
            label.text = "Things to keep in mind"
        }
        label.font = FontWithSize("AirbnbCerealApp-Book",18)
        headerview.addSubview(label)
        
        return headerview
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 2
        default:
            return 3
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellPickUp", for: indexPath) as! CellPickUp
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellKeepInMind", for: indexPath) as! CellKeepInMind
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 104
        default:
            return 78
        }
    }
}
