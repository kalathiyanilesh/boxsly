//
//  WhoComingVC.swift
//  BOXLY
//
//  Created by iMac on 21/01/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class CellUser: UITableViewCell {
    
    @IBOutlet var lblUserName: UILabel!
    @IBOutlet var imgUser: UIImageView!
}

class CellAdd: UITableViewCell {
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var btnAdd: UIButton!
}

class WhoComingVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnAccept(_ sender: Any) {
        let vc = loadVC(strStoryboardId: SB_SELECT_TIMES, strVCId: "ReviewPurchaseVC")
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension WhoComingVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellUser", for: indexPath) as! CellUser
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellAdd", for: indexPath) as! CellAdd
            if indexPath.row == 1 {
                cell.lblTitle.text = "Add another guest"
            } else {
                cell.lblTitle.text = "Add a guest under 18 years old"
            }
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.row == 0 ? 131 : 74
    }
}
