//
//  AboutDropOffVC.swift
//  BOXLY
//
//  Created by iMac on 21/01/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit

class AboutDropOffVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func btnScheduleDelivery(_ sender: Any) {
        let vc = loadVC(strStoryboardId: SB_SELECT_TIMES, strVCId: "ReviewDropOffVC")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
