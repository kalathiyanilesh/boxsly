//
//  ChatVC.swift
//  BOXLY
//
//  Created by iMac on 29/02/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class CellText: UITableViewCell {
    @IBOutlet var imgUser: UIImageView!
    @IBOutlet var lblMsg: UILabel!
    @IBOutlet var viewSendBack: UIView!
    @IBOutlet var viewReceiveBack: GradientView!
    @IBOutlet var slider: UISlider!
}

class ChatVC: UIViewController, GrowingTextViewDelegate {

    @IBOutlet var tblChat: UITableView!
    @IBOutlet var txtMsg: GrowingTextView!
    @IBOutlet var heightBottomView: NSLayoutConstraint!
    
    var arrMsg = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func textViewDidChangeHeight(_ textView: GrowingTextView, height: CGFloat) {
        heightBottomView.constant = txtMsg.contentSize.height
        if heightBottomView.constant < 58 {
            heightBottomView.constant = 58
        }
        if heightBottomView.constant > 145 {
            heightBottomView.constant = 145
        }
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnAttachment(_ sender: Any) {
        
    }
    
    @IBAction func btnbCamera(_ sender: Any) {
        self.view.layoutIfNeeded()
        ImagePickerManager().pickImage(self, true) { (img) in
            //set image here
        }
    }
    
    @IBAction func btnSend(_ sender: Any) {
        txtMsg.text = TRIM(string: txtMsg.text)
        if txtMsg.text.count > 0 {
            self.view.endEditing(true)
            arrMsg.add(txtMsg.text)
            txtMsg.text = ""
            tblChat.beginUpdates()
            tblChat.insertRows(at: [IndexPath(row: arrMsg.count-1, section: 4)], with: .automatic)
            tblChat.endUpdates()
            scrollToBottom()
        }
    }
    
    func scrollToBottom()  {
        let indexPath = NSIndexPath(item: arrMsg.count-1, section: 4)
        tblChat.scrollToRow(at: indexPath as IndexPath, at: .bottom, animated: true)
    }
}

//MARK:- UITABLEVIEW DELEGATE
extension ChatVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 30))
        let label = UILabel()
        label.frame = CGRect.init(x: 5, y: 12, width: headerView.frame.width-10, height: headerView.frame.height-10)
        label.text = "09:50 PM"
        label.textAlignment = .center
        label.textColor = Color_Hex(hex: "#2B3857").withAlphaComponent(0.50)
        label.font = UIFont(name: "AirbnbCerealApp-Bold", size: 12)
        headerView.addSubview(label)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 4:
            return arrMsg.count > 0 ? 30 : 0
        default:
            return 30
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0, 2, 3:
            return 1
        case 1:
            return 2
        default:
            return arrMsg.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0, 2:
            let cell: CellText = tableView.dequeueReusableCell(withIdentifier: "CellReceiveText", for: indexPath) as! CellText
            cell.lblMsg.text = indexPath.section == 0 ? "I can help you.\nPlease send me last location of you pet." : "Now I go to find & get notification to you when I found it."
            return cell
        case 1:
            switch indexPath.row {
            case 0:
                let cell: CellText = tableView.dequeueReusableCell(withIdentifier: "CellSendText", for: indexPath) as! CellText
                cell.lblMsg.text = "Thank you!"
                return cell
            default:
                let cell: CellText = tableView.dequeueReusableCell(withIdentifier: "CellSendMap", for: indexPath) as! CellText
                return cell
            }
        case 3:
            let cell: CellText = tableView.dequeueReusableCell(withIdentifier: "CellSendAudio", for: indexPath) as! CellText
            cell.slider.setThumbImage(UIImage(named: "thumb_slider"), for: .normal)
            cell.slider.setThumbImage(UIImage(named: "thumb_slider"), for: .highlighted)
            return cell
        default:
            let cell: CellText = tableView.dequeueReusableCell(withIdentifier: "CellSendText", for: indexPath) as! CellText
            cell.lblMsg.text = arrMsg[indexPath.row] as? String
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 3:
            return 60
        default:
            return UITableView.automaticDimension
        }
    }
}
