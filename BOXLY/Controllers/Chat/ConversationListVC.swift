//
//  ConversationListVC.swift
//  BOXLY
//
//  Created by iMac on 29/02/20.
//  Copyright © 2020 iMac. All rights reserved.
//

import UIKit
import Lottie

class ConversationListVC: UIViewController {
    
    @IBOutlet var viewEmtyConv: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        viewEmtyConv.subviews.forEach({ $0.removeFromSuperview() })
        let animationView = AnimationView(name: "emptyconversation")
        animationView.frame = viewEmtyConv.bounds
        animationView.contentMode = .scaleAspectFit
        animationView.loopMode = .loop
        viewEmtyConv.addSubview(animationView)
        animationView.play()
    }
    
    @IBAction func btnChat(_ sender: Any) {
        let vc = loadVC(strStoryboardId: SB_CHAT, strVCId: "ChatVC")
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
