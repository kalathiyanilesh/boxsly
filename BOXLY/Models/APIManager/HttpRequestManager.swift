//
//  HttpRequestManager.swift
//  SwiftDemo
//
//  Created by C79 on 14/03/2020.
//  Copyright © 2020 Narolainfotech. All rights reserved.
//

import UIKit
import Alamofire

//Encoding Type
let URL_ENCODING = URLEncoding.default
let JSON_ENCODING = JSONEncoding.default

//Web Service Result
let WS_RESPONSE_CODE = "response_code"
let WS_RESPONSE_TEXT = "response_text"
let WS_STATUS_CODE = "status_code"

public enum RESPONSE_STATUS : NSInteger
{
    case INVALID
    case VALID
    case MESSAGE
}

protocol UploadProgressDelegate
{
    func didReceivedProgress(progress:Float)
}
protocol DownloadProgressDelegate
{
    func didReceivedDownloadProgress(progress:Float,filename:String)
    func didFailedDownload(filename:String)
}

class HttpRequestManager
{
    static let sharedInstance = HttpRequestManager()
    let additionalHeader = ["User-Agent": "iOS"]
    var responseObjectDic = Dictionary<String, AnyObject>()
    var URLString : String!
    var Message : String!
    var resObjects:AnyObject!
    var alamoFireManager = Alamofire.SessionManager.default
    var delegate : UploadProgressDelegate?
    var downloadDelegate : DownloadProgressDelegate?
    // METHODS
    init()
    {
        alamoFireManager.session.configuration.timeoutIntervalForRequest = 120 //seconds
        alamoFireManager.session.configuration.httpAdditionalHeaders = additionalHeader
    }
    
    //MARK:- POST Request
    func postJSONRequest(endpointurl:String, parameters:NSDictionary, encodingType:ParameterEncoding = JSONEncoding.default, withAccessToken : Bool = false, responseData:@escaping (_ data: AnyObject?, _ error: NSError?) -> Void)
    {
        ShowNetworkIndicator(xx: true)
        var header : [String: Any]?
        if withAccessToken {
            if let token = UserDefaults.standard.getCustomObject(forKey: UD_TOKEN_OBJECT) {
                print("Token: ", token)
                header = ["token" : token]
            }
        }
        
        alamoFireManager.request(endpointurl, method: .post, parameters: parameters as? Parameters, encoding: encodingType, headers: header as? HTTPHeaders)
            .responseJSON { (response:DataResponse<Any>) in
            ShowNetworkIndicator(xx: false)
                
            if let _ = response.result.error
            {
                responseData(nil, response.result.error as NSError?)
            }
            else
            {
                switch(response.result)
                {
                case .success(_):
                    if let data = response.result.value
                    {
                        self.resObjects = (data as! NSDictionary) as AnyObject?
                        responseData(self.resObjects, nil)
                    }
                    break
                case .failure(_):
                    responseData(nil, response.result.error as NSError?)
                    break
                }
            }
        }
    }
    
    //MARK:- GET Request
    func getRequestWithoutParams(endpointurl:String, responseData:@escaping (_ data:AnyObject?, _ error: NSError?) -> Void)
    {
        ShowNetworkIndicator(xx: true)
        var header : [String: Any]?
        
        alamoFireManager.request(endpointurl, method: .get, headers: header as? HTTPHeaders).responseJSON { (response:DataResponse<Any>) in
            ShowNetworkIndicator(xx: false)
            
            if let _ = response.result.error
            {
                responseData(nil, response.result.error as NSError?)
            }
            else
            {
                switch(response.result)
                {
                case .success(_):
                    if let data = response.result.value
                    {
                        self.resObjects = (data as! NSDictionary) as AnyObject?
                        responseData(self.resObjects, nil)
                    }
                    break
                case .failure(_):
                    responseData(nil, response.result.error as NSError?)
                    break
                }
            }
        }
    }
    
    func getRequest(endpointurl:String,parameters:NSDictionary,responseData:@escaping (_ data: AnyObject?, _ error: NSError?) -> Void)
    {
        ShowNetworkIndicator(xx: true)
        alamoFireManager.request(endpointurl, method: .get, parameters: parameters as? [String : AnyObject]).responseJSON { (response:DataResponse<Any>) in
            ShowNetworkIndicator(xx: false)
            
            if let _ = response.result.error
            {
                responseData(nil, response.result.error as NSError?)
            }
            else
            {
                switch(response.result)
                {
                case .success(_):
                    if let data = response.result.value
                    {
                        self.resObjects = (data as! NSDictionary) as AnyObject?
                        responseData(self.resObjects, nil)
                    }
                    break
                case .failure(_):
                    responseData(nil, response.result.error as NSError?)
                    break
                }
            }
        }
        
    }
    
    
    //MARK:- PUT Request
    func putRequest(endpointurl:String,parameters:NSDictionary,responseData:@escaping (_ data: AnyObject?, _ error: NSError?, _ message: String?) -> Void)
    {
        ShowNetworkIndicator(xx: true)
        
        alamoFireManager.request(endpointurl, method: .post, parameters: parameters as? Parameters).responseJSON { (response:DataResponse<Any>) in
            ShowNetworkIndicator(xx: false)
            if let _ = response.result.error
            {
                responseData(nil, response.result.error as NSError?, DEFAULT_ERROR_MESSAGE)
            }
            else
            {
                switch(response.result) {
                case .success(_):
                    if let data = response.result.value
                    {
                        self.resObjects = (data as! NSDictionary) as AnyObject?
                        responseData(self.resObjects, nil, self.Message)
                    }
                    break
                    
                case .failure(_):
                    responseData(nil, response.result.error as NSError?, DEFAULT_ERROR_MESSAGE)
                    break
                    
                }
            }
        }
    }
    
    //MARK:- PATCH Request
    func patchJSONRequest(endpointurl:String, parameters:NSDictionary, encodingType:ParameterEncoding = JSONEncoding.default, withAccessToken : Bool = false, responseData:@escaping (_ data: AnyObject?, _ error: NSError?) -> Void)
    {
        ShowNetworkIndicator(xx: true)
        
        var header : [String: Any]?
        if withAccessToken {
            if let token = UserDefaults.standard.getCustomObject(forKey: UD_TOKEN_OBJECT) {
                print("Token: ", token)
                header = ["token" : token]
            }
        }
        
        alamoFireManager.request(endpointurl, method: .patch, parameters: parameters as? Parameters, encoding: encodingType, headers: header as? HTTPHeaders)
            
            .responseJSON { (response:DataResponse<Any>) in
                
            ShowNetworkIndicator(xx: false)
                
            if let _ = response.result.error
            {
                responseData(nil, response.result.error as NSError?)
            }
            else
            {
                switch(response.result)
                {
                case .success(_):
                    if let data = response.result.value
                    {
                        self.resObjects = (data as! NSDictionary) as AnyObject?
                        responseData(self.resObjects, nil)
                    }
                    break
                case .failure(_):
                    responseData(nil, response.result.error as NSError?)
                    break
                }
            }
        }
    }

    //MARK:- Cancel Request
    func cancelAllAlamofireRequests(responseData:@escaping ( _ status: Bool?) -> Void)
    {
       alamoFireManager.session.getTasksWithCompletionHandler
            {
                dataTasks, uploadTasks, downloadTasks in
                dataTasks.forEach { $0.cancel() }
                uploadTasks.forEach { $0.cancel() }
                downloadTasks.forEach { $0.cancel() }
                responseData(true)
        }
    }
    
    func postMultipartJSONRequest(endpointurl:String, parameters:NSDictionary, encodingType:ParameterEncoding = JSONEncoding.default, responseData:@escaping (_ data: AnyObject?, _ error: NSError?) -> Void)
    {
        ShowNetworkIndicator(xx: true)
        
        var header : [String: Any]?
        
        alamoFireManager.upload(multipartFormData: { (multipartFormData) in
            
            for (key, value) in parameters
            {
                if value is UIImage {
                    let imageData = (value as! UIImage).jpegData(compressionQuality: 0.3)!
                    multipartFormData.append(imageData, withName: key as! String, fileName: "swift_file.jpg", mimeType: "image/*")
                }else if value is NSURL || value is URL {
                    let videoData:Data
                    do {
                        videoData = try Data (contentsOf: (value as! URL), options: .mappedIfSafe)
                        multipartFormData.append(videoData, withName: key as! String, fileName: "swift_file.mp4", mimeType: "video/*")
                    } catch {
                        print(error)
                        return
                    }
                }else if value is NSArray || value is NSMutableArray {
                    for childValue in value as! NSArray
                    {
                        if childValue is UIImage {
                            let imageData = (childValue as! UIImage).jpegData(compressionQuality: 0.3)!
                            multipartFormData.append(imageData, withName: key as! String, fileName: "swift_file.jpg", mimeType: "image/*")
                        }
                    }
                }
                else {
                    if value is String {
                        let valueData:Data = (value as! NSString).data(using: String.Encoding.utf8.rawValue)!
                        multipartFormData.append(valueData, withName: key as! String)
                    }else{
                        let valueData:Data = ("\(value)").data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!
                        multipartFormData.append(valueData, withName: key as! String)
                    }
                }
            }
            
        }, to: endpointurl, headers: header as? HTTPHeaders) { encodingResult in
            
            ShowNetworkIndicator(xx: false)
            
            switch encodingResult {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (progress) in
                    //if isUploading && isForeground {
                    //self.delegate?.didReceivedProgress(progress: Float(progress.fractionCompleted))
                    //}
                })
                
                upload.responseString(completionHandler: { (resp) in
                    //print("RESP : \(resp)")
                })
                
                upload.responseJSON { response in
                    ////print(response)
                    switch(response.result) {
                    case .success(_):
                        if let data = response.result.value
                        {
                            self.resObjects = (data as! NSDictionary) as AnyObject?
                            responseData(self.resObjects, nil)
                        }
                        break
                        
                    case .failure(_):
                        responseData(nil, response.result.error as NSError?)
                        break
                        
                    }
                }
            case .failure( _):
                responseData(nil, nil)
            }
        }
    }
}

public func ShowNetworkIndicator(xx :Bool)
{
    DispatchQueue.main.async {
        UIApplication.shared.isNetworkActivityIndicatorVisible = xx
    }
}

public func getDataFromUrl(url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
    URLSession.shared.dataTask(with: url) { data, response, error in
        completion(data, response, error)
        }.resume()
}
