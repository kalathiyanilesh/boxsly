//
//  ImagePickerManager.swift
//  BINExchange
//
//  Created by iOS_1 on 08/01/19.
//  Copyright © 2019 iOS_1. All rights reserved.
//

import UIKit

class ImagePickerManager: NSObject,UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    var picker = UIImagePickerController();
    var alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
    var viewController: UIViewController?
    var pickImageCallback : ((UIImage) -> ())?
    var allowsPicEditing:Bool = false
    
    override init(){
        super.init()
    }
    
    func pickImage(_ viewController: UIViewController, _ allowsEditing: Bool, _ callback: @escaping ((UIImage) -> ())) {
        APP_DELEGATE.window?.endEditing(true)
        allowsPicEditing = allowsEditing
        pickImageCallback = callback
        self.viewController = viewController
        
        let cameraAction = UIAlertAction(title: "Take Photo".Localized(), style: .default){
            UIAlertAction in
            self.openCamera()
        }
        let gallaryAction = UIAlertAction(title: "Select from gallery".Localized(), style: .default){
            UIAlertAction in
            self.openGallery()
        }
        let cancelAction = UIAlertAction(title: "Cancel".Localized(), style: .cancel){
            UIAlertAction in
        }
        
        // Add the actions
        picker.delegate = self
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        alert.popoverPresentationController?.sourceView = self.viewController!.view
        viewController.present(alert, animated: true, completion: nil)
    }
    
    func openCamera(){
        alert.dismiss(animated: true, completion: nil)
        if(UIImagePickerController .isSourceTypeAvailable(.camera)){
            picker.sourceType = .camera
            picker.allowsEditing = allowsPicEditing
            self.viewController!.present(picker, animated: true, completion: nil)
        } else {
            showMessage(DontHaveCamera)
        }
    }
    
    func openGallery(){
        alert.dismiss(animated: true, completion: nil)
        picker.sourceType = .photoLibrary
        picker.allowsEditing = allowsPicEditing
        picker.modalPresentationStyle = .fullScreen
        self.viewController!.present(picker, animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        if allowsPicEditing {
            let image = info[UIImagePickerController.InfoKey.editedImage] as! UIImage
            pickImageCallback?(image)
        } else {
            let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
            pickImageCallback?(image)
        }
    }
}
